
// ADD ELEMENT SIZE TO Array ?
// IMPLEMENT LArray FUNCTIONS WITH COMPARATORS (search, remove)
// IMPLEMENT sort, binSearch AND sortedInsert FUNCTIONS FOR LArray (WITH COMPARATORS)


#ifndef ARRAY_C
#define ARRAY_C


#include <string.h>

#include "Memory.h"



typedef struct Array {
	int wdt;
	int dpt;

	int size;

	void **root;

} Array;


static inline Array* Array_create(int wdt) {
	if(wdt < 2) {
		return NULL;
	}

	Array *arr = (Array*) Memory_alloc(sizeof(Array));

	arr->wdt = wdt;
	arr->dpt = 0;

	arr->size = wdt;

	arr->root = (void**) Memory_alloc0(wdt * sizeof(void*));

	return arr;
}

static void __Array_destroy(void **ptr, int dpt, int wdt) {
	if(--dpt >= 0) {
		int i;

		for(i=0; i<wdt; i++) {
			if(ptr[i] != NULL) {
				__Array_destroy(ptr[i], dpt, wdt);
			}
		}
	}

	Memory_free(ptr);
}

static inline void Array_destroy(void *ptr) {
	Array *arr = (Array*) ptr;

	__Array_destroy(arr->root, arr->dpt, arr->wdt);

	Memory_free(arr);
}


static void** __Array_address(Array *arr, unsigned int idx) {
	if(idx >= arr->size) {
		return NULL;
	}

	if(arr->dpt == 0) {
		return arr->root + idx;
	}

	void **ptr = arr->root;

	int w = arr->size / arr->wdt;

	unsigned int i = idx / w;

	idx %= w;

	do {
		ptr += i;

		if(*ptr == NULL) {
			return NULL;
		}

		ptr = *ptr;

		w /= arr->wdt;

		i = idx / w;

		idx %= w;

	} while(w > 1);

	return ptr + i;
}

static void** Array_address(Array *arr, unsigned int idx) {
	while(idx >= arr->size) {
		++arr->dpt;

		arr->size *= arr->wdt;

		void **ptr = (void**) Memory_alloc0(arr->wdt * sizeof(void*));

		ptr[0] = arr->root;

		arr->root = ptr;
	}

	if(arr->dpt == 0) {
		return arr->root + idx;
	}

	void **ptr = arr->root;

	int w = arr->size / arr->wdt;

	unsigned int i = idx / w;

	idx %= w;

	do {
		ptr += i;

		if(*ptr == NULL) {
			*ptr = (void**) Memory_alloc0(arr->wdt * sizeof(void*));
		}

		ptr = *ptr;

		w /= arr->wdt;

		i = idx / w;

		idx %= w;

	} while(w > 1);

	return ptr + i;
}


static inline void Array_set(Array *arr, unsigned int idx, void *val) {
	*Array_address(arr, idx) = val;
}

static inline void* Array_get(Array *arr, unsigned int idx) {
	void **ptr = __Array_address(arr, idx);

	if(ptr == NULL) {
		return NULL;
	}

	return *ptr;
}


// static inline void Array_set2(Array *arr, unsigned int idx, void *ptr, unsigned int size) {
// 	memcpy(Array_address(arr, idx), ptr, size);
// }

// static inline void Array_get2(Array *arr, unsigned int idx, void *ptr, unsigned int size) {
// 	void **adr = __Array_address(arr, idx);

// 	if(adr == NULL) {
// 		memset(ptr, 0, size);

// 	} else {
// 		memcpy(ptr, adr, size);
// 	}
// }



typedef struct LArray {
	void **arr;

	unsigned int cnt;
	unsigned int sz;

} LArray;


static inline void LArray_init(LArray *arr, int sz) {
	if(sz < 2) sz = 2;

	arr->arr = (void**) Memory_alloc(sz * sizeof(void*));

	arr->cnt = 0;
	arr->sz = sz;
}

static inline LArray* LArray_create(int sz) {
	LArray *arr = Memory_alloc(sizeof(LArray));

	LArray_init(arr, sz);

	return arr;
}


static inline void LArray_term(void *ptr) {
	LArray *arr = (LArray*) ptr;

	Memory_free(arr->arr);
}

static inline void LArray_destroy(void *ptr) {
	LArray_term(ptr);

	Memory_free(ptr);
}

static inline void LArray_clear(LArray *arr, void (*dst)(void *)) {
	if(dst != NULL) {
		int i, n = arr->cnt;

		for(i=0; i<n; i++) {
			dst(arr->arr[i]);
		}	
	}

	arr->cnt = 0;
}


static inline unsigned int LArray_size(LArray *arr) {
	return arr->cnt;
}


static inline void LArray_grow(LArray *arr, unsigned int sz) {
	if(sz < 2) sz = 2;

	if(sz <= arr->sz) return;

	if(sz <= arr->cnt) return;

	void **tmp = (void**) Memory_alloc(sz * sizeof(void*));

	memcpy(tmp, arr->arr, arr->cnt * sizeof(void*));

	Memory_free(arr->arr);

	arr->sz = sz;

	arr->arr = tmp;
}

static inline void LArray_insert(LArray *arr, void *val) {
	if(arr->cnt >= arr->sz) {
		LArray_grow(arr, (arr->sz * 5) / 3);
	}

	arr->arr[arr->cnt++] = val;
}

static inline unsigned int LArray_remove(LArray *arr, void *val) {
	unsigned int i, c = 0;

	for(i=0; i<arr->cnt; i++) {
		if(arr->arr[i] == val) {
			++c;

		} else if(c > 0) {
			arr->arr[i - c] = arr->arr[i];
		}
	}

	arr->cnt -= c;

	return c;
}

static inline int LArray_removei(LArray *arr, unsigned int idx) {
	if(idx >= arr->cnt) {
		return 0;
	}

	while(++idx < arr->cnt) {
		arr->arr[idx - 1] = arr->arr[idx];
	}

	--arr->cnt;
	
	return 1;
}

static inline int LArray_removef(LArray *arr, unsigned int idx) {
	if(idx >= arr->cnt) {
		return 0;
	}

	arr->arr[idx] = arr->arr[--arr->cnt];
	
	return 1;
}

static inline int LArray_search(LArray *arr, void *val) {
	int i;

	for(i=0; i<arr->cnt; i++) {
		if(arr->arr[i] == val) {
			return i;
		}
	}

	return - 1;
}


static inline void LArray_push(LArray *arr, void *val) {
	LArray_insert(arr, val);

	int i = arr->cnt - 1;

	void *last = arr->arr[i];

	while(--i >= 0) {
		arr->arr[i + 1] = arr->arr[i];
	}

	arr->arr[0] = last;
}

static inline void* LArray_pop(LArray *arr) {
	if(arr->cnt < 1) return NULL;

	void *val = arr->arr[0];

	LArray_removei(arr, 0);

	return val;
}


static inline void LArray_usSet(LArray *arr, unsigned int idx, void *val) {
	arr->arr[idx] = val;
}

static inline int LArray_set(LArray *arr, unsigned int idx, void *val) {
	if(idx > arr->cnt) {
		return 0;
	}

	if(idx == arr->cnt) {
		LArray_insert(arr, val);

	} else {
		LArray_usSet(arr, idx, val);
	}

	return 1;
}

static inline void* LArray_usGet(LArray *arr, unsigned int idx) {
	return arr->arr[idx];
}

static inline void* LArray_get(LArray *arr, unsigned int idx) {
	if(idx >= arr->cnt) {
		return NULL;
	}

	return LArray_usGet(arr, idx);
}

static inline void** LArray_addr(LArray *arr, unsigned int idx) {
	return &arr->arr[idx];
}

static inline int LArray_getm(LArray *arr, unsigned int idx, unsigned int num, void *mem) {
	if(idx + num > arr->cnt) num = arr->cnt - idx;

	memcpy(mem, arr->arr + idx, num * sizeof(void*));

	return num;
}


static inline void LArray_append(LArray *arr, LArray *arr2) {
	int i, n = LArray_size(arr2);

	LArray_grow(arr, n + LArray_size(arr));

	for(i=0; i<n; i++) {
		LArray_insert(arr, LArray_get(arr2, i));
	}
}



static inline void __Array_use() {
	Array_create(0);
	Array_destroy(NULL);
	Array_address(NULL, 0);
	Array_set(NULL, 0, NULL);
	Array_get(NULL, 0);
	// Array_set2(NULL, 0, NULL, 0);
	// Array_get2(NULL, 0, NULL, 0);

	LArray_create(0);
	LArray_destroy(NULL);
	LArray_remove(NULL, NULL);
	LArray_removei(NULL, 0);
	LArray_removef(NULL, 0);
	LArray_search(NULL, NULL);
	LArray_set(NULL, 0, NULL);
	LArray_get(NULL, 0);
	LArray_getm(NULL, 0, 0, NULL);
}


#endif

// static inline LArray* LArray_create(int sz);

// static inline void LArray_destroy(void *ptr);

// static inline void LArray_clear(LArray *arr, void (*dst)(void *));

// static inline unsigned int LArray_size(LArray *arr);

// static inline void LArray_insert(LArray *arr, void *val);

// static inline int LArray_search(LArray *arr, void *val);

// static inline void LArray_push(LArray *arr, void *val);

// static inline unsigned int LArray_remove(LArray *arr, void *val);

// static inline int LArray_removei(LArray *arr, unsigned int idx);

// static inline int LArray_removef(LArray *arr, unsigned int idx);

// static inline int LArray_set(LArray *arr, unsigned int idx, void *val);

// static inline void* LArray_get(LArray *arr, unsigned int idx);

// static inline void LArray_append(LArray *arr, LArray *arr2);

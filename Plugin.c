
#ifndef PLUGIN_C
#define PLUGIN_C


#if defined _WIN32

#define WIN32_LEAN_AND_MEAN

#include <windows.h>


static inline void* Plugin_load(const char *pnm) {
	return LoadLibrary(pnm);
}

static inline void* Plugin_symbol(void *hdl, const char *fnm) {
	return GetProcAddress(hdl, fnm);
}

static inline int Plugin_unload(void *hdl) {
	return FreeLibrary(hdl) != 0;
}

static inline const char* Plugin_error() {
	const char *msg = "unknown error";

	// DWORD err = GetLastError();

	return msg;
}


#elif defined __linux__

#include <dlfcn.h>


static inline void* Plugin_load(const char *pnm) {
	return dlopen(pnm, RTLD_NOW | RTLD_GLOBAL);
}

static inline void* Plugin_symbol(void *hdl, const char *fnm) {
	return dlsym(hdl, fnm);
}

static inline int Plugin_unload(void *hdl) {
	return dlclose(hdl) == 0;
}

static inline const char* Plugin_error() {
	return dlerror();
}


#else

static inline void* Plugin_load(const char *pnm) {
	return NULL;
}

static inline void* Plugin_symbol(void *hdl, const char *fnm) {
	return NULL;
}

static inline int Plugin_unload(void *hdl) {
	return 0;
}

static inline const char* Plugin_error() {
	return NULL;
}

#endif


#endif

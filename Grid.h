
#ifndef GRID_H
#define GRID_H


#include "Types.c"

static inline int Grid_setToAddr(void **a, void *v) {
	if(a == NULL) return 0;

	*a = v;

	return 1;
}

static inline void* Grid_getFromAddr(void **a) {
	return a == NULL ? NULL : *a;
}



typedef struct Grid Grid;

Grid* Grid_create(uint w, uint d, uint *s);

void Grid_destroy(void *ptr);

void Grid_size(Grid *grd, uint *s);

void* Grid_addr(Grid *grd, uint *i);

static inline int Grid_set(Grid *grd, uint *i, void *v) {
	return Grid_setToAddr((void**) Grid_addr(grd, i), v);
}

static inline void* Grid_get(Grid *grd, uint *i) {
	return Grid_getFromAddr((void**) Grid_addr(grd, i));
}

// int Grid_set(Grid *grd, uint *i, void *v);

// void* Grid_get(Grid *grd, uint *i);


static inline Grid* Grid2D_create(int w, int si, int sj) {
	return Grid_create(w, 2, (uint[]) {si, sj});
}

static inline void* Grid2D_addr(Grid *grd, int i, int j) {
	return Grid_addr(grd, (uint[]) {i, j});
}

static inline int Grid2D_set(Grid *grd, int i, int j, void *v) {
	return Grid_set(grd, (uint[]) {i, j}, v);
}

static inline void* Grid2D_get(Grid *grd, int i, int j) {
	return Grid_get(grd, (uint[]) {i, j});
}

// Grid* Grid2D_create(int w, int si, int sj);

// void* Grid2D_addr(Grid *grd, int i, int j);

// int Grid2D_set(Grid *grd, int i, int j, void *v);

// void* Grid2D_get(Grid *grd, int i, int j);


static inline Grid* Grid3D_create(int w, int si, int sj, int sk) {
	return Grid_create(w, 3, (uint[]) {si, sj, sk});
}

static inline int Grid3D_set(Grid *grd, int i, int j, int k, void *v) {
	return Grid_set(grd, (uint[]) {i, j, k}, v);
}

static inline void* Grid3D_get(Grid *grd, int i, int j, int k) {
	return Grid_get(grd, (uint[]) {i, j, k});
}

// Grid* Grid3D_create(int w, int si, int sj, int sk);

// int Grid3D_set(Grid *grd, int i, int j, int k, void *v);

// void* Grid3D_get(Grid *grd, int i, int j, int k);


static inline Grid* Grid4D_create(int w, int si, int sj, int sk, int sl) {
	return Grid_create(w, 4, (uint[]) {si, sj, sk, sl});
}

static inline int Grid4D_set(Grid *grd, int i, int j, int k, int l, void *v) {
	return Grid_set(grd, (uint[]) {i, j, k, l}, v);
}

static inline void* Grid4D_get(Grid *grd, int i, int j, int k, int l) {
	return Grid_get(grd, (uint[]) {i, j, k, l});
}

// Grid* Grid4D_create(int w, int si, int sj, int sk, int sl);

// int Grid4D_set(Grid *grd, int i, int j, int k, int l, void *v);

// void* Grid4D_get(Grid *grd, int i, int j, int k, int l);



typedef struct xGrid xGrid;

xGrid* xGrid_create(uint w, uint d, uint *s);

void xGrid_destroy(void *ptr);

void xGrid_clear(xGrid *grd, void (*term)(void *));

void xGrid_setOffset(xGrid *grd, int *o);

void xGrid_size(xGrid *grd, int *s, int *o);

int xGrid_realloc(xGrid *grd, int *i);

void* xGrid_addr(xGrid *grd, int *i);

int xGrid_set(xGrid *grd, int *i, void *v);

void* xGrid_get(xGrid *grd, int *i);

static inline xGrid* xGrid2D_create(int w, int si, int sj) {
	return xGrid_create(w, 2, (uint[]) {si, sj});
}

static inline void* xGrid2D_addr(xGrid *grd, int i, int j) {
	return xGrid_addr(grd, (int[]) {i, j});
}

static inline int xGrid2D_set(xGrid *grd, int i, int j, void *v) {
	return xGrid_set(grd, (int[]) {i, j}, v);
}

static inline void* xGrid2D_get(xGrid *grd, int i, int j) {
	return xGrid_get(grd, (int[]) {i, j});
}

// xGrid* xGrid2D_create(int w, int si, int sj);

// void* xGrid2D_addr(xGrid *grd, int i, int j);

// int xGrid2D_set(xGrid *grd, int i, int j, void *v);

// void* xGrid2D_get(xGrid *grd, int i, int j);



typedef struct GridBlock GridBlock;

GridBlock* GridBlock_create(uint w, uint d, uint *s);

void GridBlock_destroy(void *ptr);

int GridBlock_newGrid(GridBlock *blk, int *i);

void* GridBlock_addr(GridBlock *blk, int *i);

int GridBlock_set(GridBlock *blk, int *i, void *v);

void* GridBlock_get(GridBlock *blk, int *i);

int GridBlock_fset(GridBlock *blk, int *i, void *v);


static inline GridBlock* GridBlock2D_create(int w, int wi, int wj) {
	return GridBlock_create(w, 2, (uint[]) {wi, wj});
}

static inline int GridBlock2D_newGrid(GridBlock *blk, int i, int j) {
	return GridBlock_newGrid(blk, (int[]) {i, j});
}

static inline void* GridBlock2D_addr(GridBlock *blk, int i, int j) {
	return GridBlock_addr(blk, (int[]) {i, j});
}

static inline int GridBlock2D_set(GridBlock *blk, int i, int j, void *v) {
	return GridBlock_set(blk, (int[]) {i, j}, v);
}

static inline void* GridBlock2D_get(GridBlock *blk, int i, int j) {
	return GridBlock_get(blk, (int[]) {i, j});
}

static inline int GridBlock2D_fset(GridBlock *blk, int i, int j, void *v) {
	return GridBlock_fset(blk, (int[]) {i, j}, v);
}

// GridBlock* GridBlock2D_create(int w, int wi, int wj);

// int GridBlock2D_newGrid(GridBlock *blk, int i, int j);

// void* GridBlock2D_addr(GridBlock *blk, int i, int j);

// int GridBlock2D_set(GridBlock *blk, int i, int j, void *v);

// void* GridBlock2D_get(GridBlock *blk, int i, int j);

// int GridBlock2D_fset(GridBlock *blk, int i, int j, void *v);



#endif

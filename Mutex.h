
#include <time.h>



#ifndef MUTEX_H
#define MUTEX_H



#if defined _WIN32

#define WIN32_LEAN_AND_MEAN

#include <windows.h>

typedef CRITICAL_SECTION Mutex;

#elif defined __linux__

#include <pthread.h>

typedef pthread_mutex_t Mutex;

#else

#error THREAD: UNSUPPORTED PLATFORM

#endif


void Mutex_init(Mutex *mtx);

void Mutex_term(Mutex *mtx);


void Mutex_lock(Mutex *mtx);

void Mutex_unlock(Mutex *mtx);

int Mutex_tryLock(Mutex *mtx);



// DEPRECATED: Mutex IS ALREADY RECURSIVE

typedef struct recMutex {
	Mutex mtx;

	int thr;
	int cnt;

} recMutex;


void recMutex_init(recMutex *mtx);

void recMutex_term(recMutex *mtx);


void recMutex_lock(recMutex *mtx, int thr);

int recMutex_unlock(recMutex *mtx);


int recMutex_tryLock(recMutex *mtx, int thr);



typedef struct rwMutex {
	Mutex mtx;

	int w;
	int r;

} rwMutex;


void rwMutex_init(rwMutex *mtx);

void rwMutex_term(rwMutex *mtx);


int rwMutex_tryWriteLock(rwMutex *mtx);

int rwMutex_tryReadLock(rwMutex *mtx);


void rwMutex_writeLock(rwMutex *mtx);

void rwMutex_writeUnlock(rwMutex *mtx);


void rwMutex_readLock(rwMutex *mtx);

void rwMutex_readUnlock(rwMutex *mtx);



void glbMutex_lock();

void glbMutex_unlock();

int glbMutex_tryLock();


void glbMutex_init();

void glbMutex_term();


#endif


#ifndef CONTAINER_H
#define CONTAINER_H

#include "Mutex.h"

#include "Flags.h"


typedef struct Container {
	Mutex *mtx;

	Flags flg;

	int num;
	int cnt;

	void **val;

	void *ptr;
	void (*insCbk)(void *, void *);
	void (*rmvCbk)(void *, void *);

} Container;

#endif


/*

static inline void Container_init(Container *ctn);

static inline void Container_term(void *ptr);


static inline Container* Container_create();

static inline void Container_destroy(void *ptr);


static inline void Container_setMT(Container *ctn);

static inline void Container_lock(Container *ctn);

static inline void Container_unlock(Container *ctn);


static inline void Container_setFlags(Container *ctn, Flags flg);


static inline int Container_insert(Container *ctn, void *val);

static inline int Container_remove(Container *ctn, void *val);

static inline int Container_search(Container *ctn, void *val);


static inline int Container_size(Container *ctn);


static inline void* Container_geti(Container *ctn, int i);

static inline void** Container_get(Container *ctn, int *n);

static inline int Container_set(Container *ctn, int n, void **v);


static inline int Container_fill(Container *ctn, void **v, int n, int o);

static inline int Container_clear(Container *ctn);


static inline void Container_clearAndDestroy(void *ptr);


static inline void Container_setCallbacks(Container *ctn, void *ptr, void *ins, void *rmv);


static inline Container* Container_createMT();

*/

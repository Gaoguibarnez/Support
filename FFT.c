
#include <math.h>


#ifndef FFT_C
#define FFT_C


#define _FFT_PI M_PI

static inline float _FFT_cos(float x) {
	return (float) cos((double) x);
}

static inline float _FFT_sin(float x) {
	return _FFT_cos(x - _FFT_PI / 2.0f);
}


static void FFT(float *X_r, float *X_i, float *x_r, float *x_i, int n) {
	int i, j;

	const float _2piN = 2.0f * _FFT_PI / (float) n;

	for(i=0; i<n; i++) {
		float tX_r = 0.0f;
		float tX_i = 0.0f;

		const float _2pikN = _2piN * (float) i;

		for(j=0; j<n; j++) {
			float cos_ = _FFT_cos(_2pikN * (float) j);
			float sin_ = _FFT_sin(_2pikN * (float) j);

			float tx_r = x_r[j];
			float tx_i = x_i[j];

			tX_r += tx_r * cos_ + tx_i * sin_;
			tX_i += tx_i * cos_ - tx_r * sin_;
		}

		X_r[i] = tX_r;
		X_i[i] = tX_i;
	}
}

static void iFFT(float *x_r, float *x_i, float *X_r, float *X_i, int n) {
	int i, j;

	const float _2piN = 2.0f * _FFT_PI / (float) n;

	for(i=0; i<n; i++) {
		float tx_r = 0.0f;
		float tx_i = 0.0f;

		const float _2pikN = _2piN * (float) i;

		for(j=0; j<n; j++) {
			float cos_ = _FFT_cos(_2pikN * (float) j);
			float sin_ = _FFT_sin(_2pikN * (float) j);

			float tX_r = X_r[j];
			float tX_i = X_i[j];

			tx_r += tX_r * cos_ - tX_i * sin_;
			tx_i += tX_i * cos_ + tX_r * sin_;
		}

		x_r[i] = tx_r / n;
		x_i[i] = tx_i / n;
	}
}


static inline void _FFT_use() {
	FFT(NULL, NULL, NULL, NULL, 0);
	iFFT(NULL, NULL, NULL, NULL, 0);
}


#endif

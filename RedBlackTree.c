
#include <string.h>

#include <stdint.h>


#ifndef REDBLACKTREEMT_C
#define REDBLACKTREEMT_C


#ifndef REDBLACKTREE_SINGLETHREADED
#define __REDBLACKTREE_MULTITHREADED
#else
#undef __REDBLACKTREE_MULTITHREADED
#endif

#ifndef REDBLACKTREE_USESTDMEMORY
#define __REDBLACKTREE_USEMEMORY
#else
#undef __REDBLACKTREE_USEMEMORY
#endif


#if defined __REDBLACKTREE_USEMEMORY
#include "Memory.h"
#if defined REDBLACKTREE_USEUMEMORY
#define __RedBlackTree_alloc(s) Memory_ualloc(s)
#define __RedBlackTree_free(p, s) Memory_ufree(p, s)
#else
#define __RedBlackTree_alloc(s) Memory_alloc(s)
#define __RedBlackTree_free(p, s) Memory_free(p)
#endif
#else
#define __RedBlackTree_alloc(s) malloc(s)
#define __RedBlackTree_free(p, s) free(p)
#endif

#if defined __REDBLACKTREE_MULTITHREADED
#include "Mutex.h"
#endif


static int (*RedBlackTree_defaultComparator)(void *, void *) = NULL;

static int RedBlackTree_stringComparator(void *k1, void *k2);

static int RedBlackTree_noCaseStringComparator(void *k1, void *k2);


typedef struct RedBlackTree RedBlackTree;

static void RedBlackTree_term(void *ptr);

static void RedBlackTree_init(RedBlackTree *tree, int (*comparator)(void *, void *));

static void RedBlackTree_initMT(RedBlackTree *tree, int (*comparator)(void *, void *));

static RedBlackTree* RedBlackTree_create(int (*comparator)(void *, void *));

static RedBlackTree* RedBlackTree_createMT(int (*comparator)(void *, void *));

static void RedBlackTree_destroy(void *ptr);

static void RedBlackTree_clear(RedBlackTree *tree, void (*destroy_key)(void *), void (*destroy_val)(void *));

static inline void* RedBlackTree_insert(RedBlackTree *tree, void *key, void *val);

static inline void* RedBlackTree_search(RedBlackTree *tree, void *key);

static inline void** RedBlackTree_addr(RedBlackTree *tree, void *key);

static inline void* RedBlackTree_remove(RedBlackTree *tree, void *key);

static inline void RedBlackTree_iterate(RedBlackTree *tree, int (*iterator)(void *, void *, void *), void *ptr);


typedef struct RedBlackNode {
    struct RedBlackNode *lft;
    struct RedBlackNode *rgt;
    struct RedBlackNode *par;

    void *key;
    void *val;

    int is_red;

} RedBlackNode;

static inline RedBlackNode* __RedBlackNode__create(void *key, void *val, RedBlackNode *par) {
    RedBlackNode *node = (RedBlackNode*) __RedBlackTree_alloc(sizeof(RedBlackNode));

    node->lft = NULL;
    node->rgt = NULL;

    node->par = par;

    node->key = key;

    node->val = val;

    node->is_red = 0;

    return node;
}

static inline void __RedBlackNode__destroy(RedBlackNode *node) {
    if(node->lft != NULL) {
        __RedBlackNode__destroy(node->lft);
    }

    if(node->rgt != NULL) {
        __RedBlackNode__destroy(node->rgt);
    }

    __RedBlackTree_free(node, sizeof(RedBlackNode));
}

static inline void __RedBlackNode__clear(RedBlackNode *node, void (*destroy_key)(void *), void (*destroy_val)(void *)) {
    if(node->lft != NULL) {
        __RedBlackNode__clear(node->lft, destroy_key, destroy_val);

        __RedBlackNode__destroy(node->lft);

        node->lft = NULL;

        destroy_key(node->key);

        destroy_val(node->val);
    }

    if(node->rgt != NULL) {
        __RedBlackNode__clear(node->rgt, destroy_key, destroy_val);

        __RedBlackNode__destroy(node->rgt);

        node->rgt = NULL;
    }
}

static inline RedBlackNode* __RedBlackNode__rotateLeft(RedBlackNode *node) {
    RedBlackNode *root = node->rgt;

    if(node->par != NULL) {
        if(node->par->lft == node) {
            node->par->lft = root;

        } else {
            node->par->rgt = root;
        }   
    }

    root->par = node->par;

    node->rgt = root->lft;
    node->rgt->par = node;

    root->lft = node;
    node->par = root;

    return root;
}

static inline RedBlackNode* __RedBlackNode__rotateRight(RedBlackNode *node) {
    RedBlackNode *root = node->lft;

    if(node->par != NULL) {
        if(node->par->lft == node) {
            node->par->lft = root;

        } else {
            node->par->rgt = root;
        }
    }

    root->par = node->par;

    node->lft = root->rgt;
    node->lft->par = node;

    root->rgt = node;
    node->par = root;

    return root;
}

static inline void __RedBlackNode__fixConsecutiveRed(RedBlackNode *node) {
    RedBlackNode *par_par, *par_sib;

    do {
        if(node->par == NULL) {
            node->is_red = 0;

            break;
        }

        if(node->par->is_red == 0) {
            break;
        }

        par_par = node->par->par;

        if(par_par->lft == node->par) {
            par_sib = par_par->rgt;

            if(par_sib->is_red == 0) {
                if(node->par->rgt == node) {
                    node = __RedBlackNode__rotateLeft(node->par);

                    node->par->is_red = 1;

                    node->is_red = 0;

                    __RedBlackNode__rotateRight(node->par);

                } else {                
                    par_par->is_red = 1;

                    node->par->is_red = 0;

                    __RedBlackNode__rotateRight(par_par);
                }

                break;
            }

        } else {
            par_sib = par_par->lft;

            if(par_sib->is_red == 0) {
                if(node->par->lft == node) {
                    node = __RedBlackNode__rotateRight(node->par);

                    node->par->is_red = 1;

                    node->is_red = 0;

                    __RedBlackNode__rotateLeft(par_par);

                } else {
                    par_par->is_red = 1;

                    node->par->is_red = 0;

                    __RedBlackNode__rotateLeft(par_par);
                }

                break;
            }
        }

        node->par->is_red = 0;

        par_sib->is_red = 0;

        par_par->is_red = 1;

        node = par_par;

    } while(1);
}

static inline void __RedBlackNode__fixSmallerBlackDepth(RedBlackNode *node) {
    RedBlackNode *sib;

    do {
        if(node->par == NULL) {
            break;
        }

        if(node->par->lft == node) {
            sib = node->par->rgt;

            if(sib->is_red == 1) {
                node->par->is_red = 1;

                sib->is_red = 0;

                __RedBlackNode__rotateLeft(node->par);

                sib = node->par->rgt;
            }

            if(node->par->is_red == 0 && sib->is_red == 0 && sib->lft->is_red == 0 && sib->rgt->is_red == 0) {
                sib->is_red = 1;

                node = node->par;

            } else {
                if(node->par->is_red == 1 && sib->is_red == 0 && sib->lft->is_red == 0 && sib->rgt->is_red == 0) {
                    sib->is_red = 1;

                    node->par->is_red = 0;

                    break;
                }

                if(sib->is_red == 0 && sib->rgt->is_red == 0 && sib->lft->is_red == 1) {
                    sib->is_red = 1;

                    sib->lft->is_red = 0;

                    sib = __RedBlackNode__rotateRight(sib);
                }

                sib->is_red = node->par->is_red;

                node->par->is_red = 0;

                sib->rgt->is_red = 0;

                __RedBlackNode__rotateLeft(node->par);

                break;
            }

        } else {
            sib = node->par->lft;

            if(sib->is_red == 1) {
                node->par->is_red = 1;

                sib->is_red = 0;

                __RedBlackNode__rotateRight(node->par);

                sib = node->par->lft;
            }

            if(node->par->is_red == 0 && sib->is_red == 0 && sib->lft->is_red == 0 && sib->rgt->is_red == 0) {
                sib->is_red = 1;

                node = node->par;

            } else {
                if(node->par->is_red == 1 && sib->is_red == 0 && sib->lft->is_red == 0 && sib->rgt->is_red == 0) {
                    sib->is_red = 1;

                    node->par->is_red = 0;

                    break;
                }

                if(sib->is_red == 0 && sib->lft->is_red == 0 && sib->rgt->is_red == 1) {
                    sib->is_red = 1;

                    sib->rgt->is_red = 0;

                    sib = __RedBlackNode__rotateLeft(sib);
                }

                sib->is_red = node->par->is_red;

                node->par->is_red = 0;

                sib->lft->is_red = 0;

                __RedBlackNode__rotateRight(node->par);

                break;
            }
        }

    } while(1);
}

static inline void* __RedBlackNode__insert(RedBlackNode *root, int (*cmp)(void *, void *), void *key, void *val) {
    RedBlackNode *node = root;

    void *old_val = NULL;

    int c;

    do {
        if(node->lft == NULL) {
            node->lft = __RedBlackNode__create(NULL, NULL, node);
            node->rgt = __RedBlackNode__create(NULL, NULL, node);

            node->val = val;

            node->key = key;

            node->is_red = 1;

            __RedBlackNode__fixConsecutiveRed(node);

            break;
        }

        c = cmp(key, node->key);

        if(c > 0) {
            node = node->rgt;

        } else if(c < 0) {
            node = node->lft;

        } else {
            old_val = node->val;

            node->val = val;

            break;
        }

    } while(1);

    return old_val;
}

static inline void** __RedBlackNode__searchd(RedBlackNode *root, void *key) {
    RedBlackNode *node = root;

    while(node->lft != NULL) {
        if(key > node->key) {
            node = node->rgt;

        } else if(key < node->key) {
            node = node->lft;

        } else {
            return &node->val;
        }
    }

    return NULL;
}

static inline void** __RedBlackNode__search(RedBlackNode *root, int (*cmp)(void *, void *), void *key) {
    RedBlackNode *node = root;

    while(node->lft != NULL) {
        int c = cmp(key, node->key);

        if(c > 0) {
            node = node->rgt;

        } else if(c < 0) {
            node = node->lft;

        } else {
            return &node->val;
        }
    }

    return NULL;
}

static inline void* __RedBlackNode__remove(RedBlackNode *root, int (*cmp)(void *, void *), void *key) {
    RedBlackNode *desc, *node = root;

    int desc_red, c;

    void *val = NULL;

    while(node->lft != NULL) {
        c = cmp(key, node->key);

        if(c > 0) {
            node = node->rgt;

        } else if(c < 0) {
            node = node->lft;

        } else {
            val = node->val;

            desc = node->lft;

            if(desc->lft == NULL) {
                desc_red = node->rgt->is_red;

                node->val = node->rgt->val;
                node->key = node->rgt->key;

                node->lft = node->rgt->lft;

                if(node->lft != NULL) {
                    node->lft->par = node;
                }

                __RedBlackNode__destroy(desc);

                desc = node->rgt;

                node->rgt = node->rgt->rgt;

                if(node->rgt != NULL) {
                    node->rgt->par = node;
                }

                desc->lft = NULL;
                desc->rgt = NULL;

                __RedBlackNode__destroy(desc);

                if(node->is_red == 1) {
                    node->is_red = 0;

                } else if(desc_red == 0) {
                    __RedBlackNode__fixSmallerBlackDepth(node);
                }

            } else {
                while(desc->rgt->lft != NULL) {
                    desc = desc->rgt;
                }

                desc_red = desc->lft->is_red;

                node->val = desc->val;
                node->key = desc->key;

                desc->val = desc->lft->val;
                desc->key = desc->lft->key;

                node = desc->rgt;

                desc->rgt = desc->lft->rgt;

                if(desc->rgt != NULL) {
                    desc->rgt->par = desc;
                }

                __RedBlackNode__destroy(node);

                node = desc->lft;

                desc->lft = desc->lft->lft;

                if(desc->lft != NULL) {
                    desc->lft->par = desc;
                }

                node->lft = NULL;
                node->rgt = NULL;

                __RedBlackNode__destroy(node);

                if(desc->is_red == 1) {
                    desc->is_red = 0;

                } else if(desc_red == 0) {
                    __RedBlackNode__fixSmallerBlackDepth(desc);
                }
            }

            break;
        }
    }

    return val;
}

static inline int __RedBlackNode__iterate(RedBlackNode *node, int (*iterator)(void *, void *, void *), void *ptr) {
    if(node->lft != NULL) {
        if(__RedBlackNode__iterate(node->lft, iterator, ptr) == 0) {
            return 0;
        }

        if(iterator(ptr, node->key, node->val) == 0) {
            return 0;
        }

        if(__RedBlackNode__iterate(node->rgt, iterator, ptr) == 0) {
            return 0;
        }
    }

    return 1;
}



/* k1 IS THE CURRENT KEY, k2 THE STORED KEY */

static int _RedBlackTree_defaultComparator(void *k1, void *k2) {
    uintptr_t v1 = (uintptr_t) k1;
    uintptr_t v2 = (uintptr_t) k2;

    if(v1 < v2) return - 1;

    return v1 > v2 ? 1 : 0;
}

static int RedBlackTree_stringComparator(void *k1, void *k2) {
    return strcmp((char*) k1, (char*) k2);
}

static int RedBlackTree_noCaseStringComparator(void *k1, void *k2) {
    return strcasecmp((char*) k1, (char*) k2);
}


typedef struct RedBlackTree {
    struct RedBlackNode *root;

    int (*cmp)(void *, void *);

    #if defined __REDBLACKTREE_MULTITHREADED
    Mutex *mtx;
    #endif

} RedBlackTree;

static void RedBlackTree_term(void *ptr) {
    RedBlackTree *tree = (RedBlackTree*) ptr;

    __RedBlackNode__destroy(tree->root);

    #if defined __REDBLACKTREE_MULTITHREADED
    if(tree->mtx != NULL) {
        Mutex_term(tree->mtx);

        __RedBlackTree_free(tree->mtx, sizeof(Mutex));
    }
    #endif
}

static void RedBlackTree_init(RedBlackTree *tree, int (*comparator)(void *, void *)) {
    tree->root = __RedBlackNode__create(NULL, NULL, NULL);

    tree->cmp = comparator;

    #if defined __REDBLACKTREE_MULTITHREADED
    tree->mtx = NULL;
    #endif
}

static void RedBlackTree_initMT(RedBlackTree *tree, int (*comparator)(void *, void *)) {
    RedBlackTree_init(tree, comparator);

    #if defined __REDBLACKTREE_MULTITHREADED
    Mutex_init(tree->mtx = (Mutex*) __RedBlackTree_alloc(sizeof(Mutex)));
    #endif
}

static RedBlackTree* RedBlackTree_create(int (*comparator)(void *, void *)) {
    RedBlackTree *tree = (RedBlackTree*) __RedBlackTree_alloc(sizeof(RedBlackTree));

    RedBlackTree_init(tree, comparator);

    return tree;
}

static RedBlackTree* RedBlackTree_createMT(int (*comparator)(void *, void *)) {
    RedBlackTree *tree = (RedBlackTree*) __RedBlackTree_alloc(sizeof(RedBlackTree));

    RedBlackTree_initMT(tree, comparator);

    return tree;
}

static void RedBlackTree_destroy(void *ptr) {
    RedBlackTree_term(ptr);

    __RedBlackTree_free(ptr, sizeof(RedBlackTree));
}

#if defined __REDBLACKTREE_MULTITHREADED
#define __REDBLACKTREE_LOCK() if(tree->mtx != NULL) Mutex_lock(tree->mtx)
#define __REDBLACKTREE_UNLOCK() if(tree->mtx != NULL) Mutex_unlock(tree->mtx)
#else
#define __REDBLACKTREE_LOCK()
#define __REDBLACKTREE_UNLOCK()
#endif

static void __RedBlackTree_dummyDestroy(void *ptr) {
}

static void RedBlackTree_clear(RedBlackTree *tree, void (*destroy_key)(void *), void (*destroy_val)(void *)) {
    if(destroy_key == NULL) {
        destroy_key = __RedBlackTree_dummyDestroy;
    }

    if(destroy_val == NULL) {
        destroy_val = __RedBlackTree_dummyDestroy;
    }

    __REDBLACKTREE_LOCK();

    __RedBlackNode__clear(tree->root, destroy_key, destroy_val);

    tree->root->lft = NULL;
    tree->root->rgt = NULL;

    tree->root->key = NULL;
    tree->root->val = NULL;

    tree->root->is_red = 0;

    __REDBLACKTREE_UNLOCK();
}

static inline void* RedBlackTree_insert(RedBlackTree *tree, void *key, void *val) {
    __REDBLACKTREE_LOCK();

    val = __RedBlackNode__insert(tree->root, (tree->cmp != NULL) ? tree->cmp : _RedBlackTree_defaultComparator, key, val);

    while(tree->root->par != NULL) tree->root = tree->root->par;

    __REDBLACKTREE_UNLOCK();

    return val;
}

static inline void** __RedBlackTree__addr(RedBlackTree *tree, void *key) {
    if(tree->cmp == NULL) {
        return __RedBlackNode__searchd(tree->root, key);
    }

    return __RedBlackNode__search(tree->root, tree->cmp, key);
}

static inline void* RedBlackTree_search(RedBlackTree *tree, void *key) {
    __REDBLACKTREE_LOCK();

    void **addr = __RedBlackTree__addr(tree, key);

    if(addr == NULL) {
        key = NULL;

    } else {
        key = *addr;
    }

    __REDBLACKTREE_UNLOCK();

    return key;
}

static inline void** RedBlackTree_addr(RedBlackTree *tree, void *key) {
    __REDBLACKTREE_LOCK();

    void **addr = __RedBlackTree__addr(tree, key);

    __REDBLACKTREE_UNLOCK();

    return addr;
}

static inline void* RedBlackTree_remove(RedBlackTree *tree, void *key) {
    __REDBLACKTREE_LOCK();

    void *val = __RedBlackNode__remove(tree->root, (tree->cmp != NULL) ? tree->cmp : _RedBlackTree_defaultComparator, key);

    while(tree->root->par != NULL) tree->root = tree->root->par;

    __REDBLACKTREE_UNLOCK();

    return val;
}

static inline void RedBlackTree_iterate(RedBlackTree *tree, int (*iterator)(void *, void *, void *), void *ptr) {
    __REDBLACKTREE_LOCK();

    __RedBlackNode__iterate(tree->root, iterator, ptr);

    __REDBLACKTREE_UNLOCK();
}



static inline void __RedBlackTree__use() {
	RedBlackTree_defaultComparator(NULL, NULL);
	RedBlackTree_stringComparator(NULL, NULL);
	RedBlackTree_noCaseStringComparator(NULL, NULL);

    RedBlackTree_createMT(NULL);
    RedBlackTree_create(NULL);
	RedBlackTree_destroy(NULL);
	RedBlackTree_clear(NULL, NULL, NULL);
}



static int __RedBlackNode__count(RedBlackNode *node, int count_red) {
    if(node->lft == NULL) {
        return 1;
    }

    int l = __RedBlackNode__count(node->lft, count_red);
    int r = __RedBlackNode__count(node->rgt, count_red);

    if(count_red) {
        if(r > l) {
            l = r;
        }

        return l + 1;

    } else if(l < 0 || l != r) {
        return - 1;
    }

    return l + (node->is_red ? 0 : 1);
}

static inline int RedBlackTree_getBlackDepth(RedBlackTree *tree) {
    return __RedBlackNode__count(tree->root, 0);
}

static inline int RedBlackTree_getDepth(RedBlackTree *tree) {
    return __RedBlackNode__count(tree->root, 1);
}

static inline int RedBlackTree_checkRootRule(RedBlackTree *tree) {
    return tree->root->is_red == 0;
}

static int __RedBlackNode__checkConsecutiveRed(RedBlackNode *node) {
    if(node->is_red) {
        if(node->rgt->is_red) {
            return 0;
        }

        if(node->lft->is_red) {
            return 0;
        }
    }

    if(node->lft == NULL) {
        return 1;
    }

    if(!__RedBlackNode__checkConsecutiveRed(node->rgt)) {
        return 0;
    }

    return __RedBlackNode__checkConsecutiveRed(node->lft);
}

static inline int RedBlackTree_checkConsecutiveRed(RedBlackTree *tree) {
    return __RedBlackNode__checkConsecutiveRed(tree->root);
}


#endif

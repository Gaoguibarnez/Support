
#include <stdint.h>


#ifndef FLAGS_C
#define FLAGS_C


#include "Flags.h"


static inline void Flags_init(Flags *flg) {
	*flg = 0;
}


static inline void Flags_set(Flags *flg, unsigned int idx) {
	FLAGS_SET(flg, idx);
}

static inline void Flags_unset(Flags *flg, unsigned int idx) {
	FLAGS_UNSET(flg, idx);
}

static inline void Flags_toggle(Flags *flg, unsigned int idx) {
	*flg = ((*flg & (1 << idx)) != 0) ? (*flg & ~(1 << idx)) : (*flg | (1 << idx));
}

#define FLAGS_TOGGLE(flg, idx) Flags_toggle((Flags*) (flg), (unsigned int) (idx))

static inline int Flags_get(Flags *flg, unsigned int idx) {
	return FLAGS_GET(flg, idx);
}


static inline void Flags_setf(Flags *flg, Flags f) {
	FLAGS_SETF(flg, f);
}

static inline void Flags_unsetf(Flags *flg, Flags f) {
	FLAGS_UNSETF(flg, f);
}

static inline int Flags_getf(Flags *flg, Flags f) {
	return FLAGS_GETF(flg, f);
}


#endif

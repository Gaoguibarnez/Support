
// COPY FROM Vec3D.c
// RENAME Vec.D TO Vec.Dd
// RENAME vec3d TO vec3dd
// RENAME VEC3D TO VEC3Dd
// CHANGE float TO double
// CHANGE .0f TO .0
// CHANGE MATH FUNCTIONS



#include <stdio.h>

#include <math.h>


#include "Memory.h"


#include "Vec3Dd.h"


#ifndef VEC3Dd_C
#define VEC3Dd_C

#define VEC3Dd_COS cos
#define VEC3Dd_SIN sin
#define VEC3Dd_SQRT sqrt
#define VEC3Dd_ATAN2 atan2


// typedef struct Vec3Dd {
// 	double x;
// 	double y;
// 	double z;
	
// } Vec3Dd;

static inline Vec3Dd* Vec3Dd_create() {
	return (Vec3Dd*) Memory_alloc(sizeof(Vec3Dd));
}

static inline Vec3Dd* Vec3Dd_create0() {
	Vec3Dd *v = Vec3Dd_create();

	v->x = 0.0;
	v->y = 0.0;
	v->z = 0.0;

	return v;
}

static inline Vec3Dd* Vec3Dd_create1(const Vec3Dd *v) {
	Vec3Dd *u = Vec3Dd_create();

	u->x = v->x;
	u->y = v->y;
	u->z = v->z;

	return u;
}

static inline Vec3Dd* Vec3Dd_create2(double hang, double vang) {
	Vec3Dd *v = Vec3Dd_create();

	v->x = VEC3Dd_SIN(hang) * VEC3Dd_SIN(vang);
	v->y = VEC3Dd_COS(vang);
	v->z = - VEC3Dd_COS(hang) * VEC3Dd_SIN(vang);

	return v;
}

static inline Vec3Dd* Vec3Dd_create3(double x, double y, double z) {
	Vec3Dd *v = Vec3Dd_create();

	v->x = x;
	v->y = y;
	v->z = z;

	return v;
}

static inline Vec3Dd* Vec3Dd_create4(double *b) {
	Vec3Dd *v = Vec3Dd_create();

	v->x = b[0];
	v->y = b[1];
	v->z = b[2];

	return v;
}

static inline void Vec3Dd_destroy(void *v) {
	Memory_free(v);
}

static inline void Vec3Dd_print(const Vec3Dd *v) {
	printf("(%f, %f, %f)\n", v->x, v->y, v->z);
}

static inline Vec3Dd* Vec3Dd_set(Vec3Dd *v1, const Vec3Dd *v2) {
	v1->x = v2->x;
	v1->y = v2->y;
	v1->z = v2->z;

	return v1;
}

static inline Vec3Dd* Vec3Dd_set0(Vec3Dd *v) {
	v->x = 0.0;
	v->y = 0.0;
	v->z = 0.0;

	return v;
}

static inline Vec3Dd* Vec3Dd_set2(Vec3Dd *v, double hang, double vang) {
	v->x = VEC3Dd_SIN(hang) * VEC3Dd_SIN(vang);
	v->y = VEC3Dd_COS(vang);
	v->z = - VEC3Dd_COS(hang) * VEC3Dd_SIN(vang);

	return v;
}

static inline Vec3Dd* Vec3Dd_set3(Vec3Dd *v, double x, double y, double z) {
	v->x = x;
	v->y = y;
	v->z = z;

	return v;
}

static inline Vec3Dd* Vec3Dd_set4(Vec3Dd *v, double *b) {
	v->x = b[0];
	v->y = b[1];
	v->z = b[2];

	return v;
}

static inline void Vec3Dd_get(const Vec3Dd *v, double *x, double *y, double *z) {
	if(x != NULL) *x = v->x;
	if(y != NULL) *y = v->y;
	if(z != NULL) *z = v->z;
}

static inline double Vec3Dd_getX(const Vec3Dd *v) {
	return v->x;
}

static inline double Vec3Dd_getY(const Vec3Dd *v) {
	return v->y;
}

static inline double Vec3Dd_getZ(const Vec3Dd *v) {
	return v->z;
}

static inline Vec3Dd* Vec3Dd_sum(Vec3Dd *v1, const Vec3Dd *v2) {
	v1->x += v2->x;
	v1->y += v2->y;
	v1->z += v2->z;

	return v1;
}

static inline Vec3Dd* Vec3Dd_sum2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3) {
	v1->x = v2->x + v3->x;
	v1->y = v2->y + v3->y;
	v1->z = v2->z + v3->z;

	return v1;
}

static inline Vec3Dd* Vec3Dd_sumMult(Vec3Dd *v1, const Vec3Dd *v2, double k) {
	v1->x += v2->x * k;
	v1->y += v2->y * k;
	v1->z += v2->z * k;

	return v1;
}

static inline Vec3Dd* Vec3Dd_sumMult2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3, double k) {
	v1->x = v2->x + v3->x * k;
	v1->y = v2->y + v3->y * k;
	v1->z = v2->z + v3->z * k;

	return v1;
}

static inline Vec3Dd* Vec3Dd_sub(Vec3Dd *v1, const Vec3Dd *v2) {
	v1->x -= v2->x;
	v1->y -= v2->y;
	v1->z -= v2->z;

	return v1;
}

static inline Vec3Dd* Vec3Dd_sub2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3) {
	v1->x = v2->x - v3->x;
	v1->y = v2->y - v3->y;
	v1->z = v2->z - v3->z;

	return v1;
}

static inline Vec3Dd* Vec3Dd_mult(Vec3Dd *v, double f) {
	v->x *= f;
	v->y *= f;
	v->z *= f;

	return v;
}

static inline Vec3Dd* Vec3Dd_mult2(Vec3Dd *v1, const Vec3Dd *v2, double f) {
	v1->x = v2->x * f;
	v1->y = v2->y * f;
	v1->z = v2->z * f;
	
	return v1;
}

static inline double Vec3Dd_dotProduct(const Vec3Dd *v1, const Vec3Dd *v2) {
	return v1->x * v2->x + v1->y * v2->y + v1->z *v2->z;
}

static inline Vec3Dd* Vec3Dd_crossProduct(Vec3Dd *v1, const Vec3Dd *v2) {
	double xt = v1->y * v2->z - v1->z * v2->y;
	double yt = v1->z * v2->x - v1->x * v2->z;
	double zt = v1->x * v2->y - v1->y * v2->x;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3Dd* Vec3Dd_crossProduct2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3) {
	double xt = v2->y * v3->z - v2->z * v3->y;
	double yt = v2->z * v3->x - v2->x * v3->z;
	double zt = v2->x * v3->y - v2->y * v3->x;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline double Vec3Dd_length2(const Vec3Dd *v) {
	return v->x * v->x + v->y * v->y + v->z * v->z;
}

static inline double Vec3Dd_length(const Vec3Dd *v) {
	return VEC3Dd_SQRT(Vec3Dd_length2(v));
}

static inline double Vec3Dd_distance2(const Vec3Dd *v1, const Vec3Dd *v2) {
	double x = v1->x - v2->x;
	double y = v1->y - v2->y;
	double z = v1->z - v2->z;

	return x * x + y * y + z * z;
}

static inline double Vec3Dd_distance(const Vec3Dd *v1, const Vec3Dd *v2) {
	return VEC3Dd_SQRT(Vec3Dd_distance2(v1, v2));
}

static inline Vec3Dd* Vec3Dd_normalize(Vec3Dd *v) {
	double l = Vec3Dd_length(v);

	if(l > 0.0) {
		v->x /= l;
		v->y /= l;
		v->z /= l;
	}

	return v;
}

static inline Vec3Dd* Vec3Dd_normalize2(Vec3Dd *v1, const Vec3Dd *v2) {
	double l = Vec3Dd_length(v2);

	if(l > 0.0) {
		v1->x = v2->x / l;
		v1->y = v2->y / l;
		v1->z = v2->z / l;

	} else {
		v1->x = 0.0;
		v1->y = 0.0;
		v1->z = 0.0;
	}

	return v1;
}

static inline Vec3Dd* Vec3Dd_normalize3(Vec3Dd *v, double *l) {
	*l = Vec3Dd_length(v);

	if(*l > 0.0) {
		v->x /= *l;
		v->y /= *l;
		v->z /= *l;
	}

	return v;
}

static inline Vec3Dd* Vec3Dd_normalize4(Vec3Dd *v1, const Vec3Dd *v2, double *l) {
	*l = Vec3Dd_length(v2);

	if(*l > 0.0) {
		v1->x = v2->x / *l;
		v1->y = v2->y / *l;
		v1->z = v2->z / *l;
		
	} else {
		v1->x = 0.0;
		v1->y = 0.0;
		v1->z = 0.0;
	}

	return v1;
}

static inline Vec3Dd* Vec3Dd_transform(Vec3Dd *v, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double xt = v->x * x->x + v->y * y->x + v->z * z->x;
	double yt = v->x * x->y + v->y * y->y + v->z * z->y;
	double zt = v->x * x->z + v->y * y->z + v->z * z->z;

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3Dd* Vec3Dd_transform2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double xt = v2->x * x->x + v2->y * y->x + v2->z * z->x;
	double yt = v2->x * x->y + v2->y * y->y + v2->z * z->y;
	double zt = v2->x * x->z + v2->y * y->z + v2->z * z->z;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3Dd* Vec3Dd_transform3(Vec3Dd *v, const Vec3Dd *p, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double xt = p->x + v->x * x->x + v->y * y->x + v->z * z->x;
	double yt = p->y + v->x * x->y + v->y * y->y + v->z * z->y;
	double zt = p->z + v->x * x->z + v->y * y->z + v->z * z->z;

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3Dd* Vec3Dd_transform4(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *p, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double xt = p->x + v2->x * x->x + v2->y * y->x + v2->z * z->x;
	double yt = p->y + v2->x * x->y + v2->y * y->y + v2->z * z->y;
	double zt = p->z + v2->x * x->z + v2->y * y->z + v2->z * z->z;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline double Vec3Dd_det(const Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3) {
	return v1->x * v2->y * v3->z + v1->y * v2->z * v3->x + v1->z * v2->x * v3->y - v1->z * v2->y * v3->x - v1->y * v2->x * v3->z - v1->x * v2->z * v3->y;
}

static inline Vec3Dd* Vec3Dd_untransform(Vec3Dd *v, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double d = Vec3Dd_det(x, y, z);

	double xt = Vec3Dd_det(v, y, z) / d;
	double yt = Vec3Dd_det(x, v, z) / d;
	double zt = Vec3Dd_det(x, y, v) / d;

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3Dd* Vec3Dd_untransform2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double d = Vec3Dd_det(x, y, z);

	double xt = Vec3Dd_det(v2, y, z) / d;
	double yt = Vec3Dd_det(x, v2, z) / d;
	double zt = Vec3Dd_det(x, y, v2) / d;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3Dd* Vec3Dd_untransform3(Vec3Dd *v, const Vec3Dd *p, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	v->x = v->x - p->x;
	v->y = v->y - p->y;
	v->z = v->z - p->z;

	double d = Vec3Dd_det(x, y, z);

	double xt = Vec3Dd_det(v, y, z) / d;
	double yt = Vec3Dd_det(x, v, z) / d;
	double zt = Vec3Dd_det(x, y, v) / d;

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3Dd* Vec3Dd_untransform4(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *p, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	v1->x = v2->x - p->x;
	v1->y = v2->y - p->y;
	v1->z = v2->z - p->z;

	double d = Vec3Dd_det(x, y, z);

	double xt = Vec3Dd_det(v1, y, z) / d;
	double yt = Vec3Dd_det(x, v1, z) / d;
	double zt = Vec3Dd_det(x, y, v1) / d;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3Dd* Vec3Dd_fastUntransform(Vec3Dd *v, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double xt = Vec3Dd_dotProduct(v, x) / Vec3Dd_dotProduct(x, x);
	double yt = Vec3Dd_dotProduct(v, y) / Vec3Dd_dotProduct(y, y);
	double zt = Vec3Dd_dotProduct(v, z) / Vec3Dd_dotProduct(z, z);

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3Dd* Vec3Dd_fastUntransform2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double xt = Vec3Dd_dotProduct(v2, x) / Vec3Dd_dotProduct(x, x);
	double yt = Vec3Dd_dotProduct(v2, y) / Vec3Dd_dotProduct(y, y);
	double zt = Vec3Dd_dotProduct(v2, z) / Vec3Dd_dotProduct(z, z);

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3Dd* Vec3Dd_fasterUntransform(Vec3Dd *v, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double xt = Vec3Dd_dotProduct(v, x);
	double yt = Vec3Dd_dotProduct(v, y);
	double zt = Vec3Dd_dotProduct(v, z);

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3Dd* Vec3Dd_fasterUntransform2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z) {
	double xt = Vec3Dd_dotProduct(v2, x);
	double yt = Vec3Dd_dotProduct(v2, y);
	double zt = Vec3Dd_dotProduct(v2, z);

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3Dd* Vec3Dd_rotate(Vec3Dd *v, const Vec3Dd *n, double a) {
	double n_l = Vec3Dd_length(n);
	if(n_l <= 0.0f) return v;

	double c = VEC3Dd_COS(a);
	double s = VEC3Dd_SIN(a);
	double c1 = 1.0 - c;

	double n_x = n->x / n_l;
	double n_y = n->y / n_l;
	double n_z = n->z / n_l;

	double x = v->x * (n_x * n_x * c1 + c) + v->y * (n_x * n_y * c1 - n_z * s) + v->z * (n_x * n_z * c1 + n_y * s);
	double y = v->x * (n_x * n_y * c1 + n_z * s) + v->y * (n_y * n_y * c1 + c) + v->z * (n_y * n_z * c1 - n_x * s);
	double z = v->x * (n_x * n_z * c1 - n_y * s) + v->y * (n_y * n_z * c1 + n_x * s) + v->z * (n_z * n_z * c1 + c);

	v->x = x;
	v->y = y;
	v->z = z;

	return v;
}

static inline Vec3Dd* Vec3Dd_rotate2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *n, double a) {
	double n_l = Vec3Dd_length(n);
	if(n_l <= 0.0f) return Vec3Dd_set(v1, v2);

	double c = VEC3Dd_COS(a);
	double s = VEC3Dd_SIN(a);
	double c1 = 1.0 - c;

	double n_x = n->x / n_l;
	double n_y = n->y / n_l;
	double n_z = n->z / n_l;

	double x = v2->x * (n_x * n_x * c1 + c) + v2->y * (n_x * n_y * c1 - n_z * s) + v2->z * (n_x * n_z * c1 + n_y * s);
	double y = v2->x * (n_x * n_y * c1 + n_z * s) + v2->y * (n_y * n_y * c1 + c) + v2->z * (n_y * n_z * c1 - n_x * s);
	double z = v2->x * (n_x * n_z * c1 - n_y * s) + v2->y * (n_y * n_z * c1 + n_x * s) + v2->z * (n_z * n_z * c1 + c);

	v1->x = x;
	v1->y = y;
	v1->z = z;

	return v1;
}


// typedef struct Vec2Dd {
// 	double x;
// 	double y;
	
// } Vec2Dd;

static inline Vec2Dd* Vec2Dd_create() {
	return (Vec2Dd*) Memory_alloc(sizeof(Vec2Dd));
}

static inline Vec2Dd* Vec2Dd_create0() {
	Vec2Dd *u = Vec2Dd_create();

	u->x = 0.0;
	u->y = 0.0;

	return u;
}

static inline Vec2Dd* Vec2Dd_create1(const Vec2Dd *v) {
	Vec2Dd *u = Vec2Dd_create();

	u->x = v->x;
	u->y = v->y;

	return u;
}

static inline Vec2Dd* Vec2Dd_create2(double x, double y) {
	Vec2Dd *v = Vec2Dd_create();

	v->x = x;
	v->y = y;

	return v;
}

static inline void Vec2Dd_destroy(void *v) {
	Memory_free(v);
}

static inline void Vec2Dd_print(const Vec2Dd *v) {
	printf("(%f, %f)\n", v->x, v->y);
}

static inline Vec2Dd* Vec2Dd_set(Vec2Dd *v1, const Vec2Dd *v2) {
	v1->x = v2->x;
	v1->y = v2->y;

	return v1;
}

static inline Vec2Dd* Vec2Dd_set0(Vec2Dd *v) {
	v->x = 0.0;
	v->y = 0.0;

	return v;
}

static inline Vec2Dd* Vec2Dd_set2(Vec2Dd *v, double x, double y) {
	v->x = x;
	v->y = y;

	return v;
}

static inline void Vec2Dd_get(const Vec2Dd *v, double *x, double *y) {
	if(x != NULL) *x = v->x;
	if(y != NULL) *y = v->y;
}

static inline double Vec2Dd_getX(const Vec2Dd *v) {
	return v->x;
}

static inline double Vec2Dd_getY(const Vec2Dd *v) {
	return v->y;
}

static inline Vec2Dd* Vec2Dd_mult(Vec2Dd *v, double k) {
	v->x *= k;
	v->y *= k;

	return v;
}

static inline Vec2Dd* Vec2Dd_sumMult(Vec2Dd *v1, const Vec2Dd *v2, double k) {
	v1->x += v2->x * k;
	v1->y += v2->y * k;

	return v1;
}

static inline Vec3Dd* Vec2Dd_toVec3Dd(Vec3Dd *u, const Vec2Dd *v) {
	u->x = v->x;
	u->y = v->y;
	u->z = 0.0;

	return u;
}

static inline Vec2Dd* Vec3Dd_toVec2Dd(Vec2Dd *u, const Vec3Dd *v) {
	u->x = v->x;
	u->y = v->y;

	return u;
}

static inline Vec2Dd* Vec2Dd_rotate2(Vec2Dd *u, const Vec2Dd *v, double a) {
	double c = VEC3Dd_COS(a);
	double s = VEC3Dd_SIN(a);

	double x = v->x * c - v->y * s;
	double y = v->x * s + v->y * c;

	u->x = x;
	u->y = y;

	return u;
}

static inline Vec2Dd* Vec2Dd_rotate(Vec2Dd *u, double a) {
	return Vec2Dd_rotate2(u, u, a);
}

static inline Vec2Dd* Vec2Dd_rotate90(Vec2Dd *u, const Vec2Dd *v) {
	double y = v->x;

	u->x = - v->y;
	u->y = y;

	return u;
}

static inline double Vec2Dd_dotProduct(const Vec2Dd *u, const Vec2Dd *v) {
	return u->x * v->x + u->y * v->y;
}

static inline double Vec2Dd_crossProduct(const Vec2Dd *u, const Vec2Dd *v) {
	return u->x * v->y - u->y * v->x;
}

static inline double Vec2Dd_length2(const Vec2Dd *v) {
	return Vec2Dd_dotProduct(v, v);
}

static inline double Vec2Dd_length(const Vec2Dd *v) {
	return VEC3Dd_SQRT(Vec2Dd_length2(v));
}

static inline double Vec2Dd_distance2(const Vec2Dd *v1, const Vec2Dd *v2) {
	double x = v1->x - v2->x;
	double y = v1->y - v2->y;

	return x * x + y * y;
}

static inline double Vec2Dd_distance(const Vec2Dd *v1, const Vec2Dd *v2) {
	return VEC3Dd_SQRT(Vec2Dd_distance2(v1, v2));
}

static inline double Vec2Dd_direction(const Vec2Dd *v) {
	return VEC3Dd_ATAN2(v->y, v->x);
}

static inline Vec2Dd* Vec2Dd_transform2(Vec2Dd *v1, const Vec2Dd *v2, const Vec2Dd *x, const Vec2Dd *y) {
	double xt = v2->x * x->x + v2->y * y->x;
	double yt = v2->x * x->y + v2->y * y->y;

	v1->x = xt;
	v1->y = yt;

	return v1;
}

static inline Vec2Dd* Vec2Dd_sum(Vec2Dd *v, const Vec2Dd *u) {
	v->x += u->x;
	v->y += u->y;

	return v;
}

static inline Vec2Dd* Vec2Dd_sum2(Vec2Dd *v, const Vec2Dd *u, const Vec2Dd *w) {
	v->x = u->x + w->x;
	v->y = u->y + w->y;

	return v;
}

static inline Vec2Dd* Vec2Dd_sub(Vec2Dd *v, const Vec2Dd *u) {
	v->x -= u->x;
	v->y -= u->y;

	return v;
}

static inline Vec2Dd* Vec2Dd_sub2(Vec2Dd *v, const Vec2Dd *u, const Vec2Dd *w) {
	v->x = u->x - w->x;
	v->y = u->y - w->y;

	return v;
}


// typedef struct Vec4Dd {
// 	double w;
// 	double x;
// 	double y;
// 	double z;

// } Vec4Dd;

static inline Vec4Dd* Vec4Dd_inverse(Vec4Dd *v) {
	double l = v->w * v->w + v->x * v->x + v->y * v->y + v->z * v->z;

	v->w *= 1.0 / l;
	v->x *= - 1.0 / l;
	v->y *= - 1.0 / l;
	v->z *= - 1.0 / l;

	return v;
}

static inline Vec4Dd* Vec4Dd_inverse2(Vec4Dd *v1, const Vec4Dd *v2) {
	double l = v2->w * v2->w + v2->x * v2->x + v2->y * v2->y + v2->z * v2->z;

	v1->w = v2->w / l;
	v1->x = - v2->x / l;
	v1->y = - v2->y / l;
	v1->z = - v2->z / l;

	return v1;
}

static inline Vec4Dd* Vec4Dd_prod(Vec4Dd *v1, const Vec4Dd *v2) {
	double w = v1->w * v2->w - v1->x * v2->x - v1->y * v2->y - v1->z * v2->z;
	double x = v1->y * v2->z - v1->z * v2->y + v1->x * v2->w + v2->x * v1->w;
	double y = v1->z * v2->x - v1->x * v2->z + v1->y * v2->w + v2->y * v1->w;
	double z = v1->x * v2->y - v1->y * v2->x + v1->z * v2->w + v2->z * v1->w;

	v1->w = w;
	v1->x = x;
	v1->y = y;
	v1->z = z;

	return v1;
}

static inline Vec4Dd* Vec4Dd_prod2(Vec4Dd *v1, const Vec4Dd *v2, const Vec4Dd *v3) {
	double w = v2->w * v3->w - v2->x * v3->x - v2->y * v3->y - v2->z * v3->z;
	double x = v2->y * v3->z - v2->z * v3->y + v2->x * v3->w + v3->x * v2->w;
	double y = v2->z * v3->x - v2->x * v3->z + v2->y * v3->w + v3->y * v2->w;
	double z = v2->x * v3->y - v2->y * v3->x + v2->z * v3->w + v3->z * v2->w;

	v1->w = w;
	v1->x = x;
	v1->y = y;
	v1->z = z;

	return v1;
}

static inline double* Vec4Dd_getRotationMatrix(const Vec4Dd *v, double *m) {
	double xx = v->x * v->x;
	double yy = v->y * v->y;
	double zz = v->z * v->z;

	double wx = v->w * v->x;
	double wy = v->w * v->y;
	double wz = v->w * v->z;
	double xy = v->x * v->y;
	double xz = v->x * v->z;
	double yz = v->y * v->z;

	double *t = m;

	*t++ = 1.0 - 2.0 * (yy + zz);
	*t++ = 2.0 * (xy - wz);
	*t++ = 2.0 * (xz + wy);
	*t++ = 2.0 * (xy + wz);
	*t++ = 1.0 - 2.0 * (xx + zz);
	*t++ = 2.0 * (yz - wx);
	*t++ = 2.0 * (xz - wy);
	*t++ = 2.0 * (yz + wx);
	*t++ = 1.0 - 2.0 * (xx + yy);

	return m;
}


static const Vec3Dd __vec3dd_0 = {0.0, 0.0, 0.0};
static const Vec3Dd __vec3dd_x = {1.0, 0.0, 0.0};
static const Vec3Dd __vec3dd_y = {0.0, 1.0, 0.0};
static const Vec3Dd __vec3dd_z = {0.0, 0.0, 1.0};

#define VEC3Dd_ZERO (&__vec3dd_0)
#define VEC3Dd_0 VEC3Dd_ZERO
#define VEC3Dd_X (&__vec3dd_x)
#define VEC3Dd_Y (&__vec3dd_y)
#define VEC3Dd_Z (&__vec3dd_z)


#endif

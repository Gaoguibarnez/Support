
#ifndef POWI_C
#define POWI_C

#include <stdint.h>


static intptr_t powli(intptr_t a, intptr_t b) {
	if(b == 0) {
		if(a == 0) {
			/* 0^0 is undefined */
			return 0;
		}

		return 1;
	}

	if(b < 0) {
		if(a == 0) {
			/* 0^(-) is undefined */
			return 0;
		}

		if(a > 1 || a < - 1) return 0;

		if(a > 0) return 1;

		return (- b) % 2 ? - 1 : 1;
	}

	if(a > 1 || a < - 1) {
		intptr_t c = 1;

		while(1) {
			if(b % 2 == 0) {
				a *= a;
				b /= 2;

			} else if(b == 1) {
				return c * a;

			} else {
				c *= a;
				--b;
			}
		}
	}

	if(a > 0) return 1;

	if(a == 0) return 0;

	return b % 2 ? - 1 : 1;
}

static int powi(int a, int b) {
	return (int) powli((intptr_t) a, (intptr_t) b);
}


static double fast_exp(double x) {
	x = 1.0 + x / 64.0;

	x *= x;
	x *= x;
	x *= x;
	x *= x;
	x *= x;
	x *= x;

	return x;
}


static inline void __powi_use() {
	powi(0, 0);
	fast_exp(0.0);
}


#endif

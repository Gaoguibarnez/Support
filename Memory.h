
#include <stddef.h>


#ifndef MEMORY_H
#define MEMORY_H


#define MEMORY_Ki (1024)
#define MEMORY_Mi (1024 * MEMORY_Ki)
#define MEMORY_Gi (1024 * MEMORY_Mi)

const char* Memory_toString(size_t m);


void* Memory_ualloc(size_t size);

void Memory_ufree(void *ptr, size_t size);

void* Memory_urealloc(void *ptr, size_t size, size_t old_size);


#ifndef MEMORY_DISABLE

void* Memory_alloc(size_t size);

int Memory_freeWithReturn(void *ptr);


void* Memory_realloc(void *ptr, size_t size);

void* Memory_alloc0(size_t size);

char* Memory_allocs(const char *str);

#else

#include <string.h>

#include <stdlib.h>

static inline void* Memory_alloc(s) {
	return malloc(s);
}

static inline int Memory_freeWithReturn(void *p) {
	free(p);

	return 1;
}

static inline void* Memory_realloc(void *ptr, size_t size) {
	return realloc(ptr, size);
}

static inline void* Memory_alloc0(size_t size) {
	return calloc(s, 1);
}

static char* Memory_allocs(const char *str) {
	void *ptr = malloc(1 + strlen(str));

	if(ptr) strcpy(ptr, str);

	return ptr;
}

#endif


static inline void Memory_free(void *ptr);

static inline void Memory_free(void *ptr) {
	Memory_freeWithReturn(ptr);
}


#define MEMORY_CAST(TYPE, VAL) (TYPE) (VAL)

#define MEMORY_ALLOC(TYPE, NUM) (TYPE *) Memory_alloc((NUM) * sizeof(TYPE))

#define MEMORY_ALLOC0(TYPE, NUM) (TYPE *) Memory_alloc0((NUM) * sizeof(TYPE))

#define MEMORY_ALLOC1(TYPE) MEMORY_ALLOC(TYPE, 1)


size_t Memory_getUsedMemory();

size_t Memory_getMaxUsedMemory();

size_t Memory_getTotalUsedMemory();


void Memory_clear();


void Memory_init();

void Memory_term();


void Memory_printFootprint(const char *id);


#endif

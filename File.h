
#ifndef FILE_H
#define FILE_H


typedef struct DIRECTORY DIRECTORY;

void File_override(FILE* (*open)(const char *, const char *), DIRECTORY* (*opendir)(const char *));


int File_isAbsolutePath(const char *path);

const char* File_getName(const char *path);


#define FILE_PATH_SIZE 1024

int File_setRoot(const char *root);

int File_pushPath(const char *path);

int File_popPath();


char* File_getPath(char *buff, int size, const char *path);


FILE* File_open(const char *path, const char *mode);

int File_close(FILE *f);


int File_mkdir(const char *path);


FILE* File_pushPathAndOpen(const char *path, const char *mode);



int File_isDir(const char *path);


DIRECTORY* DIRECTORY_create(unsigned int n, const char *e[]);

const char* DIRECTORY_entry(DIRECTORY *dir, unsigned int idx);


DIRECTORY* File_opendir(const char *path);

void File_closedir(DIRECTORY *dir);


#endif

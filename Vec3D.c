
#include <stdio.h>

#include <math.h>


#include "Memory.h"


#include "Vec3D.h"


#ifndef VEC3D_C
#define VEC3D_C

#define VEC3D_COS cosf
#define VEC3D_SIN sinf
#define VEC3D_SQRT sqrtf
#define VEC3D_ATAN2 atan2f


// typedef struct Vec3D {
// 	float x;
// 	float y;
// 	float z;
	
// } Vec3D;

static inline Vec3D* Vec3D_create() {
	return (Vec3D*) Memory_alloc(sizeof(Vec3D));
}

static inline Vec3D* Vec3D_create0() {
	Vec3D *v = Vec3D_create();

	v->x = 0.0f;
	v->y = 0.0f;
	v->z = 0.0f;

	return v;
}

static inline Vec3D* Vec3D_create1(const Vec3D *v) {
	Vec3D *u = Vec3D_create();

	u->x = v->x;
	u->y = v->y;
	u->z = v->z;

	return u;
}

static inline Vec3D* Vec3D_create2(float hang, float vang) {
	Vec3D *v = Vec3D_create();

	v->x = VEC3D_SIN(hang) * VEC3D_SIN(vang);
	v->y = VEC3D_COS(vang);
	v->z = - VEC3D_COS(hang) * VEC3D_SIN(vang);

	return v;
}

static inline Vec3D* Vec3D_create3(float x, float y, float z) {
	Vec3D *v = Vec3D_create();

	v->x = x;
	v->y = y;
	v->z = z;

	return v;
}

static inline Vec3D* Vec3D_create4(float *b) {
	Vec3D *v = Vec3D_create();

	v->x = b[0];
	v->y = b[1];
	v->z = b[2];

	return v;
}

static inline void Vec3D_destroy(void *v) {
	Memory_free(v);
}

static inline void Vec3D_print(const Vec3D *v) {
	printf("(%f, %f, %f)\n", v->x, v->y, v->z);
}

static inline Vec3D* Vec3D_set(Vec3D *v1, const Vec3D *v2) {
	v1->x = v2->x;
	v1->y = v2->y;
	v1->z = v2->z;

	return v1;
}

static inline Vec3D* Vec3D_set0(Vec3D *v) {
	v->x = 0.0f;
	v->y = 0.0f;
	v->z = 0.0f;

	return v;
}

static inline Vec3D* Vec3D_set2(Vec3D *v, float hang, float vang) {
	v->x = VEC3D_SIN(hang) * VEC3D_SIN(vang);
	v->y = VEC3D_COS(vang);
	v->z = - VEC3D_COS(hang) * VEC3D_SIN(vang);

	return v;
}

static inline Vec3D* Vec3D_set3(Vec3D *v, float x, float y, float z) {
	v->x = x;
	v->y = y;
	v->z = z;

	return v;
}

static inline Vec3D* Vec3D_set4(Vec3D *v, float *b) {
	v->x = b[0];
	v->y = b[1];
	v->z = b[2];

	return v;
}

static inline void Vec3D_get(const Vec3D *v, float *x, float *y, float *z) {
	if(x != NULL) *x = v->x;
	if(y != NULL) *y = v->y;
	if(z != NULL) *z = v->z;
}

static inline float Vec3D_getX(const Vec3D *v) {
	return v->x;
}

static inline float Vec3D_getY(const Vec3D *v) {
	return v->y;
}

static inline float Vec3D_getZ(const Vec3D *v) {
	return v->z;
}

static inline Vec3D* Vec3D_sum(Vec3D *v1, const Vec3D *v2) {
	v1->x += v2->x;
	v1->y += v2->y;
	v1->z += v2->z;

	return v1;
}

static inline Vec3D* Vec3D_sum2(Vec3D *v1, const Vec3D *v2, const Vec3D *v3) {
	v1->x = v2->x + v3->x;
	v1->y = v2->y + v3->y;
	v1->z = v2->z + v3->z;

	return v1;
}

static inline Vec3D* Vec3D_sumMult(Vec3D *v1, const Vec3D *v2, float k) {
	v1->x += v2->x * k;
	v1->y += v2->y * k;
	v1->z += v2->z * k;

	return v1;
}

static inline Vec3D* Vec3D_sumMult2(Vec3D *v1, const Vec3D *v2, const Vec3D *v3, float k) {
	v1->x = v2->x + v3->x * k;
	v1->y = v2->y + v3->y * k;
	v1->z = v2->z + v3->z * k;

	return v1;
}

static inline Vec3D* Vec3D_sub(Vec3D *v1, const Vec3D *v2) {
	v1->x -= v2->x;
	v1->y -= v2->y;
	v1->z -= v2->z;

	return v1;
}

static inline Vec3D* Vec3D_sub2(Vec3D *v1, const Vec3D *v2, const Vec3D *v3) {
	v1->x = v2->x - v3->x;
	v1->y = v2->y - v3->y;
	v1->z = v2->z - v3->z;

	return v1;
}

static inline Vec3D* Vec3D_mult(Vec3D *v, float f) {
	v->x *= f;
	v->y *= f;
	v->z *= f;

	return v;
}

static inline Vec3D* Vec3D_mult2(Vec3D *v1, const Vec3D *v2, float f) {
	v1->x = v2->x * f;
	v1->y = v2->y * f;
	v1->z = v2->z * f;
	
	return v1;
}

static inline float Vec3D_dotProduct(const Vec3D *v1, const Vec3D *v2) {
	return v1->x * v2->x + v1->y * v2->y + v1->z *v2->z;
}

static inline Vec3D* Vec3D_crossProduct(Vec3D *v1, const Vec3D *v2) {
	float xt = v1->y * v2->z - v1->z * v2->y;
	float yt = v1->z * v2->x - v1->x * v2->z;
	float zt = v1->x * v2->y - v1->y * v2->x;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3D* Vec3D_crossProduct2(Vec3D *v1, const Vec3D *v2, const Vec3D *v3) {
	float xt = v2->y * v3->z - v2->z * v3->y;
	float yt = v2->z * v3->x - v2->x * v3->z;
	float zt = v2->x * v3->y - v2->y * v3->x;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline float Vec3D_length2(const Vec3D *v) {
	return v->x * v->x + v->y * v->y + v->z * v->z;
}

static inline float Vec3D_length(const Vec3D *v) {
	return VEC3D_SQRT(Vec3D_length2(v));
}

static inline float Vec3D_distance2(const Vec3D *v1, const Vec3D *v2) {
	float x = v1->x - v2->x;
	float y = v1->y - v2->y;
	float z = v1->z - v2->z;

	return x * x + y * y + z * z;
}

static inline float Vec3D_distance(const Vec3D *v1, const Vec3D *v2) {
	return VEC3D_SQRT(Vec3D_distance2(v1, v2));
}

static inline Vec3D* Vec3D_normalize(Vec3D *v) {
	float l = Vec3D_length(v);

	if(l > 0.0f) {
		v->x /= l;
		v->y /= l;
		v->z /= l;
	}

	return v;
}

static inline Vec3D* Vec3D_normalize2(Vec3D *v1, const Vec3D *v2) {
	float l = Vec3D_length(v2);

	if(l > 0.0f) {
		v1->x = v2->x / l;
		v1->y = v2->y / l;
		v1->z = v2->z / l;

	} else {
		v1->x = 0.0f;
		v1->y = 0.0f;
		v1->z = 0.0f;
	}

	return v1;
}

static inline Vec3D* Vec3D_normalize3(Vec3D *v, float *l) {
	*l = Vec3D_length(v);

	if(*l > 0.0f) {
		v->x /= *l;
		v->y /= *l;
		v->z /= *l;
	}

	return v;
}

static inline Vec3D* Vec3D_normalize4(Vec3D *v1, const Vec3D *v2, float *l) {
	*l = Vec3D_length(v2);

	if(*l > 0.0f) {
		v1->x = v2->x / *l;
		v1->y = v2->y / *l;
		v1->z = v2->z / *l;
		
	} else {
		v1->x = 0.0f;
		v1->y = 0.0f;
		v1->z = 0.0f;
	}

	return v1;
}

static inline Vec3D* Vec3D_transform(Vec3D *v, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float xt = v->x * x->x + v->y * y->x + v->z * z->x;
	float yt = v->x * x->y + v->y * y->y + v->z * z->y;
	float zt = v->x * x->z + v->y * y->z + v->z * z->z;

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3D* Vec3D_transform2(Vec3D *v1, const Vec3D *v2, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float xt = v2->x * x->x + v2->y * y->x + v2->z * z->x;
	float yt = v2->x * x->y + v2->y * y->y + v2->z * z->y;
	float zt = v2->x * x->z + v2->y * y->z + v2->z * z->z;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3D* Vec3D_transform3(Vec3D *v, const Vec3D *p, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float xt = p->x + v->x * x->x + v->y * y->x + v->z * z->x;
	float yt = p->y + v->x * x->y + v->y * y->y + v->z * z->y;
	float zt = p->z + v->x * x->z + v->y * y->z + v->z * z->z;

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3D* Vec3D_transform4(Vec3D *v1, const Vec3D *v2, const Vec3D *p, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float xt = p->x + v2->x * x->x + v2->y * y->x + v2->z * z->x;
	float yt = p->y + v2->x * x->y + v2->y * y->y + v2->z * z->y;
	float zt = p->z + v2->x * x->z + v2->y * y->z + v2->z * z->z;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline float Vec3D_det(const Vec3D *v1, const Vec3D *v2, const Vec3D *v3) {
	return v1->x * v2->y * v3->z + v1->y * v2->z * v3->x + v1->z * v2->x * v3->y - v1->z * v2->y * v3->x - v1->y * v2->x * v3->z - v1->x * v2->z * v3->y;
}

static inline Vec3D* Vec3D_untransform(Vec3D *v, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float d = Vec3D_det(x, y, z);

	float xt = Vec3D_det(v, y, z) / d;
	float yt = Vec3D_det(x, v, z) / d;
	float zt = Vec3D_det(x, y, v) / d;

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3D* Vec3D_untransform2(Vec3D *v1, const Vec3D *v2, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float d = Vec3D_det(x, y, z);

	float xt = Vec3D_det(v2, y, z) / d;
	float yt = Vec3D_det(x, v2, z) / d;
	float zt = Vec3D_det(x, y, v2) / d;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3D* Vec3D_untransform3(Vec3D *v, const Vec3D *p, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	v->x = v->x - p->x;
	v->y = v->y - p->y;
	v->z = v->z - p->z;

	float d = Vec3D_det(x, y, z);

	float xt = Vec3D_det(v, y, z) / d;
	float yt = Vec3D_det(x, v, z) / d;
	float zt = Vec3D_det(x, y, v) / d;

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3D* Vec3D_untransform4(Vec3D *v1, const Vec3D *v2, const Vec3D *p, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	v1->x = v2->x - p->x;
	v1->y = v2->y - p->y;
	v1->z = v2->z - p->z;

	float d = Vec3D_det(x, y, z);

	float xt = Vec3D_det(v1, y, z) / d;
	float yt = Vec3D_det(x, v1, z) / d;
	float zt = Vec3D_det(x, y, v1) / d;

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3D* Vec3D_fastUntransform(Vec3D *v, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float xt = Vec3D_dotProduct(v, x) / Vec3D_dotProduct(x, x);
	float yt = Vec3D_dotProduct(v, y) / Vec3D_dotProduct(y, y);
	float zt = Vec3D_dotProduct(v, z) / Vec3D_dotProduct(z, z);

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3D* Vec3D_fastUntransform2(Vec3D *v1, const Vec3D *v2, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float xt = Vec3D_dotProduct(v2, x) / Vec3D_dotProduct(x, x);
	float yt = Vec3D_dotProduct(v2, y) / Vec3D_dotProduct(y, y);
	float zt = Vec3D_dotProduct(v2, z) / Vec3D_dotProduct(z, z);

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3D* Vec3D_fasterUntransform(Vec3D *v, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float xt = Vec3D_dotProduct(v, x);
	float yt = Vec3D_dotProduct(v, y);
	float zt = Vec3D_dotProduct(v, z);

	v->x = xt;
	v->y = yt;
	v->z = zt;

	return v;
}

static inline Vec3D* Vec3D_fasterUntransform2(Vec3D *v1, const Vec3D *v2, const Vec3D *x, const Vec3D *y, const Vec3D *z) {
	float xt = Vec3D_dotProduct(v2, x);
	float yt = Vec3D_dotProduct(v2, y);
	float zt = Vec3D_dotProduct(v2, z);

	v1->x = xt;
	v1->y = yt;
	v1->z = zt;

	return v1;
}

static inline Vec3D* Vec3D_rotate(Vec3D *v, const Vec3D *n, float a) {
	float n_l = Vec3D_length(n);
	if(n_l <= 0.0f) return v;

	float c = VEC3D_COS(a);
	float s = VEC3D_SIN(a);
	float c1 = 1.0f - c;

	float n_x = n->x / n_l;
	float n_y = n->y / n_l;
	float n_z = n->z / n_l;

	float x = v->x * (n_x * n_x * c1 + c) + v->y * (n_x * n_y * c1 - n_z * s) + v->z * (n_x * n_z * c1 + n_y * s);
	float y = v->x * (n_x * n_y * c1 + n_z * s) + v->y * (n_y * n_y * c1 + c) + v->z * (n_y * n_z * c1 - n_x * s);
	float z = v->x * (n_x * n_z * c1 - n_y * s) + v->y * (n_y * n_z * c1 + n_x * s) + v->z * (n_z * n_z * c1 + c);

	v->x = x;
	v->y = y;
	v->z = z;

	return v;
}

static inline Vec3D* Vec3D_rotate2(Vec3D *v1, const Vec3D *v2, const Vec3D *n, float a) {
	float n_l = Vec3D_length(n);
	if(n_l <= 0.0f) return Vec3D_set(v1, v2);

	float c = VEC3D_COS(a);
	float s = VEC3D_SIN(a);
	float c1 = 1.0f - c;

	float n_x = n->x / n_l;
	float n_y = n->y / n_l;
	float n_z = n->z / n_l;

	float x = v2->x * (n_x * n_x * c1 + c) + v2->y * (n_x * n_y * c1 - n_z * s) + v2->z * (n_x * n_z * c1 + n_y * s);
	float y = v2->x * (n_x * n_y * c1 + n_z * s) + v2->y * (n_y * n_y * c1 + c) + v2->z * (n_y * n_z * c1 - n_x * s);
	float z = v2->x * (n_x * n_z * c1 - n_y * s) + v2->y * (n_y * n_z * c1 + n_x * s) + v2->z * (n_z * n_z * c1 + c);

	v1->x = x;
	v1->y = y;
	v1->z = z;

	return v1;
}


// typedef struct Vec2D {
// 	float x;
// 	float y;
	
// } Vec2D;

static inline Vec2D* Vec2D_create() {
	return (Vec2D*) Memory_alloc(sizeof(Vec2D));
}

static inline Vec2D* Vec2D_create0() {
	Vec2D *u = Vec2D_create();

	u->x = 0.0f;
	u->y = 0.0f;

	return u;
}

static inline Vec2D* Vec2D_create1(const Vec2D *v) {
	Vec2D *u = Vec2D_create();

	u->x = v->x;
	u->y = v->y;

	return u;
}

static inline Vec2D* Vec2D_create2(float x, float y) {
	Vec2D *v = Vec2D_create();

	v->x = x;
	v->y = y;

	return v;
}

static inline void Vec2D_destroy(void *v) {
	Memory_free(v);
}

static inline void Vec2D_print(const Vec2D *v) {
	printf("(%f, %f)\n", v->x, v->y);
}

static inline Vec2D* Vec2D_set(Vec2D *v1, const Vec2D *v2) {
	v1->x = v2->x;
	v1->y = v2->y;

	return v1;
}

static inline Vec2D* Vec2D_set0(Vec2D *v) {
	v->x = 0.0f;
	v->y = 0.0f;

	return v;
}

static inline Vec2D* Vec2D_set2(Vec2D *v, float x, float y) {
	v->x = x;
	v->y = y;

	return v;
}

static inline void Vec2D_get(const Vec2D *v, float *x, float *y) {
	if(x != NULL) *x = v->x;
	if(y != NULL) *y = v->y;
}

static inline float Vec2D_getX(const Vec2D *v) {
	return v->x;
}

static inline float Vec2D_getY(const Vec2D *v) {
	return v->y;
}

static inline Vec2D* Vec2D_mult(Vec2D *v, float k) {
	v->x *= k;
	v->y *= k;

	return v;
}

static inline Vec2D* Vec2D_sumMult(Vec2D *v1, const Vec2D *v2, float k) {
	v1->x += v2->x * k;
	v1->y += v2->y * k;

	return v1;
}

static inline Vec3D* Vec2D_toVec3D(Vec3D *u, const Vec2D *v) {
	u->x = v->x;
	u->y = v->y;
	u->z = 0.0f;

	return u;
}

static inline Vec2D* Vec3D_toVec2D(Vec2D *u, const Vec3D *v) {
	u->x = v->x;
	u->y = v->y;

	return u;
}

static inline Vec2D* Vec2D_rotate2(Vec2D *u, const Vec2D *v, float a) {
	float c = VEC3D_COS(a);
	float s = VEC3D_SIN(a);

	float x = v->x * c - v->y * s;
	float y = v->x * s + v->y * c;

	u->x = x;
	u->y = y;

	return u;
}

static inline Vec2D* Vec2D_rotate(Vec2D *u, float a) {
	return Vec2D_rotate2(u, u, a);
}

static inline Vec2D* Vec2D_rotate90(Vec2D *u, const Vec2D *v) {
	float y = v->x;

	u->x = - v->y;
	u->y = y;

	return u;
}

static inline float Vec2D_dotProduct(const Vec2D *u, const Vec2D *v) {
	return u->x * v->x + u->y * v->y;
}

static inline float Vec2D_crossProduct(const Vec2D *u, const Vec2D *v) {
	return u->x * v->y - u->y * v->x;
}

static inline float Vec2D_length2(const Vec2D *v) {
	return Vec2D_dotProduct(v, v);
}

static inline float Vec2D_length(const Vec2D *v) {
	return VEC3D_SQRT(Vec2D_length2(v));
}

static inline float Vec2D_distance2(const Vec2D *v1, const Vec2D *v2) {
	float x = v1->x - v2->x;
	float y = v1->y - v2->y;

	return x * x + y * y;
}

static inline float Vec2D_distance(const Vec2D *v1, const Vec2D *v2) {
	return VEC3D_SQRT(Vec2D_distance2(v1, v2));
}

static inline float Vec2D_direction(const Vec2D *v) {
	return VEC3D_ATAN2(v->y, v->x);
}

static inline Vec2D* Vec2D_transform2(Vec2D *v1, const Vec2D *v2, const Vec2D *x, const Vec2D *y) {
	float xt = v2->x * x->x + v2->y * y->x;
	float yt = v2->x * x->y + v2->y * y->y;

	v1->x = xt;
	v1->y = yt;

	return v1;
}

static inline Vec2D* Vec2D_sum(Vec2D *v, const Vec2D *u) {
	v->x += u->x;
	v->y += u->y;

	return v;
}

static inline Vec2D* Vec2D_sum2(Vec2D *v, const Vec2D *u, const Vec2D *w) {
	v->x = u->x + w->x;
	v->y = u->y + w->y;

	return v;
}

static inline Vec2D* Vec2D_sub(Vec2D *v, const Vec2D *u) {
	v->x -= u->x;
	v->y -= u->y;

	return v;
}

static inline Vec2D* Vec2D_sub2(Vec2D *v, const Vec2D *u, const Vec2D *w) {
	v->x = u->x - w->x;
	v->y = u->y - w->y;

	return v;
}


// typedef struct Vec4D {
// 	float w;
// 	float x;
// 	float y;
// 	float z;

// } Vec4D;

static inline Vec4D* Vec4D_inverse(Vec4D *v) {
	float l = v->w * v->w + v->x * v->x + v->y * v->y + v->z * v->z;

	v->w *= 1.0f / l;
	v->x *= - 1.0f / l;
	v->y *= - 1.0f / l;
	v->z *= - 1.0f / l;

	return v;
}

static inline Vec4D* Vec4D_inverse2(Vec4D *v1, const Vec4D *v2) {
	float l = v2->w * v2->w + v2->x * v2->x + v2->y * v2->y + v2->z * v2->z;

	v1->w = v2->w / l;
	v1->x = - v2->x / l;
	v1->y = - v2->y / l;
	v1->z = - v2->z / l;

	return v1;
}

static inline Vec4D* Vec4D_prod(Vec4D *v1, const Vec4D *v2) {
	float w = v1->w * v2->w - v1->x * v2->x - v1->y * v2->y - v1->z * v2->z;
	float x = v1->y * v2->z - v1->z * v2->y + v1->x * v2->w + v2->x * v1->w;
	float y = v1->z * v2->x - v1->x * v2->z + v1->y * v2->w + v2->y * v1->w;
	float z = v1->x * v2->y - v1->y * v2->x + v1->z * v2->w + v2->z * v1->w;

	v1->w = w;
	v1->x = x;
	v1->y = y;
	v1->z = z;

	return v1;
}

static inline Vec4D* Vec4D_prod2(Vec4D *v1, const Vec4D *v2, const Vec4D *v3) {
	float w = v2->w * v3->w - v2->x * v3->x - v2->y * v3->y - v2->z * v3->z;
	float x = v2->y * v3->z - v2->z * v3->y + v2->x * v3->w + v3->x * v2->w;
	float y = v2->z * v3->x - v2->x * v3->z + v2->y * v3->w + v3->y * v2->w;
	float z = v2->x * v3->y - v2->y * v3->x + v2->z * v3->w + v3->z * v2->w;

	v1->w = w;
	v1->x = x;
	v1->y = y;
	v1->z = z;

	return v1;
}

static inline float* Vec4D_getRotationMatrix(const Vec4D *v, float *m) {
	float xx = v->x * v->x;
	float yy = v->y * v->y;
	float zz = v->z * v->z;

	float wx = v->w * v->x;
	float wy = v->w * v->y;
	float wz = v->w * v->z;
	float xy = v->x * v->y;
	float xz = v->x * v->z;
	float yz = v->y * v->z;

	float *t = m;

	*t++ = 1.0f - 2.0f * (yy + zz);
	*t++ = 2.0f * (xy - wz);
	*t++ = 2.0f * (xz + wy);
	*t++ = 2.0f * (xy + wz);
	*t++ = 1.0f - 2.0f * (xx + zz);
	*t++ = 2.0f * (yz - wx);
	*t++ = 2.0f * (xz - wy);
	*t++ = 2.0f * (yz + wx);
	*t++ = 1.0f - 2.0f * (xx + yy);

	return m;
}


static const Vec3D __vec3d_0 = {0.0f, 0.0f, 0.0f};
static const Vec3D __vec3d_x = {1.0f, 0.0f, 0.0f};
static const Vec3D __vec3d_y = {0.0f, 1.0f, 0.0f};
static const Vec3D __vec3d_z = {0.0f, 0.0f, 1.0f};

#define VEC3D_ZERO (&__vec3d_0)
#define VEC3D_0 VEC3D_ZERO
#define VEC3D_X (&__vec3d_x)
#define VEC3D_Y (&__vec3d_y)
#define VEC3D_Z (&__vec3d_z)


#endif


#ifndef STACKMT_C
#define STACKMT_C


#ifndef STACK_SINGLETHREADED
#define __STACK_MULTITHREADED
#else
#undef __STACK_MULTITHREADED
#endif

#ifndef STACK_USESTDMEMORY
#define __STACK_USEMEMORY
#else
#undef __STACK_USEMEMORY
#endif


#if defined __STACK_USEMEMORY
#include "Memory.h"
#define __Stack_alloc Memory_alloc
#define __Stack_free Memory_free
#else
#define __Stack_alloc malloc
#define __Stack_free free
#endif

#if defined __STACK_MULTITHREADED
#include "Mutex.h"
#endif


#include "RecycleBin.c"


typedef struct Stack Stack;

static void Stack_term(void *ptr);

static void Stack_init(Stack *stk);

static void Stack_initMT(Stack *stk);

static Stack* Stack_create();

static Stack* Stack_createMT();

static void Stack_destroy(void *ptr);

static void Stack_push(Stack *stk, void *v);

static void* Stack_pop(Stack *stk);

static void* Stack_next(Stack *stk);

#define Stack_getNext Stack_next

static inline int Stack_hasNext(Stack *stk);



typedef struct _StackNode_ {
    void *v;

    struct _StackNode_ *nxt;

} _StackNode_;


typedef struct Stack {
    _StackNode_ *top;

    #if defined __STACK_MULTITHREADED
    Mutex *mtx;
    #endif

    RecycleBin *bin;

} Stack;

static void __destroyStackNode(void *ptr) {
    Memory_free(ptr);
}

static void Stack_term(void *ptr) {
    Stack *stk = (Stack*) ptr;

    while(Stack_hasNext(stk)) Stack_pop(stk);

    #if defined __STACK_MULTITHREADED
    if(stk->mtx != NULL) {
        Mutex_term(stk->mtx);

        __Stack_free(stk->mtx);
    }
    #endif

    if(stk->bin != NULL) {
        RecycleBin_term(stk->bin, __destroyStackNode);

        __Stack_free(stk->bin);
    }
}

static void Stack_init(Stack *stk) {
    stk->top = NULL;

    #if defined __STACK_MULTITHREADED
    stk->mtx = NULL;
    #endif

    stk->bin = NULL;
}

static void Stack_initMT(Stack *stk) {
    Stack_init(stk);

    #if defined __STACK_MULTITHREADED
    Mutex_init(stk->mtx = __Stack_alloc(sizeof(Mutex)));
    #endif
}

static inline Stack* Stack_create() {
    Stack *stk = (Stack*) __Stack_alloc(sizeof(Stack));

    Stack_init(stk);

    return stk;
}

static inline Stack* Stack_createMT() {
    Stack *stk = (Stack*) __Stack_alloc(sizeof(Stack));

    Stack_initMT(stk);

    return stk;
}

static void Stack_destroy(void *ptr) {
    Stack_term(ptr);

    __Stack_free(ptr);
}

static void Stack_recycle(Stack *stk, int num) {
    if(stk->bin == NULL) RecycleBin_init(stk->bin = __Stack_alloc(num * sizeof(void*) + sizeof(RecycleBin)), num);
}

#if defined __STACK_MULTITHREADED
#define __STACK_LOCK() if(stk->mtx != NULL) Mutex_lock(stk->mtx)
#define __STACK_UNLOCK() if(stk->mtx != NULL) Mutex_unlock(stk->mtx)

#else
#define __STACK_LOCK()
#define __STACK_UNLOCK()
#endif

static inline void* _Stack_createNode(Stack *stk) {
    void *node = NULL;

    if(stk->bin != NULL) node = RecycleBin_get(stk->bin);

    return node ? node : Memory_alloc(sizeof(_StackNode_));
}

static inline void _Stack_destroyNode(Stack *stk, void *node) {
    int s_f = 1;

    if(stk->bin != NULL) {
        if(RecycleBin_add(stk->bin, node)) s_f = 0;
    }

    if(s_f) Memory_free(node);
}

static void Stack_push(Stack *stk, void *v) {
    _StackNode_ *nd = _Stack_createNode(stk);

    __STACK_LOCK();

    nd->v = v;

    nd->nxt = stk->top;

    stk->top = nd;

    __STACK_UNLOCK();
}

static void* Stack_pop(Stack *stk) {
    void *v = NULL;

    __STACK_LOCK();

    if(stk->top != NULL) {
        _StackNode_ *tmp = stk->top;

        v = tmp->v;

        stk->top = tmp->nxt;

        _Stack_destroyNode(stk, tmp);
    }

    __STACK_UNLOCK();

    return v;
}

static void* Stack_peek(Stack *stk, int idx) {
    void *v = NULL;

    __STACK_LOCK();

    _StackNode_ *node = stk->top;

    while(node != NULL) {
        if(idx <= 0) {
            v = node->v;

            break;
        }

        --idx;

        node = node->nxt;
    }

    __STACK_UNLOCK();

    return v;
}

static void* Stack_next(Stack *stk) {
    __STACK_LOCK();

    if(stk->top != NULL) {
        return stk->top->v;
    }

    __STACK_UNLOCK();

    return NULL;
}

static inline int Stack_hasNext(Stack *stk) {
    return stk->top != NULL;
}


static inline void __Stack__use() {
    Stack_createMT();
    Stack_create();
    Stack_destroy(NULL);
    Stack_recycle(NULL, 0);
    Stack_push(NULL, NULL);
    Stack_pop(NULL);
    Stack_peek(NULL, 0);
    Stack_next(NULL);
    Stack_hasNext(NULL);
}


#endif

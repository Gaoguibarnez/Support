
// COPY FROM Vec3D.h
// RENAME Vec.D TO Vec.Dd
// RENAME VEC3D TO VEC3Dd
// CHANGE float TO double



#ifndef VEC3Dd_H
#define VEC3Dd_H


typedef struct Vec3Dd {
	double x;
	double y;
	double z;
	
} Vec3Dd;

typedef struct Vec2Dd {
	double x;
	double y;
	
} Vec2Dd;

typedef struct Vec4Dd {
	double w;
	double x;
	double y;
	double z;
	
} Vec4Dd;


/*

static inline Vec3Dd* Vec3Dd_create();

static inline Vec3Dd* Vec3Dd_create0();

static inline Vec3Dd* Vec3Dd_create1(const Vec3Dd *v);

static inline Vec3Dd* Vec3Dd_create2(double hang, double vang);

static inline Vec3Dd* Vec3Dd_create3(double x, double y, double z);

static inline Vec3Dd* Vec3Dd_create4(double *b);

static inline void Vec3Dd_destroy(void *v);

static inline void Vec3Dd_print(const Vec3Dd *v);

static inline Vec3Dd* Vec3Dd_set(Vec3Dd *v1, const Vec3Dd *v2);

static inline Vec3Dd* Vec3Dd_set0(Vec3Dd *v);

static inline Vec3Dd* Vec3Dd_set2(Vec3Dd *v, double hang, double vang);

static inline Vec3Dd* Vec3Dd_set3(Vec3Dd *v, double x, double y, double z);

static inline Vec3Dd* Vec3Dd_set4(Vec3Dd *v, double *b);

static inline void Vec3Dd_get(const Vec3Dd *v, double *x, double *y, double *z);

static inline double Vec3Dd_getX(const Vec3Dd *v);

static inline double Vec3Dd_getY(const Vec3Dd *v);

static inline double Vec3Dd_getZ(const Vec3Dd *v);

static inline Vec3Dd* Vec3Dd_sum(Vec3Dd *v1, const Vec3Dd *v2);

static inline Vec3Dd* Vec3Dd_sum2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3);

static inline Vec3Dd* Vec3Dd_sumMult(Vec3Dd *v1, const Vec3Dd *v2, double k);

static inline Vec3Dd* Vec3Dd_sumMult2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3, double k);

static inline Vec3Dd* Vec3Dd_sub(Vec3Dd *v1, const Vec3Dd *v2);

static inline Vec3Dd* Vec3Dd_sub2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3);

static inline Vec3Dd* Vec3Dd_mult(Vec3Dd *v, double f);

static inline Vec3Dd* Vec3Dd_mult2(Vec3Dd *v1, const Vec3Dd *v2, double f);

static inline double Vec3Dd_dotProduct(const Vec3Dd *v1, const Vec3Dd *v2);

static inline Vec3Dd* Vec3Dd_crossProduct(Vec3Dd *v1, const Vec3Dd *v2);

static inline Vec3Dd* Vec3Dd_crossProduct2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3);

static inline double Vec3Dd_length(const Vec3Dd *v);

static inline double Vec3Dd_distance(const Vec3Dd *v1, const Vec3Dd *v2);

static inline double Vec3Dd_distance2(const Vec3Dd *v1, const Vec3Dd *v2);

static inline Vec3Dd* Vec3Dd_normalize(Vec3Dd *v);

static inline Vec3Dd* Vec3Dd_normalize2(Vec3Dd *v1, const Vec3Dd *v2);

static inline Vec3Dd* Vec3Dd_normalize3(Vec3Dd *v, double *l);

static inline Vec3Dd* Vec3Dd_normalize4(Vec3Dd *v1, const Vec3Dd *v2, double *l);

static inline Vec3Dd* Vec3Dd_transform(Vec3Dd *v, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_transform2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_transform3(Vec3Dd *v, const Vec3Dd *p, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_transform4(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *p, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_untransform(Vec3Dd *v, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_untransform2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_untransform3(Vec3Dd *v, const Vec3Dd *p, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_untransform4(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *p, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_fastUntransform(Vec3Dd *v, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_fastUntransform2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_fasterUntransform(Vec3Dd *v, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_fasterUntransform2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *x, const Vec3Dd *y, const Vec3Dd *z);

static inline Vec3Dd* Vec3Dd_rotate(Vec3Dd *v, const Vec3Dd *n, double a);

static inline Vec3Dd* Vec3Dd_rotate2(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *n, double a);


static inline Vec2Dd* Vec2Dd_create();

static inline Vec2Dd* Vec2Dd_create0();

static inline Vec2Dd* Vec2Dd_create1(const Vec2Dd *v);

static inline Vec2Dd* Vec2Dd_create2(double x, double y);

static inline void Vec2Dd_destroy(void *v);

static inline void Vec2Dd_print(const Vec2Dd *v);

static inline Vec2Dd* Vec2Dd_set(Vec2Dd *v1, const Vec2Dd *v2);

static inline Vec2Dd* Vec2Dd_set0(Vec2Dd *v);

static inline Vec2Dd* Vec2Dd_set2(Vec2Dd *v, double x, double y);

static inline void Vec2Dd_get(const Vec2Dd *v, double *x, double *y);

static inline double Vec2Dd_getX(const Vec2Dd *v);

static inline double Vec2Dd_getY(const Vec2Dd *v);

static inline Vec2Dd* Vec2Dd_mult(Vec2Dd *v, double k);

static inline Vec2Dd* Vec2Dd_sumMult(Vec2Dd *v1, const Vec2Dd *v2, double k);

static inline Vec3Dd* Vec2Dd_toVec3Dd(Vec3Dd *u, const Vec2Dd *v);

static inline Vec2Dd* Vec3Dd_toVec2Dd(Vec2Dd *u, const Vec3Dd *v);

static inline Vec2Dd* Vec2Dd_rotate2(Vec2Dd *u, const Vec2Dd *v, double a);

static inline Vec2Dd* Vec2Dd_rotate(Vec2Dd *u, double a);

static inline Vec2Dd* Vec2Dd_rotate90(Vec2Dd *u, const Vec2Dd *v);

static inline double Vec2Dd_dotProduct(const Vec2Dd *u, const Vec2Dd *v);

static inline double Vec2Dd_crossProduct(const Vec2Dd *u, const Vec2Dd *v);

static inline double Vec2Dd_length2(const Vec2Dd *v);

static inline double Vec2Dd_length(const Vec2Dd *v);

static inline double Vec2Dd_direction(const Vec2Dd *v);

static inline Vec2Dd* Vec2Dd_sum(Vec2Dd *v, const Vec2Dd *u);

static inline Vec2Dd* Vec2Dd_sum2(Vec2Dd *v, const Vec2Dd *u, const Vec2Dd *w);


static inline Vec4Dd* Vec4Dd_inverse(Vec4Dd *v);

static inline Vec4Dd* Vec4Dd_inverse2(Vec4Dd *v1, const Vec4Dd *v2);

static inline Vec4Dd* Vec4Dd_prod(Vec4Dd *v1, const Vec4Dd *v2);

static inline Vec4Dd* Vec4Dd_prod2(Vec4Dd *v1, const Vec4Dd *v2, const Vec4Dd *v3);

static inline double* Vec4Dd_getRotationMatrix(const Vec4Dd *v, double *m);

*/


#endif

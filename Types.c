
#include <stdint.h>

#include <limits.h>


#ifndef TYPES_C
#define TYPES_C


typedef unsigned int uint;

typedef uint Type_t;

#define TYPE_ID_VOID ((Type_t) 0x0)
#define TYPE_ID_BOOL ((Type_t) 0x1)
#define TYPE_ID_CHR ((Type_t) 0x2)
#define TYPE_ID_INT ((Type_t) 0x3)
#define TYPE_ID_LONG ((Type_t) 0x4)
#define TYPE_ID_FLT ((Type_t) 0x5)
#define TYPE_ID_DBL ((Type_t) 0x6)
#define TYPE_ID_PTR ((Type_t) 0x7)
#define TYPE_ID_STR ((Type_t) 0x8)

#define TYPE_ID_UINT ((Type_t) 0x12)
#define TYPE_ID_UCHR ((Type_t) 0x13)
#define TYPE_ID_ULONG ((Type_t) 0x14)

#define TYPE_ID_TYPE ((Type_t) 0x18)

#define TYPE_ID_CHAR TYPE_ID_CHR
#define TYPE_ID_UCHAR TYPE_ID_UCHR

#define TYPE_ID_NEW ((Type_t) 0x20)
#define TYPE_ID_INV ((Type_t) ~0x0)

#define TYPE_DIMOFF (CHAR_BIT * sizeof(Type_t) - 4)
#define TYPE_DIMBITS ((Type_t) 0xF)
#define TYPE_KEYBITS ((Type_t) ~(TYPE_DIMBITS << TYPE_DIMOFF))

#define POINTER_BIT (CHAR_BIT * sizeof(uintptr_t))


static inline Type_t Type_getID(Type_t type) {
	return type & ~(TYPE_DIMBITS << TYPE_DIMOFF);
}

static inline uint Type_getDim(Type_t type) {
	return (uint) (type >> TYPE_DIMOFF) & TYPE_DIMBITS;
}

static inline Type_t Type_setDim(Type_t type, uint dim) {
	Type_t bits = ((Type_t) dim & TYPE_DIMBITS) << TYPE_DIMOFF;

	return bits | (type & TYPE_KEYBITS);
}

static inline Type_t Type_modDim(Type_t type, int dim) {
	return Type_setDim(type, Type_getDim(type) + dim);
}

static inline Type_t Type_setID(Type_t type, Type_t id) {
	return Type_setDim(id, Type_getDim(type));
}

static inline int Type_isPrimitive(Type_t type) {
	if(Type_getDim(type) > 0) {
		return 0;
	}

	return Type_getID(type) < TYPE_ID_NEW;
}


union Types {
	void *p;

	double d;
	float f;

	int i;
	uint u;

	intptr_t l;
	uintptr_t ul;

	char c;
	unsigned char uc;
};


#define TYPES_DEFINETYPEFUNCTIONS(TypeNameLC, TypeNameUC, TypeDecl, TypeID)	\
	\
static inline void* TypeNameLC ## ToPtr(TypeDecl v) {	\
	union Types tps = {(void*) 0};	\
	\
	tps.TypeID = v;	\
	\
	return tps.p;	\
}	\
	\
static inline TypeDecl ptrTo ## TypeNameUC(void *p) {	\
	union Types tps = {p};	\
	\
	return (TypeDecl) tps.TypeID;	\
}	\


TYPES_DEFINETYPEFUNCTIONS(char, Char, char, c)
TYPES_DEFINETYPEFUNCTIONS(int, Int, int, i)
TYPES_DEFINETYPEFUNCTIONS(flt, Flt, float, f)
TYPES_DEFINETYPEFUNCTIONS(dbl, Dbl, double, d)

TYPES_DEFINETYPEFUNCTIONS(uchr, Uchr, unsigned char, uc)
TYPES_DEFINETYPEFUNCTIONS(uint, Uint, uint, u)


// TYPES_DEFINETYPEFUNCTIONS(long, Long, intptr_t, l);

static inline void* longToPtr(intptr_t v) {
	return (void*) v;
}

static inline intptr_t ptrToLong(void *p) {
	return (intptr_t) p;
}


static inline void* ulongToPtr(uintptr_t v) {
	return (void*) v;
}

static inline intptr_t ptrToUlong(void *p) {
	return (uintptr_t) p;
}


#define ptrToPtr(a) (a)


#define TypeToPtr(a) uintToPtr(a)
#define ptrToType(a) ptrToUint(a)


#define TYPES_CONTRACT(a, b) a ## To ## b

#define ctop TYPES_CONTRACT(char, Ptr)
#define itop TYPES_CONTRACT(int, Ptr)
#define ftop TYPES_CONTRACT(flt, Ptr)
#define dtop TYPES_CONTRACT(dbl, Ptr)

#define ptoc ptrToChar
#define ptoi ptrToInt
#define ptof ptrToFlt
#define ptod ptrToDbl

#define utop uintToPtr
#define ptou ptrToUint

#define Ttop TypeToPtr
#define ptoT ptrToType


#define TYPE_ALIGNOF(type) ((size_t) (offsetof(struct { char c; type d; }, d)))



#ifndef TYPES_DISABLESIZETEST

static void (*_test_use)(void *) = NULL;

static int _test_dbl_sz_[(sizeof(void*) >= sizeof(double)) ? 1 : - 1];
static int _test_flt_sz_[(sizeof(void*) >= sizeof(float)) ? 1 : - 1];
static int _test_long_sz_[(sizeof(void*) >= sizeof(intptr_t)) ? 1 : - 1];
static int _test_int_sz_[(sizeof(void*) >= sizeof(int)) ? 1 : - 1];
static int _test_chr_sz_[(sizeof(void*) >= sizeof(char)) ? 1 : - 1];

static int _test_ulong_sz_[(sizeof(void*) >= sizeof(uintptr_t)) ? 1 : - 1];
static int _test_uint_sz_[(sizeof(void*) >= sizeof(uint)) ? 1 : - 1];
static int _test_uchr_sz_[(sizeof(void*) >= sizeof(unsigned char)) ? 1 : - 1];

static int _test_int_uint_cast_[(- 1 == (int) (uint) - 1) ? 1 : - 1];

static inline void _Types_use_() {
	_test_use(_test_dbl_sz_);
	_test_use(_test_flt_sz_);
	_test_use(_test_long_sz_);
	_test_use(_test_int_sz_);
	_test_use(_test_chr_sz_);

	_test_use(_test_ulong_sz_);
	_test_use(_test_uint_sz_);
	_test_use(_test_uchr_sz_);

	_test_use(_test_int_uint_cast_);
}

#endif


#endif


#include <stdint.h>


#ifndef FLAGS_H
#define FLAGS_H


#define FLAGS_NUMFLAGS 64

typedef uint64_t Flags;


#define FLAGS_SET(flg, idx) (*((Flags*) (flg)) |= (1 << (idx)))

#define FLAGS_UNSET(flg, idx) (*((Flags*) (flg)) &= ~(1 << (idx)))

#define FLAGS_GET(flg, idx) ((*((Flags*) (flg)) & (1 << (idx))) != 0)


#define FLAGS_SETF(flg, f) (*((Flags*) (flg)) |= ((Flags) (f)))

#define FLAGS_UNSETF(flg, f) (*((Flags*) (flg)) &= ~((Flags) (f)))

#define FLAGS_GETF(flg, f) ((*((Flags*) (flg)) & ((Flags) (f))) != 0)


#endif

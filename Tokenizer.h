
#ifndef TOKENIZER_H
#define TOKENIZER_H


#include "Queue.h"



#define TOKEN_ID_FIRST (1 << CHAR_BIT)

#define TOKEN_ID_EQ (TOKEN_ID_FIRST + 1)
#define TOKEN_ID_GE (TOKEN_ID_FIRST + 2)
#define TOKEN_ID_LE (TOKEN_ID_FIRST + 3)
#define TOKEN_ID_NE (TOKEN_ID_FIRST + 4)

#define TOKEN_ID_DIVATT (TOKEN_ID_FIRST + 8)
#define TOKEN_ID_POWATT (TOKEN_ID_FIRST + 9)
#define TOKEN_ID_MULATT (TOKEN_ID_FIRST + 10)
#define TOKEN_ID_MODATT (TOKEN_ID_FIRST + 11)
#define TOKEN_ID_SUMATT (TOKEN_ID_FIRST + 12)
#define TOKEN_ID_SUBATT (TOKEN_ID_FIRST + 13)
#define TOKEN_ID_ANDATT (TOKEN_ID_FIRST + 14)
#define TOKEN_ID_ORATT (TOKEN_ID_FIRST + 15)
#define TOKEN_ID_XORATT (TOKEN_ID_FIRST + 16)
#define TOKEN_ID_BANDATT (TOKEN_ID_FIRST + 17)
#define TOKEN_ID_BORATT (TOKEN_ID_FIRST + 18)
#define TOKEN_ID_BXORATT (TOKEN_ID_FIRST + 19)

#define TOKEN_ID_INCR (TOKEN_ID_FIRST + 24)
#define TOKEN_ID_DECR (TOKEN_ID_FIRST + 25)

#define TOKEN_ID_AND (TOKEN_ID_FIRST + 26)
#define TOKEN_ID_OR (TOKEN_ID_FIRST + 27)
#define TOKEN_ID_XOR (TOKEN_ID_FIRST + 28)

#define TOKEN_ID_DEREF (TOKEN_ID_FIRST + 32)
#define TOKEN_ID_VA (TOKEN_ID_FIRST + 33)

#define TOKEN_ID_NAME (TOKEN_ID_FIRST + 36)
#define TOKEN_ID_INT (TOKEN_ID_FIRST + 37)
#define TOKEN_ID_FLOAT (TOKEN_ID_FIRST + 38)
#define TOKEN_ID_HEXINT (TOKEN_ID_FIRST + 39)

#define TOKEN_ID_CHAR (TOKEN_ID_FIRST + 42)
#define TOKEN_ID_STRING (TOKEN_ID_FIRST + 43)



typedef struct Token {
	const char *src;
	int ln;

	int id;
	char val[];

} Token;

static inline const char* Token_source(Token *tkn) {
	return tkn->src;
}

static inline int Token_line(Token *tkn) {
	return tkn->ln;
}

static inline int Token_identifier(Token *tkn) {
	return tkn->id;
}

static inline const char* Token_value(Token *tkn) {
	return tkn->val;
}



void Tokenizer_destroyQueue(void *ptr);

Queue* Tokenizer_parse(const char *src);


#endif


#ifndef ptrar_c
#define ptrar_c


#include <stdint.h>

static inline int ptrcmp(void *a, void *b) {
    uintptr_t v1 = (uintptr_t) a;
    uintptr_t v2 = (uintptr_t) b;

    if(v1 < v2) return - 1;

    return v1 > v2 ? 1 : 0;
}

static inline void* ptrsum(void *p, int v) {
	return (void*) (((char*) p) + v);
}


#endif


#ifndef BLOCKFILE_H
#define BLOCKFILE_H


typedef struct BlockFile BlockFile;

BlockFile* BlockFile_createLeaf(const char *txt);

BlockFile* BlockFile_create(int size);

BlockFile* BlockFile_createLeaff(const char *fmt, ...);

void BlockFile_destroy(void *ptr);

void BlockFile_setName(BlockFile *blk, const char *name);

char* BlockFile_getName(BlockFile *blk);

int BlockFile_isLeaf(BlockFile *blk);

char* BlockFile_getText(BlockFile *blk);

BlockFile* BlockFile_search(BlockFile *blk, const char *name);

int BlockFile_size(BlockFile *blk);

BlockFile* BlockFile_getBlock(BlockFile *blk, int unsigned idx);

BlockFile** BlockFile_getBlocks(BlockFile *blk, int *size);

int BlockFile_addBlock(BlockFile *blk, BlockFile *chld);

void BlockFile_setLineWidth(BlockFile *blk, int wdt);

BlockFile* BlockFile_newLeafBlock(BlockFile *blk, char *txt);

BlockFile* BlockFile_newBlock(BlockFile *blk, int sz);

BlockFile* BlockFile_parse(const char *txt);

BlockFile* BlockFile_load(FILE *f);

int BlockFile_write(BlockFile *blk, char *b, int s);

int BlockFile_save(BlockFile *blk, FILE *f);

void BlockFile_print(BlockFile *blk);


#endif

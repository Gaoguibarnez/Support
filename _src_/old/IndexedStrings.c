
#include <stdio.h>

#include <string.h>


#include "Memory.h"


#include "Queue.c"

#include "RedBlackTree.c"

#include "Types.c"


#include "IndexedStrings.h"


typedef struct IndexedStrings {
	Queue *str_lst;

	RedBlackTree *str_idx;

	int max_lns;
	int ln_size;

	int num_str;
	int num_lns;

	void ***str_vl;

} IndexedStrings;


#define _INDEXEDSTRINGS_LINE_SIZE 1024
#define _INDEXEDSTRINGS_MAX_LINES 1024

IndexedStrings* IndexedStrings_create() {
	IndexedStrings *str = (IndexedStrings*) Memory_alloc(sizeof(IndexedStrings));

	str->str_lst = Queue_create();

	str->str_idx = RedBlackTree_create(RedBlackTree_stringComparator);

	str->max_lns = _INDEXEDSTRINGS_MAX_LINES;
	str->ln_size = _INDEXEDSTRINGS_LINE_SIZE;

	str->num_str = 0;
	str->num_lns = 1;

	str->str_vl = (void***) Memory_alloc(str->max_lns * sizeof(void**));

	if(str->str_vl == NULL) {
		Queue_destroy(str->str_lst);

		RedBlackTree_destroy(str->str_idx);

		return NULL;
	}

	str->str_vl[0] = (void**) Memory_alloc(str->ln_size * sizeof(void*));

	if(str->str_vl[0] == NULL) {
		Queue_destroy(str->str_lst);

		RedBlackTree_destroy(str->str_idx);

		Memory_free(str->str_vl);

		return NULL;
	}

	int i;
	for(i=1; i<str->max_lns; i++) {
		str->str_vl[i] = NULL;
	}

	return str;
}

void IndexedStrings_destroy(void *ptr) {
	IndexedStrings *str = (IndexedStrings*) ptr;

	char *nm;

	while(Queue_getNext(str->str_lst) != NULL) {
		nm = (char*) Queue_remove(str->str_lst);

		Memory_free(nm);
	}

	Queue_destroy(str->str_lst);

	RedBlackTree_destroy(str->str_idx);

	int i;
	for(i=0; i<str->max_lns; i++) {
		if(str->str_vl[i] != NULL) {
			Memory_free(str->str_vl[i]);
		}
	}

	Memory_free(str->str_vl);

	Memory_free(str);
}


int _IndexedStrings_allocateString(IndexedStrings *str, char *var) {
	int i, j, idx = ptrToInt(RedBlackTree_search(str->str_idx, var)) - 1;

	if(idx < 0) {
		idx = str->num_str;

		i = idx / str->ln_size;
		j = idx % str->ln_size;

		if(i == str->num_lns) {
			if(str->num_lns == str->max_lns) {
				printf("IndexedStrings [%p]: applying slow expansion...\n", str);

				int k, nml = 2 * str->max_lns;

				void ***tmp = (void***) Memory_alloc(nml * sizeof(void**));

				if(tmp == NULL) {
					printf("IndexedStrings [%p]: could allocate memory for slow expansion...\n", str);

					return -1;

				} else {
					for(k=0; k<str->num_lns; k++) {
						tmp[k] = str->str_vl[k];
					}

					for(k=str->num_lns; k<nml; k++) {
						tmp[k] = NULL;
					}

					Memory_free(str->str_vl);

					str->max_lns = nml;

					str->str_vl = tmp;
				}
			}

			str->str_vl[i] = (void**) Memory_alloc(str->ln_size * sizeof(void*));

			if(str->str_vl[i] != NULL) {
				str->num_lns++;

			} else {
				printf("IndexedStrings [%p]: could not allocate memory for fast expansion...\n", str);

				return -1;
			}
		}

		str->num_str++;

		if(strchr(var, '<') != NULL) {
			printf("IndexedStrings [%p]: invalid symbol '<' in string '%s'...\n", str, var);

			return -1;

		} else if(strchr(var, '>') != NULL) {
			printf("IndexedStrings [%p]: invalid symbol '>' in string '%s'...\n", str, var);

			return -1;
		}

		int len = strlen(var);

		char *tmp = (char*) Memory_alloc((len + 1) * sizeof(char));

		if(tmp == NULL) {
			printf("IndexedStrings [%p]: could not allocate memory for string...\n", str);

			return -1;
		}
		
		strcpy(tmp, var);

		Queue_insert(str->str_lst, tmp);

		RedBlackTree_insert(str->str_idx, tmp, intToPtr(idx + 1));

		str->str_vl[i][j] = tmp;
	}

	return idx;
}

void IndexedStrings_clear(IndexedStrings *str) {
	void *vl;

	while(Queue_getNext(str->str_lst) != NULL) {
		vl = Queue_remove(str->str_lst);

		if(RedBlackTree_remove(str->str_idx, vl) == NULL) {
			printf("IndexedStrings [%p]: could remove string '%s'...\n", str, (char*) vl);
		}

		Memory_free(vl);
	}
}

int IndexedStrings_getIndex(IndexedStrings *str, char *val) {
	return _IndexedStrings_allocateString(str, val);
}

char* IndexedStrings_getString(IndexedStrings *str, int idx) {
	if(idx >= str->num_str || idx < 0) {
		return NULL;
	}

	return str->str_vl[idx / str->ln_size][idx % str->ln_size];
}


char* _IndexedStrings_getBlock(char *buf, char **end) {
	while(*buf != '<') {
		if(*buf == '>' || *buf == '\0') {
			return NULL;
		}

		buf++;
	}

	buf++;

	char *tmp = buf;

	int cnt = 0;

	while(1) {
		if(*tmp == '\0') {
			return NULL;
		}

		if(*tmp == '>') {
			cnt--;

			if(cnt < 0) {
				break;
			}

		} else if(*tmp == '<') {
			cnt++;
		}

		tmp++;
	}

	*tmp = '\0';

	if(end != NULL) {
		*end = tmp + 1;
	}

	return buf;
}

int _IndexedStrings_parse(IndexedStrings *str, char *buf) {
	char *end;

	char *str_blk = _IndexedStrings_getBlock(buf, &end);

	if(str_blk == NULL) {
		return - 1;
	}

	if(strcmp(str_blk, "str") != 0) {
		return 0;
	}

	str_blk = _IndexedStrings_getBlock(end, &end);

	if(str_blk == NULL) {
		return - 1;
	}

	int idx0, idx1;

	char *idx_blk, *vl_blk;

	while(strchr(str_blk, '<') != NULL) {
		idx_blk = _IndexedStrings_getBlock(str_blk, &str_blk);

		if(idx_blk == NULL) {
			return - 1;
		}

		vl_blk = _IndexedStrings_getBlock(str_blk, &str_blk);

		if(vl_blk == NULL) {
			return 0;
		}

		idx0 = atoi(idx_blk);

		idx1 = IndexedStrings_getIndex(str, vl_blk);

		if(idx0 != idx1) {
			printf("IndexedStrings [%p]: index mismatch [%d:%d] for string '%s'...\n", str, idx0, idx1, vl_blk);

			return - 1;
		}
	}

	return (int) (end - buf);
}


int IndexedStrings_write(IndexedStrings *str, char *buf, int buf_sz) {
	int _buf_sz = buf_sz;

	int len = snprintf(buf, buf_sz, "\n<str><\n");

	if(len >= buf_sz) {
		return - 1;
	}

	buf += len;

	buf_sz -= len;

	int i;

	for(i=0; i<str->num_str; i++) {
		char *val = IndexedStrings_getString(str, i);

		len = snprintf(buf, buf_sz, "\t<%d><%s>\n", i, val);

		if(len >= buf_sz) {
			return - 1;
		}

		buf += len;

		buf_sz -= len;
	}

	len = snprintf(buf, buf_sz, ">\n");

	if(len >= buf_sz) {
		return - 1;
	}

	buf += len;

	buf_sz -= len;

	return _buf_sz - buf_sz;
}

int IndexedStrings_read(IndexedStrings *str, char *buf) {
	return _IndexedStrings_parse(str, buf);
}


int IndexedStrings_save(IndexedStrings *str, char *fl_nm) {
	FILE *f = fopen(fl_nm, "w");

	if(f == NULL) {
		return 0;
	}

	fprintf(f, "\n<str><\n");

	int i;

	for(i=0; i<str->num_str; i++) {
		char *val = IndexedStrings_getString(str, i);

		fprintf(f, "\t<%d><%s>\n", i, val);
	}

	fprintf(f, ">\n");

	fclose(f);

	return 1;
}

#define _INDEXEDSTRINGS_BUFFER_SIZE 1048576

char _idxstr_buf[_INDEXEDSTRINGS_BUFFER_SIZE + 1];

int IndexedStrings_load(IndexedStrings *str, char *fl_nm) {
	FILE *f = fopen(fl_nm, "r");

	if(f == NULL) {
		printf("IndexedStrings [%p]: could open file '%s'...\n", str, fl_nm);

		return 0;
	}

	int n = fread(_idxstr_buf, sizeof(char), _INDEXEDSTRINGS_BUFFER_SIZE, f);

	if(feof(f) == 0) {
		fclose(f);

		printf("IndexedStrings [%p]: file '%s' is too big...\n", str, fl_nm);

		return 0;
	}

	_idxstr_buf[n] = '\0';

	fclose(f);

	if(_IndexedStrings_parse(str, _idxstr_buf) < 0) {
		printf("IndexedStrings [%p]: could not parse file '%s'...\n", str, fl_nm);

		return 0;
	}

	return 1;
}

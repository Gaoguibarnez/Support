
#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H

typedef struct MemoryManager MemoryManager;

MemoryManager* MemoryManager_create();

void MemoryManager_destroy(void *v);

void MemoryManager_insertPointer(MemoryManager *mgr, void *ptr, void (*destroy_fnc)(void *));

void MemoryManager_removePointer(MemoryManager *mgr, void *ptr);

#endif


#include <stdio.h>


#include "Memory.h"

#include "Mutex.h"


#include "Queue.c"

#include "RedBlackTree.c"

#include "MemoryManager.h"

typedef struct MemoryManager {
	Mutex mtx;

	Queue *ptrs;

	RedBlackTree *destroy_fncs;
	
} MemoryManager;

MemoryManager* MemoryManager_create() {
	MemoryManager *mgr = (MemoryManager*) Memory_alloc(sizeof(MemoryManager));

	Mutex_init(&mgr->mtx);

	mgr->ptrs = Queue_create();

	mgr->destroy_fncs = RedBlackTree_create(RedBlackTree_defaultComparator);

	return mgr;
}

void MemoryManager_destroy(void *v) {
	MemoryManager *mgr = (MemoryManager*) v;

	Mutex_term(&mgr->mtx);

	void *ptr;

	void (*destroy_fnc)(void *ptr);

	while(Queue_getNext(mgr->ptrs) != NULL) {
		ptr = Queue_remove(mgr->ptrs);

		destroy_fnc = (void (*)(void *ptr)) RedBlackTree_search(mgr->destroy_fncs, ptr);

		if(destroy_fnc != NULL) {
			destroy_fnc(ptr);
		}
	}

	Memory_free(mgr);
}

void MemoryManager_insertPointer(MemoryManager *mgr, void *ptr, void (*destroy_fnc)(void *ptr)) {
	if(destroy_fnc != NULL) {
		Mutex_lock(&mgr->mtx);

		Queue_insert(mgr->ptrs, ptr);

		RedBlackTree_insert(mgr->destroy_fncs, ptr, destroy_fnc);

		Mutex_unlock(&mgr->mtx);
	}
}

void MemoryManager_removePointer(MemoryManager *mgr, void *ptr) {
	Mutex_lock(&mgr->mtx);

	RedBlackTree_remove(mgr->destroy_fncs, ptr);
	
	Mutex_unlock(&mgr->mtx);
}

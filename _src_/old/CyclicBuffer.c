
#include <string.h>

#include "Memory.h"

#include "CyclicBuffer.h"

typedef struct CyclicBuffer {
	int idx;
	int cnt;

	int sz;

	void *buf;

	char _mem_[];

} CyclicBuffer;

CyclicBuffer* CyclicBuffer_create(int sz) {
	CyclicBuffer *buf = (CyclicBuffer*) Memory_alloc(sz + sizeof(CyclicBuffer));

	buf->idx = 0;
	buf->cnt = 0;

	buf->sz = sz;

	buf->buf = (void*) buf->_mem_;

	return buf;
}

void CyclicBuffer_destroy(void *ptr) {
	Memory_free(ptr);
}

int CyclicBuffer_getSize(CyclicBuffer *buf) {
	return buf->sz;
}

int CyclicBuffer_getOccupied(CyclicBuffer *buf) {
	return buf->cnt;
}

int CyclicBuffer_getFree(CyclicBuffer *buf) {
	return buf->sz - buf->cnt;
}

int CyclicBuffer_push(CyclicBuffer *buf, void *val, int sz) {
	if(sz < 0) {
		return 0;
	}

	if(sz > buf->sz - buf->cnt) {
		// sz = buf->sz - buf->cnt;

		return 0;
	}

	int fst = buf->idx + buf->cnt;

	if(fst >= buf->sz) {
		memcpy(buf->buf - buf->sz + fst, val, sz);

		buf->cnt += sz;

		return sz;
	}

	int cpy = buf->sz - fst;

	if(sz > cpy) {
		memcpy(buf->buf + fst, val, cpy);

		memcpy(buf->buf, val + cpy, sz - cpy);

		buf->cnt += sz;

		return sz;
	}

	memcpy(buf->buf + fst, val, sz);

	buf->cnt += sz;

	return sz;
}

int CyclicBuffer_pop(CyclicBuffer *buf, void *val, int sz) {
	if(sz < 0) {
		return 0;
	}

	if(sz > buf->cnt) {
		// sz = buf->cnt;

		return 0;
	}

	int cnt = buf->sz - buf->idx;

	if(cnt >= sz) {
		memcpy(val, buf->buf + buf->idx, sz);

		buf->cnt -= sz;

		buf->idx += sz;

		return sz;
	}

	memcpy(val, buf->buf + buf->idx, cnt);

	buf->cnt -= sz;

	buf->idx = sz - cnt;

	memcpy(val + cnt, buf->buf, buf->idx);

	return sz;
}

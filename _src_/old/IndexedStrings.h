
#ifndef INDEXEDSTRINGS_H
#define INDEXEDSTRINGS_H

typedef struct IndexedStrings IndexedStrings;


IndexedStrings* IndexedStrings_create();

void IndexedStrings_destroy(void *ptr);


void IndexedStrings_clear(IndexedStrings *str);

int IndexedStrings_getIndex(IndexedStrings *str, char *val);

char* IndexedStrings_getString(IndexedStrings *str, int idx);


int IndexedStrings_write(IndexedStrings *str, char *buf, int buf_sz);

int IndexedStrings_read(IndexedStrings *str, char *buf);


int IndexedStrings_save(IndexedStrings *str, char *fl_nm);

int IndexedStrings_load(IndexedStrings *str, char *fl_nm);

#endif

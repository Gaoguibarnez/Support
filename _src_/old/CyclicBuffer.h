
#ifndef CYCLICBUFFER_H
#define CYCLICBUFFER_H

typedef struct CyclicBuffer CyclicBuffer;

CyclicBuffer* CyclicBuffer_create(int sz);

void CyclicBuffer_destroy(void *ptr);

int CyclicBuffer_getSize(CyclicBuffer *buf);

int CyclicBuffer_getOccupied(CyclicBuffer *buf);

int CyclicBuffer_getFree(CyclicBuffer *buf);

int CyclicBuffer_push(CyclicBuffer *buf, void *val, int sz);

int CyclicBuffer_pop(CyclicBuffer *buf, void *val, int sz);

#endif


#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include <dirent.h>

#include <sys/stat.h>

#include <unistd.h>

#include <errno.h>


int exists(const char *path) {
	struct stat info;

	return stat(path, &info) == 0;
}

int isDir(const char *path) {
	struct stat info;

	if(stat(path, &info)) {
		return 0;
	}

	return S_ISDIR(info.st_mode);
}

int isFile(const char *path) {
	struct stat info;

	if(stat(path, &info)) {
		return 0;
	}

	return !S_ISDIR(info.st_mode);
}

int createdir(const char *path) {
	#if defined _WIN32
	int r = mkdir(path);
	#else
	int r = mkdir(path, 0777);
	#endif

	return r == 0 || errno == EEXIST;
}


#include "copy.c"

#include "rmv.c"


int touch(char *path) {
	FILE *f = fopen(path, "wb");

	if(f != NULL) {
		fclose(f);
	}

	return f != NULL;
}


int main(int argc, char *argv[]) {
	if(argc < 2) {
		printf("    not enough arguments!\n");

		return 0;
	}

	if(strcasecmp(argv[1], "-c") == 0) {
		if(argc < 4) {
			printf("    not enough arguments!\n");

			return 0;
		}

		char *src = argv[2];
		char *dst = argv[3];

		printf("    copying '%s' > '%s'...", src, dst);

		if(!copy(src, dst)) {
			printf(" failed.\n");

			return 1;
		}

		printf(" done!\n");

	} else if(strcasecmp(argv[1], "-m") == 0) {
		if(argc < 4) {
			printf("    not enough arguments!\n");

			return 0;
		}

		char *src = argv[2];
		char *dst = argv[3];

		printf("    moving '%s' > '%s'...", src, dst);

		if(copy(src, dst)) {
			rmv(src);

		} else {
			printf(" failed.\n");

			rmv(dst);

			return 1;
		}

		printf(" done!\n");

	} else if(strcasecmp(argv[1], "-r") == 0) {
		if(argc < 3) {
			printf("    not enough arguments!\n");

			return 0;
		}

		int i;

		for(i=2; i<argc; i++) {
			char *src = argv[i];

			printf("    removing '%s'...", src);

			if(!rmv(src)) {
				printf(" failed\n");

				return 1;
			}
		}
		
		printf(" done!\n");

	} else if(strcasecmp(argv[1], "-pl") == 0) {
		int i;

		for(i=2; i<argc; i++) {
			printf("%s\n", argv[i]);
		}

	} else if(strcasecmp(argv[1], "--touch") == 0) {
		int i;

		for(i=2; i<argc; i++) {
			touch(argv[i]);
		}

	} else if(strcasecmp(argv[1], "--mkdir") == 0) {
		int i;

		for(i=2; i<argc; i++) {
			if(!createdir(argv[i])) {
				printf("    failed to create dir '%s'.", argv[i]);

				return 1;
			}
		}
	}

	return 0;
}

int copyfl(char *src, char *dst) {
	FILE *s = fopen(src, "rb");

	if(s == NULL) {
		return 0;
	}

	FILE *d = fopen(dst, "wb");

	if(d == NULL) {
		fclose(s);

		return 0;
	}

	int err = 0;

	int sz = 1024;

	char buf[sz];

	while(1) {
		int n = fread(buf, 1, sz, s);

		int m = fwrite(buf, 1, n, d);

		if(m != n) {
			err = 1;

			break;
		}

		if(n != sz) {
			if(!feof(s)) {
				err = 1;
			}

			break;
		}
	}

	fclose(s);
	fclose(d);

	return err == 0;
}

int copy(char *src, char *dst) {
	if(isFile(src)) return copyfl(src, dst);

	DIR *d = opendir(src);

	if(d == NULL) return 0;

	int err = 0;

	while(1) {
		struct dirent *dir = readdir(d);

		if(dir == NULL) break;

		if(strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) continue;

		int sl = strlen(src);
		int dl = strlen(dst);
		int nl = strlen(dir->d_name);

		char src_tmp[sl + nl + 2];
		char dst_tmp[dl + nl + 2];

		strcpy(src_tmp, src);
		strcpy(src_tmp + sl, "/");
		strcpy(src_tmp + sl + 1, dir->d_name);

		strcpy(dst_tmp, dst);
		strcpy(dst_tmp + dl, "/");
		strcpy(dst_tmp + dl + 1, dir->d_name);

		if(isDir(src_tmp)) {
			if(!createdir(dst_tmp)) {
				err = 1;
				break;
			}

			if(!copy(src_tmp, dst_tmp)) {
				err = 1;
				break;	
			}

		} else {
			if(!copyfl(src_tmp, dst_tmp))  {
				err = 1;
				break;	
			}
		}
	}

	closedir(d);

	return err == 0;
}

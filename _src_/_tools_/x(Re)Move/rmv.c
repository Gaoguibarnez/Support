
int rmv(char *path) {
	if(!exists(path)) return 1;

	if(isFile(path)) return remove(path) == 0;

	DIR *d = opendir(path);

	if(d == NULL) return 0;

	int err = 0;

	while(1) {
		struct dirent *dir = readdir(d);

		if(dir == NULL) break;

		if(strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) continue;

		int sl = strlen(path);
		int dl = strlen(dir->d_name);

		char tmp[sl + dl + 2];

		strcpy(tmp, path);
		strcpy(tmp + sl, "/");
		strcpy(tmp + sl + 1, dir->d_name);

		if(!rmv(tmp)) {
			err = 1;
			break;
		}
	}

	closedir(d);

	if(err) return 0;

	return rmdir(path) == 0;
}

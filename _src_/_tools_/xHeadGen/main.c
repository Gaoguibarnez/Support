
#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include <dirent.h>

#include <sys/stat.h>



int exists(const char *path) {
	struct stat info;

	return stat(path, &info) == 0;
}

int isDir(const char *path) {
	struct stat info;

	if(stat(path, &info)) {
		return 0;
	}

	return S_ISDIR(info.st_mode);
}

int isFile(const char *path) {
	struct stat info;

	if(stat(path, &info)) {
		return 0;
	}

	return !S_ISDIR(info.st_mode);
}

int isSpace(char c) {
	return c == '\n' || c == '\r' || c == '\t' || c == ' ';
}


FILE *h_file = NULL;


int file_count = 0;

int fail_count = 0;


#define MAX_ARGS 64

int isIDChar(char c) {
	if(c == '_') {
		return 1;
	}

	if(c >= '0' && c <= '9') {
		return 1;
	}

	if(c >= 'a' && c <= 'z') {
		return 1;
	}

	if(c >= 'A' && c <= 'Z') {
		return 1;
	}

	return 0;
}

int isID(char *s) {
	if(*s >= '0' && *s <= '9') {
		return 0;
	}

	while(*s != '\0') {
		if(!isIDChar(*s++)) {
			return 0;
		}
	}

	return 1;
}

void eatSpaces(char **file) {
	while(isSpace(**file)) {
		++(*file);
	}
}

char *types[] = {
	"void", 
	"char", 
	"int", 
	"float", 
	"double", 
	"pointer"
};

char* nextDeclaration(char *file, char **ret_name, char **fnc_name, char **fnc_args) {
	while(1) {
		while(!isIDChar(*file)) {
			if(*file == '\0') {
				return NULL;
			}

			++file;
		}

		char *type = file;

		while(isIDChar(*file)) {
			++file;
		}

		char tmp_char = *file;

		*file = '\0';

		if(!isID(type) || strcmp(type, "else") == 0) {
			++file;

			continue;
		}

		*file = tmp_char;

		while(*file == '*' || *file == '[' || *file == ']') {
			++file;
		}

		if(*file == '\0') {
			return NULL;
		}

		if(!isSpace(*file)) {
			continue;
		}

		*file++ = '\0';

		while(isSpace(*file)) {
			++file;
		}

		// printf("found possible return: %s\n", type);

		char *name = file;

		while(isIDChar(*file)) {
			++file;
		}

		if(*file == '(') {
			*file++ = '\0';

		} else {
			char *name_end = NULL;

			if(isSpace(*file)) {
				name_end = file++;
			}

			eatSpaces(&file);

			if(*file != '(') {
				file = name;

				continue;
			}

			if(name_end == NULL) {
				*file++ = '\0';

			} else {
				*name_end = '\0';

				++file;
			}
		}

		if(*name == '_') {
			continue;
		}

		// printf("\tfound possible function name: %s\n", name);

		int par_cnt = 0;

		int quotes = 0;

		int was_bar = 0;

		char *args = file;

		while(1) {
			if(*file == '\\') {
				was_bar = 1;

				++file;

				continue;
			}

			if(*file == '\0') {
				return NULL;
			}

			if(*file == '\"' && was_bar == 0) {
				quotes = !quotes;

				++file;

				continue;
			}

			was_bar = 0;

			if(quotes) {
				++file;

			} else if(*file == '(') {
				++file;

				++par_cnt;

			} else if(*file == ')') {
				if(--par_cnt < 0) {
					*file++ = '\0';

					break;
				}

				++file;

			} else {
				++file;
			}
		}

		// printf("\t\tfound possible args: '%s'\n", args);

		while(isSpace(*file)) {
			++file;
		}

		if(*file != '{') {
			continue;
		}

		*ret_name = type;

		*fnc_name = name;

		*fnc_args = args;

		break;
	}

	return file;
}

int shouldIgnore(char *fnc_name) {
	if(strcmp(fnc_name, "File_getCurrentPath") == 0) {
		return 1;
	}

	if(strcmp(fnc_name, "IndexedStrings_create") == 0) {
		return 1;
	}

	if(strcmp(fnc_name, "IndexedStrings_destroy") == 0) {
		return 1;
	}

	if(strcmp(fnc_name, "IndexedStrings_clear") == 0) {
		return 1;
	}

	if(strcmp(fnc_name, "yylex") == 0) {
		return 1;
	}

	if(strcmp(fnc_name, "yyparse") == 0) {
		return 1;
	}

	if(strcmp(fnc_name, "Color_randomize") == 0) {
		return 1;
	}

	if(strcmp(fnc_name, "yyerror") == 0) {
		return 1;
	}

	if(strcmp(fnc_name, "") == 0) {
		return 1;
	}

	if(strcmp(fnc_name, "") == 0) {
		return 1;
	}

	return 0;
}

int process(char *file) {

	while(file != NULL) {
		char *ret_name = NULL;
		char *fnc_name = NULL;
		char *fnc_args = NULL;

		file = nextDeclaration(file, &ret_name, &fnc_name, &fnc_args);

		if(file == NULL) {
			break;
		}

		if(ret_name == NULL) {
			return 0;
		}

		if(fnc_name == NULL) {
			return 0;
		}

		if(fnc_args == NULL) {
			return 0;
		}

		if(shouldIgnore(fnc_name)) {
			continue;
		}

		// printf("%s %s(%s);\n\n", ret_name, fnc_name, fnc_args);

		fprintf(h_file, "%s %s(%s);\n\n", ret_name, fnc_name, fnc_args);
	}

	return 1;
}

int shouldProcessDir(char *path) {
	return 1;
}

int shouldProcess(char *file_name, char *path) {
	if(strcmp(file_name, "IndexedStrings.c") == 0) {
		return 0;
	}

	if(strcmp(file_name, "IndexedStrings.h") == 0) {
		return 0;
	}

	if(strcmp(file_name, "CyclicBuffer.c") == 0) {
		return 0;
	}

	if(strcmp(file_name, "CyclicBuffer.h") == 0) {
		return 0;
	}

	if(strcmp(file_name, "AnimationInstance.h") == 0) {
		return 0;
	}

	if(strstr(path, "zzzOLDzzz") != NULL) {
		return 0;
	}

	if(strstr(path, "/old/") != NULL) {
		return 0;
	}

	if(isDir(path)) {
		return shouldProcessDir(path);
	}

	return 1;
}

int search(char *name) {
	int l = strlen(name);

	if(name[l - 1] == '/') {
		--l;
	}

	char tmp[l + 9];

	strcpy(tmp, name);
	strcpy(tmp + l, "/.ignore");

	FILE *ignore = fopen(tmp, "r");

	if(ignore != NULL) {
		fclose(ignore);

		return 1;
	}

	if(isFile(name)) {
		return 0; // process file
	}

	// printf("processing directory '%s'.\n", name);

	DIR *d = opendir(name);

	if(d == NULL) {
		// printf("could not open directory '%s'.\n", name);

		return 0;
	}

	struct dirent *dir;

	while(1) {
		dir = readdir(d);

		if(dir == NULL) {
			break;
		}

		if(strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) {
			continue;
		}

		char tmp[l + 2 + strlen(dir->d_name)];

		strcpy(tmp, name);
		strcpy(tmp + l, "/");
		strcpy(tmp + l + 1, dir->d_name);

		char *file_name = dir->d_name;
		
		if(!isDir(tmp) && shouldProcess(file_name, tmp)) {
			fprintf(h_file, "\n/* %s */\n\n", file_name);


			// printf("processing file '%s'.\n", tmp);

			FILE *f = fopen(tmp, "rb");

			if(f == NULL) {
				// printf("\tfile '%s' does not exists.\n", tmp);

			} else {
				#define FILE_MAX_SIZE 1048576

				char *buf = (char*) malloc(FILE_MAX_SIZE);

				int n = fread(buf, 1, FILE_MAX_SIZE, f);

				buf[n] = '\0';
	
				if(process(buf) == 0) {
					++fail_count;

					// printf("\tfailed to process file '%s'.\n", tmp);

				} else {
					++file_count;
				}

				free(buf);

				fclose(f);
			}
		}
	}

	closedir(d);

	return 1;
}


int main(int argc, char *argv[]) {
	if(argc < 3) {
		printf("not enough arguments...\n");

		return 1;
	}

	char *s_name = argv[1];
	char *h_name = argv[2];

	h_file = fopen(h_name, "wb");

	if(h_file == NULL) {
		printf("could not open file '%s'...\n", h_name);

		return 1;
	}

	if(!search(s_name)) {
		return 1;
	}

	printf("\n%d file(s) processed.\n", file_count);
	printf("%d file(s) failed.\n\n", fail_count);

	return 0;
}


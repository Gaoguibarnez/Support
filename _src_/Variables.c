
#include <stdio.h>

#include <stdlib.h>

#include <string.h>


#include "../Memory.h"


#include "../BlockFile.h"

#include "../Keys.h"

#include "../Mutex.h"


#include "../Types.c"


#include "../Queue.c"

#include "../RedBlackTree.c"

#include "../Stack.c"


#include "../Variables.h"


static inline void _Variables_globalInit() {
	Memory_init();

	Keys_init();
}

static inline void _Variables_globalTerm() {
	Keys_term();

	Memory_term();
}



// typedef struct Variables {
// 	Mutex *mtx;

// 	Queue *key;

// 	RedBlackTree *var_val;
// 	RedBlackTree *var_type;

// 	RedBlackTree *str_val;

// } Variables;

Variables* Variables_create() {
	_Variables_globalInit();

	Variables *var = (Variables*) Memory_alloc(sizeof(Variables));

	var->mtx = NULL;

	var->key = Queue_create();

	var->var_val = RedBlackTree_create(NULL);
	var->var_type = RedBlackTree_create(NULL);

	var->str_val = RedBlackTree_create(NULL);

	return var;
}

Variables* Variables_createMT() {
	Variables *var = Variables_create();

	Mutex_init(var->mtx = Memory_alloc(sizeof(Mutex)));

	return var;
}

static void _Variables_destroyString(void *ptr) {
	if(ptr != NULL) Memory_free(ptr);
}

void Variables_destroy(void *ptr) {
	Variables *var = (Variables*) ptr;

	if(var->mtx != NULL) {
		Mutex_term(var->mtx);

		Memory_free(var->mtx);
	}

	Queue_destroy(var->key);

	RedBlackTree_clear(var->str_val, NULL, _Variables_destroyString);

	RedBlackTree_destroy(var->var_val);
	RedBlackTree_destroy(var->var_type);

	RedBlackTree_destroy(var->str_val);

	Memory_free(var);

	_Variables_globalTerm();
}



#define _VARIABLES_LOCK() if(var->mtx != NULL) Mutex_lock(var->mtx)
#define _VARIABLES_UNLOCK() if(var->mtx != NULL) Mutex_unlock(var->mtx)


static inline void* _Variables_getExistAddr(Variables *var, void *key) {
	return RedBlackTree_addr(var->var_val, key);
}

static inline void** _Variables_allocVar(Variables *var, void *key, Type_t type, void *def_val) {
	_VARIABLES_LOCK();

	void **ptr = RedBlackTree_addr(var->var_val, key);

	if(ptr == NULL) {
		if(RedBlackTree_addr(var->str_val, key) == NULL) {
			Queue_insert(var->key, key);
		}

		RedBlackTree_insert(var->var_val, key, def_val);

		ptr = RedBlackTree_addr(var->var_val, key);

		RedBlackTree_insert(var->var_type, key, TypeToPtr(type));
	}

	_VARIABLES_UNLOCK();

	return ptr;
}

void Variables_setVar(Variables *var, void *key, Type_t type, void *val) {
	*_Variables_allocVar(var, key, type, val) = val;
}

void* Variables_getVar(Variables *var, void *key, Type_t type, void *def_val) {
	return *_Variables_allocVar(var, key, type, def_val);
}

void** Variables_getAddr(Variables *var, void *key, Type_t type, void *def_val) {
	return _Variables_allocVar(var, key, type, def_val);
}


static inline char* _Variables_getExistStr(Variables *var, void *key) {
	void **ptr = RedBlackTree_addr(var->str_val, key);

	if(ptr == NULL) {
		return NULL;
	}

	return (char*) *ptr;
}

static inline char** _Variables_allocStr(Variables *var, void *key, char *def_val, int *new) {
	_VARIABLES_LOCK();

	void **ptr = RedBlackTree_addr(var->str_val, key);

	if(ptr == NULL) {
		if(RedBlackTree_addr(var->var_val, key) == NULL) {
			Queue_insert(var->key, key);
		}

		if(def_val != NULL) {
			def_val = strcpy(Memory_alloc(1 + strlen(def_val)), def_val);
		}

		RedBlackTree_insert(var->str_val, key, def_val);

		ptr = RedBlackTree_addr(var->str_val, key);

		if(new != NULL) *new = 1;
	}

	_VARIABLES_UNLOCK();

	return (char**) ptr;
}

static inline int _Variables_setStr(Variables *var, void *key, char *val) {
	int new = 0;

	char **ptr = (char**) _Variables_allocStr(var, key, val, &new);

	if(new) return 1;

	if(val == NULL) {
		if(*ptr != NULL)
			Memory_free(*ptr);
		
		*ptr = NULL;

		return 1;
	}

	if(*ptr != NULL) {
		if(strcmp(*ptr, val) == 0) return 1;
	}

	char *str = (char*) Memory_alloc(1 + strlen(val));

	if(str == NULL) return 0;

	if(*ptr != NULL)
		Memory_free(*ptr);

	*ptr = strcpy(str, val);

	return 1;
}

void* Variables_getStringAddressFromKey(Variables *var, void *key, char *def_val) {
	return (char**) _Variables_allocStr(var, key, def_val, NULL);
}

void Variables_setStringFromKey(Variables *var, void *key, char *val) {
	_Variables_setStr(var, key, val);
}

char* Variables_getStringFromKey(Variables *var, void *key, char *def_val) {
	return *_Variables_allocStr(var, key, def_val, NULL);
}

void* Variables_getStringAddress(Variables *var, char *nm, char *def_val) {
	return Variables_getStringAddressFromKey(var, Keys_get(nm), def_val);
}

void Variables_setString(Variables *var, char *nm, char *val) {
	Variables_setStringFromKey(var, Keys_get(nm), val);
}

char* Variables_getString(Variables *var, char *nm, char *def_val) {
	return Variables_getStringFromKey(var, Keys_get(nm), def_val);
}


void Variables_setStringFromAddress(void *addr, char *str) {
	if(*(char**) addr != NULL) Memory_free(*(char**) addr);

	*(char**) addr = Memory_allocs(str);
}


int Variables_hasVar(Variables *var, void *key) {
	return _Variables_getExistAddr(var, key) != NULL;
}

int Variables_hasStr(Variables *var, void *key) {
	return _Variables_getExistStr(var, key) != NULL;
}


void Variables_setTypeFromKey(Variables *var, void *key, Type_t type) {
	RedBlackTree_insert(var->var_type, key, TypeToPtr(type));
}

void Variables_setType(Variables *var, char *nm, Type_t type) {
	Variables_setTypeFromKey(var, Keys_get(nm), type);
}

Type_t Variables_getTypeFromKey(Variables *var, void *key) {
	return ptrToType(RedBlackTree_search(var->var_type, key));
}

Type_t Variables_getType(Variables *var, char *nm) {
	return Variables_getTypeFromKey(var, Keys_get(nm));
}


static inline char* __Variables_ptos(char *buf, int len, void *val) {
	char *tmp = buf + 2;

	snprintf(tmp, len - 2, "%p", val);

	while(1) {
		if(*tmp == '0' || *tmp == 'x' || *tmp == 'X') {
			++tmp;

		} else if(*tmp == '\0') {
			return "0x0";

		} else {
			break;
		}
	}

	*--tmp = 'x';
	*--tmp = '0';

	return tmp;
}

const char __dynvar_ignore[] = "._*~ \t\n\r";

static inline int __Variables_ignore(const char *nm) {
	return strchr(__dynvar_ignore, *nm) != NULL;
}

static void _Variables_readVal(Variables *var, char *name, const char *val) {
	if(*val == '\"') {
		char *end = strrchr(val + 1, '\"');

		if(end == NULL) return;

		int n = (int) (end - val) - 1;

		char tmp[n + 1];

		strncpy(tmp, val + 1, n);

		tmp[n] = '\0';

		Variables_setString(var, name, tmp);
	}
	else if(*val == '\'') {
		Variables_setChar(var, name, val[1]);
	}
	else if(strncmp(val, "0x", 2) == 0 || strncmp(val, "0X", 2) == 0) {
		void *pval;

		sscanf(val + 2, "%p", &pval);

		Variables_setPtr(var, name, pval);
	}
	else if(strchr(val, '.') != NULL || strchr(val, 'e') != NULL || strchr(val, 'E') != NULL) {
		if(strchr(val, 'd') != NULL || strchr(val, 'D') != NULL) {
			Variables_setDbl(var, name, atof(val));
		}
		else {
			Variables_setFlt(var, name, (float) atof(val));
		}
	}
	else {
		Variables_setInt(var, name, atoi(val));
	}
}

static int _Variables_readBlock(Variables *var, BlockFile *blk) {
	int i, n;

	BlockFile **blks = BlockFile_getBlocks(blk, &n);

	if(n != 1) return 0;

	if(BlockFile_isLeaf(blks[0])) return 0;

	char *name = BlockFile_getName(blks[0]);

	if(name == NULL) return 0;

	if(strcasecmp(name, "var") != 0) return 0;

	blks = BlockFile_getBlocks(blks[0], &n);

	for(i=0; i<n; i++) {
		if(!BlockFile_isLeaf(blks[i])) return 0;

		char *name = BlockFile_getName(blks[i]);

		if(name == NULL) return 0;

		const char *val = BlockFile_getText(blks[i]);

		_Variables_readVal(var, name, val);
	}

	return 1;
}

static int _Variables_writeBlock(Variables *var, BlockFile *blk) {
	BlockFile *chld, *val = BlockFile_create(16);

	BlockFile_setName(val, "var");

	BlockFile_setLineWidth(val, 1);

	BlockFile_addBlock(blk, val);

	blk = val;

	void *fst = Queue_getNext(var->key);

	if(fst != NULL) {
		do {
			void *key = Queue_remove(var->key);

			const char *name = Keys_toString(key);

			Queue_insert(var->key, key);

			if(!__Variables_ignore(name)) {
				void *addr = _Variables_getExistAddr(var, key);

				if(addr != NULL) {
					Type_t type = Variables_getTypeFromKey(var, key);

					if(type != 0) {
						if(type == TYPE_ID_CHR) {
							chld = BlockFile_createLeaff("'%c'", *(char*) addr);
						}
						else if(type == TYPE_ID_INT) {
							chld = BlockFile_createLeaff("%d", *(int*) addr);
						}
						else if(type == TYPE_ID_FLT) {
							chld = BlockFile_createLeaff("%f", *(float*) addr);
						}
						else if(type == TYPE_ID_DBL) {
							chld = BlockFile_createLeaff("%lfd", *(double*) addr);
						}
						else {
							char sval[64];
							chld = BlockFile_createLeaff("%s", 
								__Variables_ptos(sval, 64, *(void**) addr));
						}

						BlockFile_setName(chld, name);

						BlockFile_addBlock(blk, chld);
					}
				}

				char *sval = _Variables_getExistStr(var, key);

				if(sval != NULL) {
					chld = BlockFile_createLeaff("\"%s\"", sval);

					BlockFile_setName(chld, name);

					BlockFile_addBlock(blk, chld);
				}
			}
		}
		while(Queue_getNext(var->key) != fst);
	}

	return 1;
}


int Variables_save(Variables *var, FILE *file) {
	BlockFile *blk = BlockFile_create(2);

	BlockFile_setLineWidth(blk, 2);

	_VARIABLES_LOCK();

	int r = _Variables_writeBlock(var, blk);

	_VARIABLES_UNLOCK();

	if(r) {
		if(BlockFile_save(blk, file) < 0) r = 0;
	}

	BlockFile_destroy(blk);

	return r;
}

int Variables_load(Variables *var, FILE *file) {
	BlockFile *blk = BlockFile_load(file);

	if(blk == NULL) return 0;

	_VARIABLES_LOCK();

	int r = _Variables_readBlock(var, blk);

	_VARIABLES_UNLOCK();

	BlockFile_destroy(blk);

	return r;
}


#include <stdio.h>

#include <stdlib.h>

#include <stdint.h>

#include <string.h>


#ifndef MEMORY_SINGLETHREAD
#include "../Mutex.h"
#endif


#define REDBLACKTREE_USESTDMEMORY

#include "../RedBlackTree.c"

#include "../Types.c"

#include "../ptrar.c"


#include "../Memory.h"


#include "Memory/MemBuckets.c"


#define MEMORY_PRINTLEAKS 1



#ifndef MEMORY_SINGLETHREAD
static int _mem_mtx_init = 0;

static Mutex _mem_mtx;
#endif

static inline void _Memory_lock() {
	#ifndef MEMORY_SINGLETHREAD
	if(!_mem_mtx_init) {
		glbMutex_lock();

		if(!_mem_mtx_init) {
			Mutex_init(&_mem_mtx);

			_mem_mtx_init = 1;
		}

		glbMutex_unlock();
	}

	Mutex_lock(&_mem_mtx);
	#endif
}

static inline void _Memory_unlock() {
	#ifndef MEMORY_SINGLETHREAD
	Mutex_unlock(&_mem_mtx);
	#endif
}



#define MEMORY_Ki (1024)
#define MEMORY_Mi (1024 * MEMORY_Ki)
#define MEMORY_Gi (1024 * MEMORY_Mi)

static unsigned int _mem_str_buf_idx = 0;

static char _mem_str_buf[4][24];

const char* Memory_toString(size_t m) {
	#define _MEMORY_SCALECHANGE 2.0f

	char *b = _mem_str_buf[_mem_str_buf_idx++ % 4];

	float f = (float) m;

	if(f > _MEMORY_SCALECHANGE * MEMORY_Gi) {
		snprintf(b, 24, "%.2f MiB", f / MEMORY_Gi);

	} else if(f > _MEMORY_SCALECHANGE * MEMORY_Mi) {
		snprintf(b, 24, "%.2f MiB", f / MEMORY_Mi);

	} else if(f > _MEMORY_SCALECHANGE * MEMORY_Ki) {
		snprintf(b, 24, "%.2f KiB", f / MEMORY_Ki);

	} else {
		snprintf(b, 24, "%u Bytes", (unsigned int) m);
	}

	return b;
}



static size_t _mem_alloc = 0;

static size_t _mem_alloc_max = 0;

static size_t _mem_alloc_total = 0;

static size_t _mem_alloc_count = 0;

static size_t _mem_alloc_fail = 0;

static inline void* mallocWithRegistry(size_t size) {
	void *ptr = malloc(size);

	if(ptr != NULL) {
		_mem_alloc += size;

		_mem_alloc_total += size;

		if(_mem_alloc > _mem_alloc_max) {
			_mem_alloc_max = _mem_alloc;
		}

		++_mem_alloc_count;

	} else {
		++_mem_alloc_fail;
	}

	return ptr;
}

static inline void freeWithRegistry(void *ptr, size_t size) {
	free(ptr);
	
	_mem_alloc -= size;
}

static inline void* reallocWithRegistry(void *ptr, size_t size, size_t old_size) {
	ptr = realloc(ptr, size);

	if(ptr != NULL) {
		_mem_alloc += size;
		_mem_alloc -= old_size;
	}

	return ptr;
}



static int _mem_init = 0;

static RedBlackTree *_mem_allocated = NULL;

static void _Memory_init() {
	_mem_allocated = RedBlackTree_create(NULL);

	MemBuckets_init();
}

static inline void _Memory_term() {
	MemBuckets_term();

	RedBlackTree_destroy(_mem_allocated);
}

static void _Memory_locki() {
	_Memory_lock();

	if(_mem_init) return;

	_Memory_init();

	_mem_init = 1;
}


static inline void* _Memory_alloc(size_t size) {
	_Memory_locki();

	void *ptr_usr = MemBuckets_alloc(size);

	if(ptr_usr == NULL) {
		size_t ext_sz = sizeof(size_t);

		size += ext_sz;

		void *ptr = mallocWithRegistry(size);

		*(size_t*) ptr = size;

		ptr_usr = ptrsum(ptr, ext_sz);

		RedBlackTree_insert(_mem_allocated, ptr_usr, ptr);		
	}

	_Memory_unlock();

	return ptr_usr;
}

static inline int _Memory_free(void *ptr_usr) {
	int ret = 1;

	_Memory_locki();

	if(!MemBuckets_freeWithReturn(ptr_usr)) {
		void *ptr = RedBlackTree_remove(_mem_allocated, ptr_usr);

		ret = ptr != NULL;

		if(ret) freeWithRegistry(ptr, *(size_t*) ptr);
	}

	_Memory_unlock();

	return ret;
}

static inline void* _Memory_realloc(void *old_ptr_usr, size_t size) {
	_Memory_locki();

	void *ptr_usr = MemBuckets_realloc(old_ptr_usr, size);

	if(ptr_usr == NULL) {
		void *old_ptr = RedBlackTree_remove(_mem_allocated, old_ptr_usr);

		if(old_ptr != NULL) {
			size += sizeof(size_t);

			size_t old_sz = *(size_t*) old_ptr;

			void *new_ptr = reallocWithRegistry(old_ptr, size, old_sz);

			*(size_t*) new_ptr = size;

			ptr_usr = ptrsum(new_ptr, sizeof(size_t));

			RedBlackTree_insert(_mem_allocated, ptr_usr, new_ptr);
		}
	}

	_Memory_unlock();

	return ptr_usr;
}


static size_t _mem_alloc_unmanaged_count = 0;

static size_t _mem_alloc_unmanaged_total = 0;

void* Memory_ualloc(size_t size) {
	_Memory_lock();

	void *ptr = malloc(size);

	if(ptr == NULL) {
		++_mem_alloc_fail;

	} else {
		++_mem_alloc_unmanaged_count;

		_mem_alloc_unmanaged_total += size;
	}

	_Memory_unlock();

	return ptr;
}

void Memory_ufree(void *ptr, size_t size) {
	_Memory_lock();

	--_mem_alloc_unmanaged_count;

	_mem_alloc_unmanaged_total -= size;

	free(ptr);

	_Memory_unlock();
}

void* Memory_urealloc(void *ptr, size_t size, size_t old_size) {
	_Memory_lock();

	void *new_ptr = realloc(ptr, size);

	if(new_ptr != NULL) {
		_mem_alloc_unmanaged_total += size;
		_mem_alloc_unmanaged_total -= old_size;		
	}

	_Memory_unlock();

	return new_ptr;
}


void* Memory_alloc(size_t size) {
	return _Memory_alloc(size);
}

int Memory_freeWithReturn(void *ptr) {
	return _Memory_free(ptr);
}


void* Memory_realloc(void *ptr, size_t size) {
	return _Memory_realloc(ptr, size);
}

void* Memory_alloc0(size_t size) {
	void *ptr = Memory_alloc(size);

	if(ptr != NULL) {
		memset(ptr, 0, size);
	}

	return ptr;
}

char* Memory_allocs(const char *str) {
	void *ptr = Memory_alloc(1 + strlen(str));

	if(ptr) strcpy(ptr, str);

	return ptr;
}


size_t Memory_getUsedMemory() {
	return _mem_alloc;
}

size_t Memory_getMaxUsedMemory() {
	return _mem_alloc_max;
}

size_t Memory_getTotalUsedMemory() {
	return _mem_alloc_total;
}


static int _mem_itr_num;

static int _mem_itr_cnt;

static void **_mem_itr_buf;

static int _Memory_clearIterator(void *ptr, void *key, void *val) {
	_mem_itr_buf[_mem_itr_cnt] = key;

	return ++_mem_itr_cnt < _mem_itr_num;
}

void Memory_clear() {
	if(_mem_init == 0) return;

	_mem_itr_num = 1024;

	void *buf[_mem_itr_num];

	do {
		_Memory_lock();

		_mem_itr_cnt = 0;

		_mem_itr_buf = buf;

		RedBlackTree_iterate(_mem_allocated, _Memory_clearIterator, NULL);

		_Memory_unlock();

		int i;

		for(i=0; i<_mem_itr_cnt; i++) {
			Memory_free(buf[i]);
		}

	} while(_mem_itr_cnt != 0);
}


void Memory_init() {
	_Memory_lock();

	if(_mem_init++ == 0) _Memory_init();

	_Memory_unlock();
}

static int _Memory_printIterator(void *ptr, void *key, void *val) {
	printf(">>> %p (%d)\n", key, (int) *(size_t*) val);

	return 1;
}

void Memory_term() {
	_Memory_lock();

	if(--_mem_init == 0) {
		if(MEMORY_PRINTLEAKS) {
			RedBlackTree_iterate(_mem_allocated, _Memory_printIterator, NULL);

			MemBuckets_printLeaks();
		}

		_Memory_term();

		_Memory_unlock();

		#ifndef MEMORY_SINGLETHREAD
		Mutex_term(&_mem_mtx);

		_mem_mtx_init = 0;
		#endif

	} else {
		_Memory_unlock();
	}
}


void Memory_printFootprint(const char *id) {
	if(id != NULL) {
		printf("Memory (%s):\n", id);

	} else {
		if(Memory_getUsedMemory() == 0
		 && _mem_alloc_unmanaged_count == 0
		  && !MemBuckets_hasFootprint()) return;

		printf("Memory:\n");
	}
	
	printf("\tused: %s\n", Memory_toString(Memory_getUsedMemory()));
	printf("\tmax used: %s\n", Memory_toString(Memory_getMaxUsedMemory()));
	printf("\ttotal used: %s\n", Memory_toString(Memory_getTotalUsedMemory()));

	if(_mem_alloc_unmanaged_count > 0) {
		printf("\tunmanaged:\n");
		printf("\t\tcount: %d\n", (int) _mem_alloc_unmanaged_count);
		printf("\t\tunsure: %s\n", Memory_toString(_mem_alloc_unmanaged_total));
	}

	MemBuckets_printFootprint(id != NULL);
}

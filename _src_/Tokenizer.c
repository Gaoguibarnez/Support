
#include <stdio.h>

#include <string.h>

#include <limits.h>


#include "../Memory.h"

#include "../Queue.c"


#include "../Tokenizer.h"



static inline int __Tokenizer_isSpace(char c) {
	return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

static inline int __Tokenizer_isDigit(char c) {
	return c >= '0' && c <= '9';
}

static inline int __Tokenizer_isLetter(char c) {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

static inline int __Tokenizer_isHexDigit(char c) {
	return (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') || __Tokenizer_isDigit(c);
}

static inline int __Tokenizer_isNameChar(char c) {
	if(c == '_') return 1;

	if(__Tokenizer_isDigit(c)) return 1;

	return __Tokenizer_isLetter(c);
}



// typedef struct Token {
// 	...
// } Token;

Token* Token_create(const char *src, int ln, int id, const char *val, int l) {
	Token *tkn = Memory_alloc(1 + l + sizeof(Token));

	if(tkn == NULL) return NULL;

	tkn->src = src;
	tkn->ln = ln;

	tkn->id = id;
	memcpy(tkn->val, val, l);
	tkn->val[l] = '\0';

	// printf(">>> '%s'\n", tkn->val);

	return tkn;
}

void Token_destroy(void *ptr) {
	Memory_free(ptr);
}


#define TOKENIZER_ERROR_UNEXPECTEDSYMBOLININT 1
#define TOKENIZER_ERROR_UNEXPECTEDSYMBOLINHEX 2
#define TOKENIZER_ERROR_UNEXPECTEDSYMBOLINFLOAT 3

typedef struct Tokenizer {
	const char *source;
	int line;

	const char *curr;

	int err;

	int dot_in_name;

} Tokenizer;

void Tokenizer_init(Tokenizer *tkn, const char *src) {
	tkn->source = src;
	tkn->line = 1;

	tkn->curr = src;

	tkn->err = 0;

	tkn->dot_in_name = 1;
}

void Tokenizer_term(Tokenizer *tkn) {
}


static Token* Tokenizer_createToken(Tokenizer *tkn, int id, const char *val, int l) {
	return Token_create(tkn->source, tkn->line, id, val, l);
}


static inline int __Tokenizer_readComment(Tokenizer *tkn, const char *curr) {
	int c = 1;

	while(1) {
		if(*curr == '\0') {
			// CHECK OPEN COMMENT FLAG ?

			break;
		}

		if(*curr == '\n') {
			++tkn->line;

		} else if(*curr == '*') {
			if(*(curr + 1) == '/') {
				if(--c == 0) {
					curr += 2;

					break;
				}

			} else if(*(curr - 1) == '/') {
				// CHECK NESTED COMMENT FLAG ?

				++c;
			}
		}

		++curr;
	}

	tkn->curr = curr;

	return 1;
}

static inline void __Tokenizer_readLineComment(Tokenizer *tkn) {
	const char *end = strchr(tkn->curr + 1, '\n');

	if(end == NULL) end = strchr(tkn->curr + 1, '\0');

	tkn->curr = end;
}

static inline Token* __Tokenizer_readStringOrChar(Tokenizer *tkn, const char *str, char qt, int id) {
	const char *curr = str + 1;

	while(1) {
		if(*curr == '\0') {
			// CHECK OPEN STRING FLAG

			break;
		}

		if(*curr == '\n') {
			// CHECK MULTI LINE STRING FLAG

			++tkn->line;

		} else if(*curr == '\\') {
			int i = 1;

			while(*(curr + i) == '\\') ++i;

			curr += i - 1;

			if(*(curr + 1) == qt) {
				if(i % 2 == 0) {
					++curr;

					break;
				}
			}

		} else if(*curr == qt) {
			++curr;

			break;
		}

		++curr;
	}

	tkn->curr = curr;

	return Tokenizer_createToken(tkn, id, str, (int) (curr - str));
}

static Token* __Tokenizer_tryCharEqToken(Tokenizer *tkn, char c, int id) {
	if(*tkn->curr == '=') {
		++tkn->curr;

		return Tokenizer_createToken(tkn, id, (char[]) {c, '='}, 2);
	}

	return Tokenizer_createToken(tkn, c, &c, 1);
}

static Token* __Tokenizer_tryBoolToken(Tokenizer *tkn, char c, int cce_id, int cc_id, int ce_id) {
	if(*tkn->curr == c) {
		++tkn->curr;

		if(*tkn->curr == '=') {
			++tkn->curr;

			return Tokenizer_createToken(tkn, cce_id, (char[]) {c, c, '='}, 3);
		}

		return Tokenizer_createToken(tkn, cc_id, (char[]) {c, c}, 2);
	}

	return __Tokenizer_tryCharEqToken(tkn, c, ce_id);
}

static inline Token* __Tokenizer_tryName(Tokenizer *tkn, const char *str) {
	const char *curr = tkn->curr;

	while(1) {
		char c = *curr;

		if(__Tokenizer_isNameChar(c)) {
			++curr;

		} else if(c == '.') {
			if(!tkn->dot_in_name) break;

			++curr;

		} else break;
	}

	tkn->curr = curr;

	return Tokenizer_createToken(tkn, TOKEN_ID_NAME, str, (int) (curr - str));
}

static inline Token* __Tokenizer_tryHexNumber(Tokenizer *tkn, const char *str) {
	/* tkn->curr POINTS TO 0^x */

	if(!__Tokenizer_isHexDigit(*++tkn->curr)) {
		tkn->err = TOKENIZER_ERROR_UNEXPECTEDSYMBOLINHEX;

		return NULL;
	}

	while(__Tokenizer_isHexDigit(*++tkn->curr));

	if(__Tokenizer_isNameChar(*tkn->curr)) {
		tkn->err = TOKENIZER_ERROR_UNEXPECTEDSYMBOLINHEX;

		return NULL;
	}

	return Tokenizer_createToken(tkn, TOKEN_ID_HEXINT, str, (int) (tkn->curr - str));
}

static inline Token* __Tokenizer_tryFloat(Tokenizer *tkn, const char *str) {
	/* tkn->curr POINTS TO .{DIGIT}^ */

	if(__Tokenizer_isDigit(*tkn->curr)) {
		while(__Tokenizer_isDigit(*++tkn->curr));
	}

	char c = *tkn->curr;

	if(c == 'e' || c == 'E') {
		c = *++tkn->curr;

		if(c == '-') {
			c = *++tkn->curr;
		}

		if(!__Tokenizer_isDigit(*++tkn->curr)) {
			tkn->err = TOKENIZER_ERROR_UNEXPECTEDSYMBOLINFLOAT;

			return NULL;
		}

		while(__Tokenizer_isDigit(*++tkn->curr));

		c = *tkn->curr;
	}

	if(c == 'd' || c == 'D' || c == 'f' || c == 'F') {
		c = *++tkn->curr;
	}

	if(__Tokenizer_isNameChar(c)) {
		tkn->err = TOKENIZER_ERROR_UNEXPECTEDSYMBOLINFLOAT;

		return NULL;
	}
	
	return Tokenizer_createToken(tkn, TOKEN_ID_FLOAT, str, (int) (tkn->curr - str));
}

static inline Token* __Tokenizer_tryNumber(Tokenizer *tkn, const char *str) {
	/* tkn->curr POINTS TO {DIGIT}^ */

	char c = *tkn->curr;

	if(c == 'x' || c == 'X') return __Tokenizer_tryHexNumber(tkn, str);

	if(__Tokenizer_isDigit(c)) {
		while(__Tokenizer_isDigit(*++tkn->curr));
	}

	c = *tkn->curr;

	if(c == '.') {
		if(!__Tokenizer_isDigit(*(tkn->curr + 1))) {
			return Tokenizer_createToken(tkn, TOKEN_ID_INT, str, (int) (tkn->curr - str));
		}

		tkn->curr += 2;

		return __Tokenizer_tryFloat(tkn, str);
	}

	if(c == 'l' || c == 'L') {
		c = *++tkn->curr;

		if(c == 'i' || c == 'I') {
			c = *++tkn->curr;
		}
	}

	if(__Tokenizer_isNameChar(c)) {
		tkn->err = TOKENIZER_ERROR_UNEXPECTEDSYMBOLININT;

		return NULL;
	}

	return Tokenizer_createToken(tkn, TOKEN_ID_INT, str, (int) (tkn->curr - str));
}

static Token* __Tokenizer_readToken(Tokenizer *tkn) {
	char c = *tkn->curr++;

	if(__Tokenizer_isDigit(c)) {
		return __Tokenizer_tryNumber(tkn, tkn->curr - 1);
	}

	if(__Tokenizer_isNameChar(c)) {
		return __Tokenizer_tryName(tkn, tkn->curr - 1);
	}

	if(c == '\"') {
		return __Tokenizer_readStringOrChar(tkn, tkn->curr - 1, c, TOKEN_ID_STRING);
	}

	if(c == '\'') {
		return __Tokenizer_readStringOrChar(tkn, tkn->curr - 1, c, TOKEN_ID_CHAR);
	}

	if(c == '-') {
		c = *tkn->curr;

		if(c == '-') {
			++tkn->curr;

			return Tokenizer_createToken(tkn, TOKEN_ID_DECR, "--", 2);
		}

		if(c == '>') {
			++tkn->curr;

			return Tokenizer_createToken(tkn, TOKEN_ID_DEREF, "->", 2);
		}

		return __Tokenizer_tryCharEqToken(tkn, '-', TOKEN_ID_SUBATT);
	}

	if(c == '+') {
		if(*tkn->curr == '+') {
			++tkn->curr;

			return Tokenizer_createToken(tkn, TOKEN_ID_INCR, "++", 2);
		}

		return __Tokenizer_tryCharEqToken(tkn, '+', TOKEN_ID_SUMATT);
	}

	if(c == '.') {
		if(strncmp(tkn->curr, "..", 2) == 0) {
			tkn->curr += 2;

			return Tokenizer_createToken(tkn, TOKEN_ID_VA, "...", 3);
		}

		// TRY NUMBER OR NAME ?

		return Tokenizer_createToken(tkn, '.', ".", 1);
	}

	if(c == '/') {
		c = *tkn->curr;

		if(c == '/') {
			__Tokenizer_readLineComment(tkn);

			return NULL;
		}

		if(c == '*') {
			__Tokenizer_readComment(tkn, tkn->curr + 1);

			return NULL;
		}

		return __Tokenizer_tryCharEqToken(tkn, '/', TOKEN_ID_DIVATT);
	}

	if(c == '&') return __Tokenizer_tryBoolToken(tkn, '&', TOKEN_ID_ANDATT, TOKEN_ID_AND, TOKEN_ID_BANDATT);
	if(c == '|') return __Tokenizer_tryBoolToken(tkn, '|', TOKEN_ID_ORATT, TOKEN_ID_OR, TOKEN_ID_BORATT);
	if(c == '~') return __Tokenizer_tryBoolToken(tkn, '~', TOKEN_ID_XORATT, TOKEN_ID_XOR, TOKEN_ID_BXORATT);

	if(c == '%') return __Tokenizer_tryCharEqToken(tkn, '%', TOKEN_ID_MODATT);
	if(c == '*') return __Tokenizer_tryCharEqToken(tkn, '*', TOKEN_ID_MULATT);
	if(c == '^') return __Tokenizer_tryCharEqToken(tkn, '^', TOKEN_ID_POWATT);

	if(c == '=') return __Tokenizer_tryCharEqToken(tkn, '=', TOKEN_ID_EQ);
	if(c == '>') return __Tokenizer_tryCharEqToken(tkn, '>', TOKEN_ID_GE);
	if(c == '<') return __Tokenizer_tryCharEqToken(tkn, '<', TOKEN_ID_LE);
	if(c == '!') return __Tokenizer_tryCharEqToken(tkn, '!', TOKEN_ID_NE);

	// CHECK IF SINGLE CHAR TOKEN IS VALID

	return Tokenizer_createToken(tkn, c, &c, 1);
}

static int __Tokenizer_goToNextToken(Tokenizer *tkn) {
	while(__Tokenizer_isSpace(*tkn->curr)) {
		if(*tkn->curr == '\n') ++tkn->line;

		++tkn->curr;
	}

	return *tkn->curr != '\0';
}

static int __Tokenizer_parse(Tokenizer *tkn, Queue *q) {
	while(1) {
		if(!__Tokenizer_goToNextToken(tkn)) {
			return 1; /* DONE */
		}

		Token *t = __Tokenizer_readToken(tkn);

		if(t != NULL) {
			Queue_insert(q, t);
		}

		if(tkn->err) return 0;
	}

	return 0;
}


void Tokenizer_destroyQueue(void *ptr) {
	Queue_destroy2(ptr, Token_destroy);
}

Queue* Tokenizer_parse(const char *src) {
	Tokenizer tkn[1];

	Tokenizer_init(tkn, src);

	Queue *q = Queue_create();

	int r = __Tokenizer_parse(tkn, q);

	Tokenizer_term(tkn);

	if(r) return q;

	Tokenizer_destroyQueue(q);

	return NULL;
}

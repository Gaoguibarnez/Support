
#include <stdio.h>

#include <stdarg.h>

#include <string.h>

#include <stdint.h>

#include <sys/stat.h>


#include "../Memory.h"


#ifndef BLOCKFILE_C
#define BLOCKFILE_C


typedef struct BlockFile {
	char *name;
	char *txt;

	int size;
	int cnt;

	struct BlockFile **chld;

	int ln_wdt_hint;
	
} BlockFile;

BlockFile* BlockFile_createLeaf(const char *txt) {
	char *txt_n = Memory_allocs(txt);

	if(txt_n == NULL) return NULL;

	BlockFile *blk = (BlockFile*) Memory_alloc(sizeof(BlockFile));

	blk->name = NULL;
	blk->txt = txt_n;

	blk->size = 0;
	blk->cnt = 0;

	blk->chld = NULL;

	blk->ln_wdt_hint = 0;

	return blk;
}

BlockFile* BlockFile_create(int size) {
	if(size < 2) size = 2;

	BlockFile *blk = (BlockFile*) Memory_alloc(sizeof(BlockFile));

	blk->name = NULL;
	blk->txt = NULL;

	blk->size = size;
	blk->cnt = 0;

	blk->chld = (BlockFile**) Memory_alloc(size * sizeof(BlockFile*));

	blk->ln_wdt_hint = 1;

	return blk;
}

BlockFile* BlockFile_createLeaff(const char *fmt, ...) {
	va_list args;

	char buf[4096];

	va_start(args, fmt);
	
	vsnprintf(buf, 4096, fmt, args);

	va_end(args);

	return BlockFile_createLeaf(buf);
}

void BlockFile_destroy(void *ptr) {
	BlockFile *blk = (BlockFile*) ptr;

	if(blk->name != NULL) Memory_free(blk->name);

	if(blk->txt != NULL) Memory_free(blk->txt);

	if(blk->chld != NULL) {
		int i, n = blk->cnt;

		for(i=0; i<n; i++) {
			BlockFile_destroy(blk->chld[i]);
		}

		Memory_free(blk->chld);
	}

	Memory_free(blk);
}


void BlockFile_setName(BlockFile *blk, const char *name) {
	if(blk->name != NULL) Memory_free(blk->name);

	blk->name = Memory_allocs(name);
}

char* BlockFile_getName(BlockFile *blk) {
	return blk->name;
}


int BlockFile_isLeaf(BlockFile *blk) {
	return blk->txt != NULL;
}

char* BlockFile_getText(BlockFile *blk) {
	return blk->txt;
}


BlockFile* BlockFile_search(BlockFile *blk, const char *name) {
	int i, n = blk->cnt;

	for(i=0; i<n; i++) {
		char *name_c = blk->chld[i]->name;

		if(name_c != NULL) {
			if(strcmp(name, name_c) == 0) return blk->chld[i];
		}
	}

	return NULL;
}


int BlockFile_size(BlockFile *blk) {
	return blk->cnt;
}

BlockFile* BlockFile_getBlock(BlockFile *blk, unsigned int idx) {
	if(idx >= blk->cnt) return NULL;

	return blk->chld[idx];
}

BlockFile** BlockFile_getBlocks(BlockFile *blk, int *size) {
	if(size != NULL) *size = blk->cnt;

	return blk->chld;
}

int BlockFile_addBlock(BlockFile *blk, BlockFile *chld) {
	if(blk->chld == NULL) return 0;

	if(blk->cnt >= blk->size) {
		int size = (5 * blk->size) / 3;

		BlockFile **tmp = (BlockFile**) Memory_alloc(size * sizeof(BlockFile*));

		memcpy(tmp, blk->chld, blk->cnt * sizeof(BlockFile*));

		Memory_free(blk->chld);

		blk->size = size;
		blk->chld = tmp;
	}

	blk->chld[blk->cnt++] = chld;

	return 1;
}

void BlockFile_setLineWidth(BlockFile *blk, int wdt) {
	blk->ln_wdt_hint = wdt;
}


BlockFile* BlockFile_newLeafBlock(BlockFile *blk, char *txt) {
	if(BlockFile_isLeaf(blk)) return NULL;

	BlockFile *new_blk = BlockFile_createLeaf(txt);

	BlockFile_addBlock(blk, new_blk);

	return new_blk;
}

BlockFile* BlockFile_newBlock(BlockFile *blk, int sz) {
	if(BlockFile_isLeaf(blk)) return NULL;

	BlockFile *new_blk = BlockFile_create(sz);

	BlockFile_addBlock(blk, new_blk);

	return new_blk;
}


static inline int __BlockFile_isSpace(char c) {
	return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

static void BlockFile_parseString(char *dst, const char *buf, int len) {
	while(1) {
		if(len <= 0) {
			*dst = '\0';

			return;
		}

		*dst = *buf;

		if(*buf == '\\') {
			++buf;
			--len;

			if(len > 0) {
				if(*buf != '\\') {
					*dst++ = *buf;

					++buf;
					--len;
				}
				else ++dst;
			}
			else ++dst;
		}
		else {
			++dst;

			++buf;
			--len;
		}
	}
}

static void BlockFile_setNameN(BlockFile *blk, const char *name, int len) {
	char tmp[len + 1];

	BlockFile_parseString(tmp, name, len);

	BlockFile_setName(blk, tmp);
}

static BlockFile* BlockFile_createLeafN(const char *txt, int len) {
	char tmp[len + 1];

	BlockFile_parseString(tmp, txt, len);

	return BlockFile_createLeaf(tmp);
}

static BlockFile* _BlockFile_parseBlock(const char *txt, const char **end);

static BlockFile* _BlockFile_parseBlockList(const char *txt, const char **end) {
	BlockFile *blk = BlockFile_create(2);

	while(1) {
		if(*txt++ != '<') goto error_ret;

		BlockFile *chld = _BlockFile_parseBlock(txt, &txt);

		if(chld == NULL) goto error_ret;

		BlockFile_addBlock(blk, chld);

		if(*txt++ != '>') goto error_ret;

		while(__BlockFile_isSpace(*txt)) ++txt;

		if(*txt == '>' || *txt == '\0') break;
	}

	*end = txt;

	return blk;

error_ret:
	BlockFile_destroy(blk);

	return NULL;
}

static BlockFile* _BlockFile_parseBlockInfo(const char *txt, const char **end) {
	while(__BlockFile_isSpace(*txt)) ++txt;

	if(*txt == '<') {
		return _BlockFile_parseBlockList(txt, end);
	}

	const char *info = txt;

	int was_esc = 0;

	while(1) {
		if(*txt == '\0') return NULL;

		if(*txt == '\\') was_esc = 1;

		else {
			if(!was_esc) {
				if(*txt == '<') return NULL;

				if(*txt == '>') break;
			}

			was_esc = 0;
		}

		++txt;
	}

	int info_l = (uintptr_t) (*end = txt) - (uintptr_t) info;

	return BlockFile_createLeafN(info, info_l);
}

static BlockFile* _BlockFile_parseBlock(const char *txt, const char **end) {
	while(__BlockFile_isSpace(*txt)) ++txt;

	if(*txt == '\0') {
		*end = txt;

		return BlockFile_createLeaf("");
	}

	if(*txt == '<') {
		return _BlockFile_parseBlockList(txt, end);
	}

	const char *name = txt;

	int name_l = - 1;

	while(1) {
		if(*txt == '\0') break;

		if(*(txt - 1) != '\\') {
			if(*txt == '<') return NULL;

			if(*txt == '>') break;

			if(*txt == ':') {
				name_l = (uintptr_t) txt - (uintptr_t) name;

				break;
			}
		}

		++txt;
	}

	if(name_l < 0) {
		name_l = (uintptr_t) (*end = txt) - (uintptr_t) name;

		return BlockFile_createLeafN(name, name_l);
	}

	BlockFile *blk = _BlockFile_parseBlockInfo(txt + 1, end);

	if(blk == NULL) return NULL;

	BlockFile_setNameN(blk, name, name_l);

	return blk;
}

BlockFile* BlockFile_parse(const char *txt) {
	BlockFile *blk = _BlockFile_parseBlock(txt, &txt);

	if(blk == NULL) return NULL;

	if(*txt != '\0') return NULL;

	return blk;
}

BlockFile* BlockFile_load(FILE *f) {
	int d = fileno(f);

	struct stat t;

	if(fstat(d, &t) != 0) return NULL;

	if(t.st_size < 0) return NULL;

	char *b = (char*) Memory_alloc(t.st_size + 1);

	int n = fread(b, 1, t.st_size, f);

	if(n > t.st_size) n = t.st_size;

	b[n] = '\0';

	BlockFile *blk = BlockFile_parse(b);

	Memory_free(b);

	return blk;
}


#define __BLOCKFILE_SNPRINTF(i, j, b, l, s) { i = snprintf(b + j, l, "%s", s); if(i < 0 || i >= l) return - 1; j += i; l -= i; }
#define __BLOCKFILE_WRITESTR(i, j, b, l, s, n) { i = _BlockFile_writeString(s, b + j, l, n); if(i < 0 || i >= l) return - 1; j += i; l -= i; }

static inline int _BlockFile_writeString(const char *t, char *b, int s, int n) {
	int i, j = 0;

	while(*t != '\0') {
		if(*t == '>' || *t == '<' || (!n && *t == ':')) {
			__BLOCKFILE_SNPRINTF(i, j, b, s, "\\");
		}

		__BLOCKFILE_SNPRINTF(i, j, b, s, ((char[]) {*t++, '\0'}));
	}

	return j;
}

static int _BlockFile_write(BlockFile *blk, char *b, int s, int d) {
	int l, k, i, j = 0;

	if(blk->name != NULL) {
		__BLOCKFILE_WRITESTR(i, j, b, s, blk->name, 0);

		__BLOCKFILE_SNPRINTF(i, j, b, s, ": ");
	}

	if(BlockFile_isLeaf(blk)) {
		__BLOCKFILE_WRITESTR(i, j, b, s, blk->txt, blk->name != NULL);

		return j;
	}

	__BLOCKFILE_SNPRINTF(i, j, b, s, "\n");

	for(k=0; k<blk->cnt; k++) {
		l = d;

		if(k % blk->ln_wdt_hint == 0) {
			while(--l >= 0) __BLOCKFILE_SNPRINTF(i, j, b, s, "\t");
		}

		__BLOCKFILE_SNPRINTF(i, j, b, s, "<");

		i = _BlockFile_write(blk->chld[k], b + j, s, d + 1);

		if(i < 0 || i >= s) return - 1;

		j += i;
		s -= i;

		if((k + 1) % blk->ln_wdt_hint == 0) {
			if(!BlockFile_isLeaf(blk->chld[k])) {
				l = d;

				while(--l >= 0) __BLOCKFILE_SNPRINTF(i, j, b, s, "\t");
			}

			__BLOCKFILE_SNPRINTF(i, j, b, s, ">\n");
		}
		else {
			__BLOCKFILE_SNPRINTF(i, j, b, s, ">");
		}
	}

	return j;
}

int BlockFile_write(BlockFile *blk, char *b, int s) {
	return _BlockFile_write(blk, b, s, 0);
}

int BlockFile_save(BlockFile *blk, FILE *f) {
	int s = 1 * 1048576;

	char *buf = Memory_alloc(s);

	if(buf == NULL) return 0;

	int r = BlockFile_write(blk, buf, s);

	if(r >= 0) fprintf(f, "%s", buf);

	Memory_free(buf);

	return r;
}


void BlockFile_print(BlockFile *blk) {
	int s = 1 * 1048576;

	char *buf = Memory_alloc(s);

	if(buf == NULL) return;

	int r = BlockFile_write(blk, buf, s);

	if(r >= 0) printf("%s\n", buf);

	Memory_free(buf);
}


#include "../BlockFile.h"


#endif

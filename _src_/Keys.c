
#include <stdio.h>

#include <stdlib.h>

#include <string.h>


#include "../Memory.h"

#include "../Mutex.h"


#include "../RedBlackTree.c"


#ifndef KEYS_C
#define KEYS_C


static const char __keys_null[] = "(null)";


static Mutex _keys_mtx;

static RedBlackTree *_keys_key = NULL;


static void _Key_free(void *ptr) {
	// free(ptr);
	Memory_free(ptr);
}

static inline void* _Key_alloc(const char *name) {
	// return (void*) strcpy((char*) malloc(1 + strlen(name)), name);
	return (void*) Memory_allocs(name);
}


void* Keys_has(const char *name) {
	if(name == NULL) {
		return NULL;
	}

	Mutex_lock(&_keys_mtx);

	void *key = RedBlackTree_search(_keys_key, (void*) name);

	Mutex_unlock(&_keys_mtx);

	return key;
}

void* Keys_get(const char *name) {
	if(name == NULL) return NULL;

	Mutex_lock(&_keys_mtx);

	void *key = RedBlackTree_search(_keys_key, (void*) name);

	if(key == NULL) {
		key = _Key_alloc(name);

		RedBlackTree_insert(_keys_key, key, key);
	}

	Mutex_unlock(&_keys_mtx);

	return key;
}


static void* _Keys_hasi(const char *nm, unsigned int idx, int gen) {
	#define _KEYS_IDXSZ (2 * sizeof(unsigned int) + 2)

	char idx_key_buf[_KEYS_IDXSZ], *idx_key = idx_key_buf + (_KEYS_IDXSZ - 1);

	int kl = 0;

	if(idx == 0) {
		idx_key = "0";

		++kl;
	}
	else {
		*idx_key = '\0';

		while(idx > 0) {
			*(--idx_key) = "0123456789ABCDEF"[idx & 0xF];

			idx >>= 4;

			++kl;
		}
	}

	if(nm == NULL) nm = __keys_null;

	int nl = strlen(nm);

	char key[2 + kl + nl];

	memcpy(key, nm, nl);

	*(key + nl) = '.';

	strcpy(key + nl + 1, idx_key);

	if(gen) return Keys_get(key);

	return Keys_has(key);
}

void* Keys_hasi(const char *nm, unsigned int idx) {
	return _Keys_hasi(nm, idx, 0);
}

void* Keys_geti(const char *nm, unsigned int idx) {
	return _Keys_hasi(nm, idx, 1);
}


const char* Keys_toString(void *key) {
	if(key == NULL) {
		return __keys_null;
	}

	return (const char*) key;
}


static int _keys_init = 0;

void Keys_init() {
	glbMutex_lock();

	if(_keys_init++ == 0) {
		Memory_init();

		Mutex_init(&_keys_mtx);

		_keys_key = RedBlackTree_create(RedBlackTree_stringComparator);
	}

	glbMutex_unlock();
}

void Keys_term() {
	glbMutex_lock();

	if(--_keys_init == 0) {
		Mutex_term(&_keys_mtx);

		RedBlackTree_clear(_keys_key, NULL, _Key_free);

		RedBlackTree_destroy(_keys_key);

		Memory_term();

	} else if(_keys_init < 0) {
		// TODO: LOG
		
		_keys_init = 0;
	}

	glbMutex_unlock();
}


#endif


#include "../Keys.h"


#include <time.h>


#include "../Thread.h"



#if defined _WIN32



#include <process.h>

#define WIN32_LEAN_AND_MEAN

#include <windows.h>


#define __THREAD_THREAD(name) static void name(void *arg)

#define __THREAD_CREATE(fnc, arg) (_beginthread(fnc, 0, arg) != -1L)

#define __THREAD_EXIT() _endthread()


#define __THREAD_GETID() (int) GetCurrentThreadId()



#elif defined __linux__



#include <pthread.h>

#include <unistd.h>

#include <sys/syscall.h>


pthread_t __thread__;

#define __THREAD_THREAD(name) static void* name(void *arg)

#define __THREAD_CREATE(fnc, arg) (pthread_create(&__thread__, NULL, fnc, arg) == 0)

#define __THREAD_EXIT() pthread_exit(NULL)

#define __THREAD_GETID() (int) syscall(SYS_gettid)



#else

#error THREAD: UNSUPPORTED PLATFORM

#endif


int Thread_getID() {
	return __THREAD_GETID();
}

void Thread_sleep(double t) {
	if(t <= 0.0) return;

	int sec = (int) t;
	int nsec = (int) (1000000000.0 * (t - (double) sec));

	struct timespec ts;

	ts.tv_sec = sec;
	ts.tv_nsec = nsec;

	nanosleep(&ts, NULL);
}

void Thread_sleepf(float t) {
	Thread_sleep((double) t);
}


// typedef struct Thread {
// 	void* (*run)(void *arg);

// 	int str;
// 	int end;

// 	void *arg;
// 	void *ret;

// } Thread;


void Thread_init(Thread *thr, void* (*run)(void *arg)) {
	thr->run = run;

	thr->str = 0;
	thr->end = 0;
}

void Thread_term(Thread *thr) {
}


Thread* Thread_create(void* (*run)(void*)) {
	Thread *thr = (Thread*) Memory_alloc(sizeof(Thread));

	Thread_init(thr, run);

	return thr;
}

void Thread_destroy(void *ptr) {
	Thread_term((Thread*) ptr);

	Memory_free(ptr);
}


__THREAD_THREAD(_Thread_thread) {
	Thread *thr = (Thread*) arg;

	thr->ret = thr->run(thr->arg);

	thr->end = 1;

	__THREAD_EXIT();
}

int Thread_start(Thread *thr, void *arg) {
	if(thr->str != 0 && thr->end == 0) {
		return 0;
	}

	thr->arg = arg;

	thr->ret = NULL;

	thr->str = 1;

	thr->end = 0;

	if(__THREAD_CREATE(_Thread_thread, thr)) {
		return 1;
	}

	thr->str = 0;

	return 0;
}

void Thread_wait(Thread *thr) {
	#define __THREAD_WAIT_SLEEP_TIME_NANO 0.000001

	while(thr->str != 0 && thr->end == 0) {
		Thread_sleep(__THREAD_WAIT_SLEEP_TIME_NANO);
	}
}


int Thread_isRunning(Thread *thr) {
	return thr->str != 0 && thr->end == 0;
}

void* Thread_getReturn(Thread *thr) {
	return thr->ret;
}


struct _qThread_args {
	void* (*run)(void*);
	void* arg;
};

__THREAD_THREAD(_Thread_quickThread) {
	struct _qThread_args *args = (struct _qThread_args*) arg;

	void* (*r)(void*) = args->run;
	void *a = args->arg;

	Memory_free(args);

	r(a);

	__THREAD_EXIT();
}

int QuickThread_start(void* (*run)(void*), void *arg) {
	struct _qThread_args *args = (struct _qThread_args*) Memory_alloc(sizeof(struct _qThread_args));

	args->run = run;
	args->arg = arg;

	if(__THREAD_CREATE(_Thread_quickThread, args)) {
		return 1;
	}

	Memory_free(args);

	return 0;
}


#include <stdio.h>

#include <stdarg.h>

#include <string.h>

#include <stdint.h>


#ifndef OUTPUT_C
#define OUTPUT_C

#include "../getRealTime.c"


#define OUTPUT_MAXSIZE 4096


static void (*_output_print)(const char *) = NULL;

void Output_overridePrint(void (*prt)(const char *)) {
	_output_print = prt;
}


static char _output_nolnbuf[OUTPUT_MAXSIZE] = {0};

static int _output_nolnbuf_idx = 0;

static void _Output_appendToLineBuf(const char *msg, const char *lst) {
	int len = lst ? (int) ((uintptr_t) lst - (uintptr_t) msg) : strlen(msg);

	int rmn = OUTPUT_MAXSIZE - _output_nolnbuf_idx;

	if(len >= rmn) len = rmn - 1;

	memcpy(_output_nolnbuf + _output_nolnbuf_idx, msg, len);

	_output_nolnbuf_idx += len;

	_output_nolnbuf[_output_nolnbuf_idx] = '\0';

}

static void _Output_print(const char *msg) {
	if(_output_print != NULL) {
		if(_output_nolnbuf_idx > 0) {
			_Output_appendToLineBuf(msg, NULL);

			msg = _output_nolnbuf;
		}

		_output_print(msg);
	}
	else {
		printf("%s%s\n", _output_nolnbuf, msg);
	}

	_output_nolnbuf[0] = '\0';

	_output_nolnbuf_idx = 0;
}


static char *_output_logfile = NULL;

static int _Output_log(const char *msg0, const char *msg1) {
	if(_output_logfile == NULL) return 1;

	FILE *file = fopen(_output_logfile, "ab");

	if(file == NULL) {
		_output_logfile = NULL;

		return 0;
	}

	fprintf(file, "%s%s\n", msg0, msg1);

	fclose(file);

	return 1;
}


static int _output_log_prt = 0;

void Output_print(const char *msg) {
	if(_output_log_prt) _Output_log(_output_nolnbuf, msg);

	_Output_print(msg);
}

void Output_printf(const char *fmt, ...) {
	char tmp[OUTPUT_MAXSIZE];

	va_list args;

	va_start(args, fmt);

	vsnprintf(tmp, OUTPUT_MAXSIZE, fmt, args);

	va_end(args);

	Output_print(tmp);
}


void Output_printnoln(const char *msg) {
	const char *lln = strrchr(msg, '\n');

	if(lln != NULL) {
		_Output_appendToLineBuf(msg, lln);

		Output_print("");

		msg = lln + 1;
	}

	_Output_appendToLineBuf(msg, NULL);
}


void Output_error(const char *msg) {
	char tmp[OUTPUT_MAXSIZE];

	snprintf(tmp, OUTPUT_MAXSIZE, "ERROR: %s", msg);

	Output_print(msg);
}

void Output_errorf(const char *fmt, ...) {
	char tmp[OUTPUT_MAXSIZE] = "ERROR: ";

	va_list args;

	va_start(args, fmt);

	vsnprintf(tmp + 7, OUTPUT_MAXSIZE - 7, fmt, args);

	va_end(args);

	Output_print(tmp);
}


void Output_warning(const char *msg) {
	char tmp[OUTPUT_MAXSIZE];

	snprintf(tmp, OUTPUT_MAXSIZE, "WARNING: %s", msg);

	Output_print(tmp);
}

void Output_warningf(const char *fmt, ...) {
	char tmp[OUTPUT_MAXSIZE] = "WARNING: ";

	va_list args;

	va_start(args, fmt);

	vsnprintf(tmp + 9, OUTPUT_MAXSIZE - 9, fmt, args);

	va_end(args);

	Output_print(tmp);
}


static char _output_logfile_buf[256];

static double _output_logtime = - 1.0;

static int _output_prt_log = 0;

void Output_log(const char *msg) {
	if(_output_logtime == - 1.0) _output_logtime = getRealTime();

	char tm[32];

	snprintf(tm, 32, "[%.3lf] ", getRealTime() - _output_logtime);

	if(!_Output_log(tm, msg)) {
		Output_errorf("could not open log file '%s'.", _output_logfile_buf);
	}

	if(_output_prt_log) {
		Output_printnoln("LOG: ");
		Output_printnoln(tm);
		_Output_print(msg);
	}
}

void Output_logf(const char *fmt, ...) {
	char tmp[OUTPUT_MAXSIZE];

	va_list args;

	va_start(args, fmt);

	vsnprintf(tmp, OUTPUT_MAXSIZE, fmt, args);

	va_end(args);

	Output_log(tmp);
}


void Output_setLogFile(const char *log) {
	if(log == NULL) {
		_output_logfile = NULL;
	}
	else if(strlen(log) < 256) {
		_output_logfile = strcpy(_output_logfile_buf, log);
	}

	if(_output_logtime == - 1.0) _output_logtime = getRealTime();
}

void Output_initLogFile(const char *log) {
	Output_setLogFile(log);

	if(_output_logfile != NULL) {
		FILE *file = fopen(_output_logfile, "wb");

		if(file != NULL) fclose(file);

		else {
			Output_errorf("could not open log file '%s'.", _output_logfile);

			_output_logfile = NULL;
		}
	}
}

void Output_enablePrintLog(int v) {
	_output_prt_log = v;
}

void Output_enableLogPrint(int v) {
	_output_log_prt = v;
}



#define OUTPUT_DEBUG_MAX 16

static unsigned int _output_debug = 0;

void Output_setDebugLevel(int v) {
	if(v > OUTPUT_DEBUG_MAX) v = OUTPUT_DEBUG_MAX;

	_output_debug = v;
}

void _pvt_Output_debug(unsigned int lvl, char *msg) {
	if(lvl <= _output_debug) Output_log(msg);
}

void _pvt_Output_debugf(unsigned int lvl, char *fmt, va_list args) {
	if(lvl <= _output_debug) {
		char tmp[OUTPUT_MAXSIZE];

		vsnprintf(tmp, OUTPUT_MAXSIZE, fmt, args);

		Output_log(tmp);
	}
}


#include "../Output.h"

#endif

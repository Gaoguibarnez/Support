
#define MEMBUCKETS_DISABLE 0


#define MEMBUCKETS_RECLSIZE 4

static int _mem_bck_recl_cnt = 0;

static void *_mem_bck_recl[MEMBUCKETS_RECLSIZE];

static inline void* _MemBucket_ralloc(size_t size) {
	if(_mem_bck_recl_cnt > 0) return _mem_bck_recl[--_mem_bck_recl_cnt];

	return malloc(size);
}

static inline void _MemBucket_rfree(void *ptr) {
	if(_mem_bck_recl_cnt >= MEMBUCKETS_RECLSIZE) {
		free(ptr);

	} else {
		_mem_bck_recl[_mem_bck_recl_cnt++] = ptr;
	}
}

static inline void _MemBucket_rclear() {
	while(--_mem_bck_recl_cnt >= 0) free(_mem_bck_recl[_mem_bck_recl_cnt]);
}



static unsigned int _mem_bck_alloc = 0;

static unsigned int _mem_bck_alloc_total = 0;

static unsigned int _mem_bck_alloc_max = 0;


#ifndef MEMBUCKET_BUFSIZE
#define MEMBUCKET_BUFSIZE (262144)
#endif

typedef struct MemBucket {
	struct MemBucket *prv;
	struct MemBucket *nxt;

	void *recl;
	int recl_cnt;

	int usz;
	int off;

} MemBucket;

#define MEMBUCKET_SIZE(sz) ((sz) + sizeof(void*) * ((sizeof(MemBucket) + sizeof(void*) - 1) / sizeof(void*)))

static inline void _MemBucket_init(MemBucket *bck, int usz) {
	bck->prv = NULL;
	bck->nxt = NULL;

	bck->recl = NULL;
	bck->recl_cnt = 0;

	bck->usz = usz;
	bck->off = 0;
}

static MemBucket* _MemBucket_create(int usz) {
	MemBucket *bck = (MemBucket*) _MemBucket_ralloc(MEMBUCKET_SIZE(MEMBUCKET_BUFSIZE));

	if(bck == NULL) return NULL;

	_MemBucket_init(bck, usz);

	// printf(">>> alloc mem bucket (%d): %p\n", usz, bck);

	return bck;
}

static void _MemBucket_destroy(void *ptr) {
	// MemBucket *bck = (MemBucket*) ptr;

	// printf(">>> free mem bucket (%d): %p\n", bck->usz, bck);

	_MemBucket_rfree(ptr);
}


static int _MemBucket_isFull(MemBucket *bck) {
	return bck->off >= MEMBUCKET_BUFSIZE && bck->recl_cnt == 0;
}

static int _MemBucket_isEmpty(MemBucket *bck) {
	return bck->off == bck->recl_cnt;
}


static inline void* _MemBucket_alloc(MemBucket *bck) {
	if(bck->recl != NULL) {
		void *ptr = bck->recl;

		bck->recl = *(void**) ptr;

		bck->recl_cnt -= bck->usz;

		return ptr;
	}

	if(bck->off < MEMBUCKET_BUFSIZE) {
		void *ptr = ptrsum(bck, MEMBUCKET_SIZE(bck->off));

		bck->off += bck->usz;

		return ptr;
	}

	return NULL;
}

static inline void _MemBucket_free(MemBucket *bck, void *ptr) {
	*(void**) ptr = bck->recl;

	bck->recl = ptr;

	bck->recl_cnt += bck->usz;
}


static void MemBucket_printLeaks(MemBucket *bck) {
	int usz = bck->usz;

	int i, n = bck->off / usz;

	char *map = (char*) malloc(n);

	memset(map, 0, n);

	void **ptr = bck->recl;

	while(ptr != NULL) {
		int off = (uintptr_t) ptr - (uintptr_t) bck - MEMBUCKET_SIZE(0);

		map[off / usz] = 1;

		ptr = *ptr;
	}

	for(i=0; i<n; i++) {
		if(map[i] == 0) {
			printf(">>> %p (%d)\n", ptrsum(bck, MEMBUCKET_SIZE(i * usz)), usz);
		}
	}

	free(map);

	if(bck->nxt != NULL) MemBucket_printLeaks(bck->nxt);
}


#ifndef MEMBUCKET_WIDTH
#define MEMBUCKET_WIDTH 6
#endif

static RedBlackTree *_mem_bck_tree = NULL;

static MemBucket *_mem_bck[MEMBUCKET_WIDTH] = {0};

static unsigned int _mem_bck_alloc_i[MEMBUCKET_WIDTH] = {0};

static unsigned int _mem_bck_alloc_max_i[MEMBUCKET_WIDTH] = {0};

static unsigned int _mem_bck_alloc_total_i[MEMBUCKET_WIDTH] = {0};


#define MEMBUCKETS_MINSIZE (2 * sizeof(void*))
#define MEMBUCKETS_SIZESTEPMULT 1

static int _MemBuckets_getIdx(size_t size) {
	int idx = 0;

	size_t b_sz = MEMBUCKETS_MINSIZE;

	while(size > b_sz) {
		b_sz <<= MEMBUCKETS_SIZESTEPMULT;

		++idx;
	}

	return idx;
}


void* MemBuckets_alloc(size_t size) {
	if(MEMBUCKETS_DISABLE) return NULL;

	int idx = _MemBuckets_getIdx(size);

	if(idx >= MEMBUCKET_WIDTH) return NULL;

	MemBucket *bck = _mem_bck[idx];

	if(_mem_bck[idx] == NULL) {
		bck = _mem_bck[idx] = _MemBucket_create(MEMBUCKETS_MINSIZE << (idx * MEMBUCKETS_SIZESTEPMULT));

		if(bck == NULL) return NULL;

		++_mem_bck_alloc;

		++_mem_bck_alloc_total;

		if(_mem_bck_alloc > _mem_bck_alloc_max) {
			_mem_bck_alloc_max = _mem_bck_alloc;
		}

		RedBlackTree_insert(_mem_bck_tree, bck, bck);
	}

	void *ptr = _MemBucket_alloc(bck);

	if(_MemBucket_isFull(bck)) {
		_mem_bck[idx] = bck->nxt;

		if(bck->nxt) bck->nxt->prv = NULL;

		bck->nxt = NULL;
	}

	++_mem_bck_alloc_i[idx];

	++_mem_bck_alloc_total_i[idx];

	if(_mem_bck_alloc_i[idx] > _mem_bck_alloc_max_i[idx]) {
		_mem_bck_alloc_max_i[idx] = _mem_bck_alloc_i[idx];
	}

	return ptr;
}

int MemBuckets_freeWithReturn(void *ptr) {
	if(MEMBUCKETS_DISABLE) return 0;

	MemBucket *bck = RedBlackTree_search(_mem_bck_tree, ptr);

	if(bck == NULL) return 0;

	// TODO: CHECK ptr ALIGNMENT WITH bck

	int fll = _MemBucket_isFull(bck);

	_MemBucket_free(bck, ptr);

	int idx = _MemBuckets_getIdx(bck->usz);

	if(_MemBucket_isEmpty(bck)) {
		if(!fll) {
			if(bck->nxt != NULL) {
				bck->nxt->prv = bck->prv;
			}

			if(bck->prv == NULL) {
				_mem_bck[idx] = bck->nxt;

			} else {
				bck->prv->nxt = bck->nxt;
			}
		}

		RedBlackTree_remove(_mem_bck_tree, bck);

		--_mem_bck_alloc;

		_MemBucket_destroy(bck);

	} else if(fll) {
		int idx = _MemBuckets_getIdx(bck->usz);

		bck->nxt = _mem_bck[idx];

		if(bck->nxt) bck->nxt->prv = bck;

		_mem_bck[idx] = bck;
	}

	--_mem_bck_alloc_i[idx];

	return 1;
}

void MemBuckets_free(void *ptr) {
	MemBuckets_freeWithReturn(ptr);
}

void* MemBuckets_realloc(void *ptr, size_t size) {
	if(MEMBUCKETS_DISABLE) return NULL;

	MemBucket *bck = RedBlackTree_search(_mem_bck_tree, ptr);

	if(bck == NULL) return NULL;

	if(size <= bck->usz) return ptr;

	void *new_ptr = MemBuckets_alloc(size);

	memcpy(new_ptr, ptr, bck->usz);

	MemBuckets_free(ptr);

	return new_ptr;
}


static int _MemBucket_comparator(void *ptr, void *bck) {
	uintptr_t v1 = (uintptr_t) ptr;
	uintptr_t v2 = (uintptr_t) bck;

	if(v1 < v2) return - 1;

	return v1 >= v2 + MEMBUCKET_SIZE(MEMBUCKET_BUFSIZE) ? 1 : 0;
}

static inline void MemBuckets_init() {
	_mem_bck_tree = RedBlackTree_create(_MemBucket_comparator);

	memset(_mem_bck, 0, MEMBUCKET_WIDTH * sizeof(MemBucket*));

	_mem_bck_alloc = 0;

	_mem_bck_alloc_max = 0;

	_mem_bck_alloc_total = 0;

	memset(_mem_bck_alloc_i, 0, MEMBUCKET_WIDTH * sizeof(unsigned int));

	memset(_mem_bck_alloc_max_i, 0, MEMBUCKET_WIDTH * sizeof(unsigned int));

	memset(_mem_bck_alloc_total_i, 0, MEMBUCKET_WIDTH * sizeof(unsigned int));
}

static inline void MemBuckets_term() {
	RedBlackTree_clear(_mem_bck_tree, _MemBucket_destroy, NULL);

	RedBlackTree_destroy(_mem_bck_tree);

	_MemBucket_rclear();

	_mem_bck_tree = NULL;
}


static inline void MemBuckets_printLeaks() {
	int i;

	for(i=0; i<MEMBUCKET_WIDTH; i++) {
		if(_mem_bck[i] != NULL) MemBucket_printLeaks(_mem_bck[i]);
	}
}


static inline int MemBuckets_hasFootprint() {
	return _mem_bck_alloc > 0;
}

static inline void MemBuckets_printFootprint(int f) {
	if(f || MemBuckets_hasFootprint()) {
		printf("\tbuckets:\n");
		printf("\t\tused: %s\n", Memory_toString(_mem_bck_alloc * MEMBUCKET_SIZE(MEMBUCKET_BUFSIZE)));
		printf("\t\tmax used: %s\n", Memory_toString(_mem_bck_alloc_max * MEMBUCKET_SIZE(MEMBUCKET_BUFSIZE)));
		printf("\t\ttotal used: %s\n", Memory_toString(_mem_bck_alloc_total * MEMBUCKET_SIZE(MEMBUCKET_BUFSIZE)));

		int i;

		for(i=0; i<MEMBUCKET_WIDTH; i++) {
			if(_mem_bck_alloc_total_i[i] > 0 && (f || _mem_bck_alloc_i[i] > 0)) {
				size_t sz = MEMBUCKETS_MINSIZE * (1 << (i * MEMBUCKETS_SIZESTEPMULT));

				printf("\tbucket[%d] (%s):\n", i, Memory_toString(sz));
				printf("\t\tused: %s (%d)\n", Memory_toString(_mem_bck_alloc_i[i] * sz), _mem_bck_alloc_i[i]);
				printf("\t\tmax used: %s (%d)\n", Memory_toString(_mem_bck_alloc_max_i[i] * sz), _mem_bck_alloc_max_i[i]);
				printf("\t\ttotal used: %s (%d)\n", Memory_toString(_mem_bck_alloc_total_i[i] * sz), _mem_bck_alloc_total_i[i]);
			}
		}
	}
}

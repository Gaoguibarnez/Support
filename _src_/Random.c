
#include "../Rand64.c"


static Rand64 __rnd_seed = {{'T', 'M'}, 0, 0, 0};

void randomSeed(int s) {
	Rand64_init(&__rnd_seed, s);
}

void randomSeed64(uint64_t s) {
    Rand64_init(&__rnd_seed, s);
}

unsigned int randomInt(unsigned int rng) {
	return Rand64_int(&__rnd_seed, rng);
}

double randomDouble() {
    return Rand64_double(&__rnd_seed);
}

double randomGaussianDouble(double m, double s) {
    return Rand64_gaussianDouble(&__rnd_seed, m, s);
}

double randomGammaDouble(double a, double b) {
    return Rand64_gammaDouble(&__rnd_seed, a, b);
}

float randomFloat() {
	return Rand64_float(&__rnd_seed);
}

float randomGaussianFloat(float m, float s) {
	return Rand64_gaussianFloat(&__rnd_seed, m, s);
}

float randomGammaFloat(float a, float b) {
	return Rand64_gammaFloat(&__rnd_seed, a, b);
}

char* randomString(char *b, int n) {
	return Rand64_string(&__rnd_seed, b, n);
}

unsigned int randomPoissonInt(float l) {
	return Rand64_poissonInt(&__rnd_seed, l);
}


float symRandomFloat() {
	return 2.0f * randomFloat() - 1.0f;
}



#include "../Random.h"

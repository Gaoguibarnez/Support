
#include <stdio.h>

#include <time.h>


#include "../Output.h"

#include "../Mutex.h"



#if defined _WIN32



#include <process.h>

#define WIN32_LEAN_AND_MEAN

#include <windows.h>


typedef CRITICAL_SECTION Mutex;

#define __MUTEX_INIT(mtx) InitializeCriticalSection(mtx)

#define __MUTEX_TERM(mtx) DeleteCriticalSection(mtx)

#define __MUTEX_LOCK(mtx) EnterCriticalSection(mtx)

#define __MUTEX_UNLOCK(mtx) LeaveCriticalSection(mtx)

#define __MUTEX_TRYLOCK(mtx) TryEnterCriticalSection(mtx)



#elif defined __linux__



#include <pthread.h>


typedef pthread_mutex_t Mutex;

static inline void __MUTEX_INIT(Mutex *mtx) {
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);

	pthread_mutex_init(mtx, &attr);

	pthread_mutexattr_destroy(&attr);
}

#define __MUTEX_TERM(mtx) pthread_mutex_destroy(mtx)

#define __MUTEX_LOCK(mtx) pthread_mutex_lock(mtx)

#define __MUTEX_UNLOCK(mtx) pthread_mutex_unlock(mtx)

#define __MUTEX_TRYLOCK(mtx) pthread_mutex_trylock(mtx)

// #define __MUTEX_STATICINIT PTHREAD_RECURSIVE_MUTEX_INITIALIZER



#else

#error THREAD: UNSUPPORTED PLATFORM

#endif


void Mutex_init(Mutex *mtx) {
	__MUTEX_INIT(mtx);
}

void Mutex_term(Mutex *mtx) {
	__MUTEX_TERM(mtx);
}


int Mutex_tryLock(Mutex *mtx) {
	return __MUTEX_TRYLOCK(mtx);
}


void Mutex_lock(Mutex *mtx) {
	__MUTEX_LOCK(mtx);
}

void Mutex_unlock(Mutex *mtx) {
	__MUTEX_UNLOCK(mtx);
}


void Mutex_lock2(Mutex *mtx, int lck) {
	if(lck == 0) {
		Mutex_unlock(mtx);

	} else {
		Mutex_lock(mtx);
	}
}



// DEPRECATED: Mutex IS ALREADY RECURSIVE

// typedef struct recMutex {
// 	Mutex mtx;

// 	int thr;
// 	int cnt;

// } recMutex;


void recMutex_init(recMutex *mtx) {
	__MUTEX_INIT(&mtx->mtx);

	mtx->thr = - 1;
	mtx->cnt = 0;
}

void recMutex_term(recMutex *mtx) {
	__MUTEX_TERM(&mtx->mtx);
}


int recMutex_tryLock(recMutex *mtx, int thr) {
	if(mtx->thr == thr) {
		++mtx->cnt;

	} else {
		if(__MUTEX_TRYLOCK(&mtx->mtx) == 0) {
			return 0;
		}

		mtx->thr = thr;

		mtx->cnt = 0;
	}

	return 1;
}


void recMutex_lock(recMutex *mtx, int thr) {
	if(mtx->thr == thr) {
		++mtx->cnt;

	} else {
		__MUTEX_LOCK(&mtx->mtx);

		mtx->thr = thr;

		mtx->cnt = 0;
	}
}

int recMutex_unlock(recMutex *mtx) {
	if(--mtx->cnt > 0) {
		return 0;
	}

	mtx->thr = - 1;

	__MUTEX_UNLOCK(&mtx->mtx);

	return 1;
}



// typedef struct rwMutex {
// 	Mutex mtx;

// 	int w;
// 	int r;

// } rwMutex;


void rwMutex_init(rwMutex *mtx) {
	__MUTEX_INIT(&mtx->mtx);

	mtx->w = 0;
	mtx->r = 0;
}

void rwMutex_term(rwMutex *mtx) {
	__MUTEX_TERM(&mtx->mtx);
}


int rwMutex_tryWriteLock(rwMutex *mtx) {
	__MUTEX_LOCK(&mtx->mtx);

	if(mtx->w == 0) {
		mtx->w = 1;
	}
	
	__MUTEX_UNLOCK(&mtx->mtx);

	if(mtx->r <= 0) {
		return 1;
	}

	mtx->w = 0;

	return 0;
}

int rwMutex_tryReadLock(rwMutex *mtx) {
	__MUTEX_LOCK(&mtx->mtx);

	if(mtx->w == 0) {
		++mtx->r;

		__MUTEX_UNLOCK(&mtx->mtx);

		return 1;
	}
	
	__MUTEX_UNLOCK(&mtx->mtx);

	return 0;
}


void rwMutex_writeLock(rwMutex *mtx) {
	while(1) {
		__MUTEX_LOCK(&mtx->mtx);

		if(mtx->w == 0) {
			mtx->w = 1;

			__MUTEX_UNLOCK(&mtx->mtx);

			break;
		}
		
		__MUTEX_UNLOCK(&mtx->mtx);
	}

	while(mtx->r > 0);
}

void rwMutex_writeUnlock(rwMutex *mtx) {
	mtx->w = 0;
}


void rwMutex_readLock(rwMutex *mtx) {
	while(1) {
		__MUTEX_LOCK(&mtx->mtx);

		if(mtx->w == 0) {
			++mtx->r;

			__MUTEX_UNLOCK(&mtx->mtx);

			break;
		}
		
		__MUTEX_UNLOCK(&mtx->mtx);
	}
}

void rwMutex_readUnlock(rwMutex *mtx) {
	__MUTEX_LOCK(&mtx->mtx);

	--mtx->r;

	__MUTEX_UNLOCK(&mtx->mtx);
}



#ifdef __MUTEX_STATICINIT
#define _GLBMUTEX_INIT 1
#define _GLBMUTEX __MUTEX_STATICINIT
#else
#define _GLBMUTEX_INIT 0
#define _GLBMUTEX {0}
#endif

volatile int _init_mtx_init = _GLBMUTEX_INIT;

static Mutex _init_mtx = _GLBMUTEX;

void glbMutex_init();

static void _glbMutex_initMT() {
	if(!_init_mtx_init) {
		glbMutex_init();

		Output_logf("WARNING: UNSAFE glbMutex INITIALIZATION. (SHOULD BE FINE IF THIS MESSAGE APPEARS ONLY ONCE)");
	}
}

void glbMutex_lock() {
	_glbMutex_initMT();
	
	Mutex_lock(&_init_mtx);
}

void glbMutex_unlock() {
	Mutex_unlock(&_init_mtx);
}

int glbMutex_tryLock() {
	_glbMutex_initMT();
	
	Mutex_tryLock(&_init_mtx);

	return 1;
}

void glbMutex_init() {
	if(_init_mtx_init++ == 0) {
		Mutex_init(&_init_mtx);
	}
}

void glbMutex_term() {
	if(--_init_mtx_init == 0) {
		Mutex_term(&_init_mtx);

	} else if(_init_mtx_init < 0) {
		_init_mtx_init = 0;
	}
}


#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include <dirent.h>

#include <sys/stat.h>


#ifndef FILE_SINGLETHREADED
#include "../Mutex.h"

#include "../Thread.h"
#endif


// #if defined __linux__

// FILE* File_openMem(void *buf, int size, const char *mode) {
// 	if(mode == NULL) {
// 		mode = "r+";
// 	}

// 	return fmemopen(buf, size, mode);
// }

// #else

// FILE* File_openMem(void *buf, int size, const char *mode) {
// 	if(mode == NULL) {
// 		mode = "w+";
// 	}

// 	FILE *f = fopen("NUL", mode);

// 	if(f == NULL) {
// 		return NULL;
// 	}

// 	if(setvbuf(f, buf, _IOFBF, size) == 0) {
// 		return f;
// 	}

// 	fclose(f);

// 	return NULL;
// }

// #endif


#ifndef FILE_SINGLETHREADED

static int __file_mtx_init = 0;

static Mutex __memfile_mtx;

static recMutex __file_mtx;

static inline void _File_initMT() {
	if(__file_mtx_init++ == 0) {
		Mutex_init(&__memfile_mtx);

		recMutex_init(&__file_mtx);
	}
}

static inline void _File_termMT() {
	if(--__file_mtx_init == 0) {
		Mutex_term(&__memfile_mtx);

		recMutex_term(&__file_mtx);

	} else if(__file_mtx_init < 0) {
		__file_mtx_init = 0;
	}
}

#endif

static inline void _File_lock() {	
	#ifndef FILE_SINGLETHREADED
	_File_initMT();

	recMutex_lock(&__file_mtx, Thread_getID());
	#endif
}

static inline void _File_unlock() {	
	#ifndef FILE_SINGLETHREADED
	recMutex_unlock(&__file_mtx);

	_File_termMT();
	#endif
}

// static inline void _File_lockmem() {
// 	#ifndef FILE_SINGLETHREADED
// 	_File_initMT();

// 	Mutex_lock(&__memfile_mtx);
// 	#endif
// }

// static inline void _File_unlockmem() {
// 	#ifndef FILE_SINGLETHREADED
// 	Mutex_unlock(&__memfile_mtx);

// 	_File_termMT();
// 	#endif
// }



typedef struct DIRECTORY DIRECTORY;


static FILE* (*__file_open)(const char *, const char *) = NULL;

static DIRECTORY* (*__file_opendir)(const char *) = NULL;

void File_override(FILE* (*open)(const char *, const char *), DIRECTORY* (*opendir)(const char *)) {
	__file_open = open;

	__file_opendir = opendir;
}


// static int __memfile_idx = 0;

// FILE* File_openmem(const void *buf, int size) {
// 	FILE *f = tmpfile();

// 	if(f == NULL) {
// 		char tmp[64];

// 		_File_lockmem();

// 		snprintf(tmp, 64, ".memfile%d", __memfile_idx++);

// 		_File_unlockmem();

// 		f = fopen(tmp, "wb+");

// 		if(f == NULL) {
// 			return NULL;
// 		}
// 	}

// 	if(buf != NULL) {
// 		fwrite(buf, 1, size, f);

// 		rewind(f);
// 	}

// 	return f;
// }

// int File_setmem(FILE *f, const void *buf, int size) {
// 	long int p = ftell(f);

// 	rewind(f);

// 	int n = fwrite(buf, 1, size, f);

// 	fseek(f, p, SEEK_SET);

// 	return n;
// }

// int File_getmem(FILE *f, void *buf, int size) {
// 	long int p = ftell(f);

// 	rewind(f);

// 	int r = fread(buf, 1, size, f);

// 	fseek(f, p, SEEK_SET);

// 	return r;
// }


int File_isAbsolutePath(const char *path) {
	return *path == '~';
}

const char* File_getName(const char *path) {
	char *file = strrchr(path, '/');

	if(file == NULL) {
		return path;
	}

	return file + 1;
}


#define FILE_STACK_SIZE 48

#define FILE_PATH_SIZE 1024

static int __file_idx = 0;

static char __file_path[FILE_STACK_SIZE * FILE_PATH_SIZE] = {"./"};

static inline char* _File_path(int idx) {
	return __file_path + idx * FILE_PATH_SIZE;
}


static char* _File_cleanPath(char *path) {
	char *t = path;

	int l = 1;

	while(*t != '\0') {
		if(*t == '\\') {
			*t = '/';
		}

		++t;
		++l;
	}

	char b[l], *s = NULL;

	t = strcpy(b, path);

	while(*t != '\0') {
		if(*t == '/') {
			if(strncmp(t, "/./", 3) == 0) {
				memcpy(t, "/\\\\", 3);

				t += 2;
			}
		}

		++t;
	}

	do {
		l = 0;
		s = NULL;
		t = b;

		while(*t != '\0') {
			if(*t == '/') {
				if(s != NULL && strncmp(t, "/../", 4) == 0) {
					if((int) (t - s) != 3 || strncmp(s, "/..", 3) != 0) {
						t += 3;

						while(s < t) {
							*++s = '\\';
						}

						l = 1;
						s = NULL;
					}

				} else {
					s = t;
				}
			}

			++t;
		}

	} while(l);

	t = b;
	s = path;

	while(*t != '\0') {
		if(*t != '\\') {
			*s++ = *t++;

		} else {
			++t;
		}
	}

	*s = '\0';

	return path;
}


static inline int _File_setRoot(char *path, const char *root) {
	char *end = strrchr(root, '/');

	if(end == NULL) {
		strcpy(path, "./");

		return 1;
	}

	int l = 1 + (int) (end - root);

	if(l >= FILE_PATH_SIZE) {
		return 0;

	} else {
		strncpy(path, root, l);

		path[l] = '\0';
	}

	_File_cleanPath(path);

	return 1;
}

int File_setRoot(const char *root) {
	int r = 1;

	_File_lock();

	if(__file_idx != 0) {
		r = 0;

	} else {
		r = _File_setRoot(_File_path(0), root);
	}

	_File_unlock();

	return r;
}

static inline int __File_pushPath(const char *path) {
	if(__file_idx >= FILE_STACK_SIZE - 1) {
		return 0;
	}

	if(File_isAbsolutePath(path)) {
		if(__file_idx >= FILE_STACK_SIZE - 1) {
			return 0;
		}

		if(_File_setRoot(_File_path(++__file_idx), path + 1)) {
			return 1;
		}

		--__file_idx;

		return 0;
	}

	char *end = strrchr(path, '/');

	if(end == NULL) {
		char *src = _File_path(__file_idx);

		memcpy(_File_path(++__file_idx), src, 1 + strlen(src));

		return 1;
	}
	
	int l = 1 + (int) (end - path);

	int r = strlen(_File_path(__file_idx));

	if(r + l >= FILE_PATH_SIZE) {
		return 0;
	}

	char *s = _File_path(__file_idx);
	char *p = _File_path(++__file_idx);

	memcpy(p, s, r);

	memcpy(p + r, path, l);

	p[r + l] = '\0';

	_File_cleanPath(p);

	return 1;
}

int File_pushPath(const char *path) {
	int ret = 0;

	_File_lock();

	if(__File_pushPath(path) != 0) {
		ret = 1;
	}

	_File_unlock();

	return ret;
}

int File_popPath() {
	if(--__file_idx < 0) {
		__file_idx = 0;

		return 0;
	}

	_File_unlock();

	return 1;
}


static inline char* __File_getPath(char *buff, int size, const char *path) {
	if(path == NULL) {
		if(strlen(_File_path(__file_idx)) >= size) {
			return NULL;
		}

		return strcpy(buff, _File_path(__file_idx));
	}

	if(File_isAbsolutePath(path)) {
		if(strlen(path + 1) >= size) {
			return NULL;
		}

		return strcpy(buff, path + 1);
	}

	int l = strlen(_File_path(__file_idx));

	if(l + strlen(path) >= size) {
		return NULL;
	}

	strcpy(buff, _File_path(__file_idx));

	strcpy(buff + l, path);

	return _File_cleanPath(buff);
}

char* File_getPath(char *buff, int size, const char *path) {
	_File_lock();

	char *r = __File_getPath(buff, size, path);

	_File_unlock();

	return r;
}


FILE* File_open(const char *path, const char *mode) {
	char buf[FILE_PATH_SIZE];

	path = File_getPath(buf, FILE_PATH_SIZE, path);

	if(path == NULL) {
		return NULL;
	}

	if(__file_open == NULL) {
		return fopen(path, mode);
	}

	return __file_open(path, mode);
}

int File_close(FILE *f) {
	return fclose(f);
}


int File_mkdir(const char *path) {
	char buf[FILE_PATH_SIZE];

	path = File_getPath(buf, FILE_PATH_SIZE, path);

	if(path == NULL) {
		return 0;
	}

	return
	 #if defined _WIN32
	 mkdir(path)
	 #else
	 mkdir(path, 0777)
	 #endif
	  == 0;
}


FILE* File_pushPathAndOpen(const char *path, const char *mode) {
	if(!File_pushPath(path)) {
		return NULL;
	}

	FILE *f = File_open(File_getName(path), mode);

	if(f == NULL) {
		File_popPath();

		return NULL;
	}

	return f;
}


int File_isDir(const char *path) {
	struct stat info;

	if(stat(path, &info)) {
		return 0;
	}

	return S_ISDIR(info.st_mode);
}

typedef struct DIRECTORY {
	unsigned int n;

	const char *e[];

} DIRECTORY;

DIRECTORY* DIRECTORY_create(unsigned int n, const char *e[]) {
	DIRECTORY *dir = malloc(n * sizeof(const char *) + sizeof(DIRECTORY));

	dir->n = n;

	int i;

	for(i=0; i<n; i++) {
		dir->e[i] = strcpy(malloc(1 + strlen(e[i])), e[i]);		
	}

	return dir;
}

const char* DIRECTORY_entry(DIRECTORY *dir, unsigned int idx) {
	if(idx >= dir->n) return NULL;

	return dir->e[idx];
}


#define DIRECTORY_MAX_ENTRIES 1024

DIRECTORY* File_opendir(const char *path) {
	char buf[FILE_PATH_SIZE];

	path = File_getPath(buf, FILE_PATH_SIZE, path);

	if(path == NULL) return NULL;

	if(__file_opendir != NULL) return __file_opendir(path);

	DIR *d = opendir(path);

	if(d == NULL) return NULL;

	int n = 0;

	const char *e[DIRECTORY_MAX_ENTRIES];

	struct dirent *d_e;

	while(1) {
		d_e = readdir(d);

		if(d_e == NULL) break;

		const char *e_n = d_e->d_name;
		
		if(strcmp(e_n, ".") == 0 || strcmp(e_n, "..") == 0) {
			continue;
		}

		e[n++] = strcpy(malloc(1 + strlen(e_n)), e_n);

		if(n >= DIRECTORY_MAX_ENTRIES) break;
	}

	DIRECTORY *dir = DIRECTORY_create(n, e);

	while(--n >= 0) free((void*) e[n]);

	closedir(d);

	return dir;
}

void File_closedir(DIRECTORY *dir) {
	int n = dir->n;

	while(--n >= 0) free((void*) dir->e[n]);

	free((void*) dir);
}



#include "../File.h"

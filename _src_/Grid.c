
#include <stdlib.h>

#include <string.h>


#include "../Memory.h"


#include "../Grid.h"


#include "../ptrar.c"


#define GRID_MAXDIM 4

static inline int _Grid_count(uint d, uint *s) {
	if(d < 1 || d > GRID_MAXDIM) return - 1;

	int i, n = 1;

	for(i=0; i<d; i++) {
		if(s[i] < 1) return - 1;

		n *= s[i];
	}

	return n;
}


typedef struct Grid {
	uint w;
	uint d;

	uint s[GRID_MAXDIM];

	void *v[];

} Grid;

Grid* Grid_create(uint w, uint d, uint *s) {
	int n = _Grid_count(d, s);

	if(n < 1) return NULL;

	if(w < 1) w = sizeof(void*);

	Grid *grd = (Grid*) Memory_alloc0(n * w + sizeof(Grid));

	grd->w = w;
	grd->d = d;

	memcpy(grd->s, s, d * sizeof(uint));

	return grd;
}

void Grid_destroy(void *ptr) {
	Memory_free(ptr);
}

void Grid_size(Grid *grd, uint *s) {
	if(s != NULL) memcpy(s, grd->s, grd->d * sizeof(uint));
}

inline void* Grid_addr(Grid *grd, uint *i) {
	int j, idx = 0;

	for(j=0; j<grd->d; j++) {
		if(*i >= grd->s[j]) return NULL;

		idx = idx * grd->s[j] + *i++;
	}

	return ptrsum(grd->v, grd->w * idx);
}


typedef struct xGrid {
	uint w;
	uint d;

	uint s[GRID_MAXDIM];
	int o[GRID_MAXDIM];

	void *v;

} xGrid;

xGrid* xGrid_create(uint w, uint d, uint *s) {
	int n = _Grid_count(d, s);

	if(n < 1) return NULL;

	if(w < 1) w = sizeof(void*);

	xGrid *grd = (xGrid*) Memory_alloc(sizeof(xGrid));

	grd->w = w;
	grd->d = d;

	memcpy(grd->s, s, d * sizeof(uint));
	memset(grd->o, 0, d * sizeof(int));

	grd->v = Memory_alloc0(n * w);

	return grd;
}

void xGrid_destroy(void *ptr) {
	xGrid *grd = (xGrid*) ptr;

	Memory_free(grd->v);

	Memory_free(grd);
}

void xGrid_clear(xGrid *grd, void (*term)(void *)) {
	int n = _Grid_count(grd->d, grd->s);

	void *a = grd->v;

	while(--n >= 0) {
		term(a);

		a = ptrsum(a, grd->w);
	}
}

void xGrid_setOffset(xGrid *grd, int *o) {
	if(o != NULL) {
		int i;

		for(i=0; i<grd->d; i++) grd->o[i] = o[i];
	}
}

void xGrid_size(xGrid *grd, int *s, int *o) {
	int i;

	if(s != NULL) {
		for(i=0; i<grd->d; i++) s[i] = grd->s[i];
	}
	
	if(o != NULL) {
		for(i=0; i<grd->d; i++) o[i] = grd->o[i];
	}
}

static inline int _xGrid_idx(int d, int *o, uint *s, int *i) {
	uint idx = 0;

	while(--d >= 0) {
		uint io = *i++ + *o++;

		if(io >= *s) return - 1;

		idx = idx * *s++ + io;
	}

	return idx;
}

int xGrid_realloc(xGrid *grd, int *idx) {
	uint s[GRID_MAXDIM];

	int c[GRID_MAXDIM] = {0};

	int i, o[GRID_MAXDIM] = {0};

	for(i=0; i<grd->d; i++) {
		s[i] = grd->s[i];

		int io = *idx++ + grd->o[i];

		if(io < 0) {
			o[i] = - io;
			s[i] += (uint) o[i];

		} else if(io >= grd->s[i]) {
			s[i] = io + 1;
		}
	}

	int n = _Grid_count(grd->d, s);

	void *v = Memory_alloc0(n * grd->w);

	if(v == NULL) return 0;

	void *c_addr = grd->v;

	while(c[0] < grd->s[0]) {
		void *d_addr = ptrsum(v, grd->w * _xGrid_idx(grd->d, o, s, c));

		memcpy(d_addr, c_addr, grd->w);

		int j = grd->d - 1;

		c[j]++;

		while(c[j] >= grd->s[j] && j > 0) {
			c[j] = 0;
			c[--j]++;
		}

		c_addr = ptrsum(c_addr, grd->w);
	}

	Memory_free(grd->v);

	grd->v = v;

	for(i=0; i<grd->d; i++) {
		grd->o[i] += o[i];
		grd->s[i] = s[i];
	}

	return 1;
}

inline void* xGrid_addr(xGrid *grd, int *i) {
	int idx = _xGrid_idx(grd->d, grd->o, grd->s, i);

	if(idx < 0) return NULL;

	return ptrsum(grd->v, grd->w * idx);
}

int xGrid_set(xGrid *grd, int *i, void *v) {
	void **a = xGrid_addr(grd, i);

	if(a == NULL) {
		if(!xGrid_realloc(grd, i)) return 0;

		a = xGrid_addr(grd, i);
	}

	return Grid_setToAddr(a, v);
}

void* xGrid_get(xGrid *grd, int *i) {
	return Grid_getFromAddr((void**) xGrid_addr(grd, i));
}



typedef struct GridBlock {
	uint w;
	uint d;
	uint s[GRID_MAXDIM];

	xGrid *blk;

} GridBlock;

GridBlock* GridBlock_create(uint w, uint d, uint *s) {
	Grid *grd = Grid_create(w, d, s);

	if(grd == NULL) return NULL;

	GridBlock *blk = (GridBlock*) Memory_alloc(sizeof(GridBlock));

	blk->w = w;
	blk->d = d;

	int i, blk_i[GRID_MAXDIM] = {0};

	uint blk_s[GRID_MAXDIM];

	for(i=0; i<d; i++) {
		blk->s[i] = s[i];
		blk_s[i] = 1;
	}

	blk->blk = xGrid_create(w, d, blk_s);

	if(blk->blk != NULL) {
		if(xGrid_set(blk->blk, blk_i, grd)) return blk;

		xGrid_destroy(blk->blk);
	}

	Grid_destroy(grd);

	Memory_free(blk);

	return NULL;
}

static void _GridBlock_destroyGrid(void *ptr) {
	void **addr = (void**) ptr;

	if(*addr != NULL) {
		Grid_destroy(*addr);
	}

	*addr = NULL;
}

void GridBlock_destroy(void *ptr) {
	GridBlock *blk = (GridBlock*) ptr;

	xGrid_clear(blk->blk, _GridBlock_destroyGrid);

	xGrid_destroy(blk->blk);

	Memory_free(blk);
}

int GridBlock_newGrid(GridBlock *blk, int *i) {
	Grid *grd = Grid_create(blk->w, blk->d, blk->s);

	if(grd == NULL) return 0;

	void **addr = xGrid_addr(blk->blk, i);

	if(addr != NULL) {
		if(*addr != NULL) goto cleanup;

		*addr = grd;

		return 1;
	}

	if(xGrid_set(blk->blk, i, grd)) return 1;

cleanup:
	Grid_destroy(grd);

	return 0;
}

static inline void GridBlock_split(int d, int *i, uint *s, int *g, uint *u) {
	while(--d >= 0) {
		int g_i = *i / (int) *s;
		int u_i = *i % (int) *s;

		if(u_i < 0) {
			--g_i;
			u_i += *s;
		}

		++i;
		++s;

		*g++ = g_i;
		*u++ = (uint) u_i;
	}
}

void* GridBlock_addr(GridBlock *blk, int *i) {
	int g[GRID_MAXDIM]; uint u[GRID_MAXDIM];

	GridBlock_split(blk->d, i, blk->s, g, u);

	Grid *grd = xGrid_get(blk->blk, g);

	if(grd == NULL) return NULL;

	return Grid_addr(grd, u);
}

int GridBlock_set(GridBlock *blk, int *i, void *v) {
	return Grid_setToAddr((void**) GridBlock_addr(blk, i), v);
}

void* GridBlock_get(GridBlock *blk, int *i) {
	return Grid_getFromAddr((void**) GridBlock_addr(blk, i));
}


int GridBlock_fset(GridBlock *blk, int *i, void *v) {
	int g[GRID_MAXDIM]; uint u[GRID_MAXDIM];

	GridBlock_split(blk->d, i, blk->s, g, u);

	Grid *grd = xGrid_get(blk->blk, g);

	if(grd == NULL) {
		if(!GridBlock_newGrid(blk, g)) return 0;

		grd = xGrid_get(blk->blk, g);

		if(grd == NULL) return 0;
	}

	void *addr = GridBlock_addr(blk, i);

	return Grid_setToAddr(addr, v);
}

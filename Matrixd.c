
// COPY FROM Matrix.c
// RENAME Matrix TO dMatrix
// RENAME MATRIX TO MATRIXD
// CHANGE float TO double
// CHANGE .0f TO .0
// CHANGE MATH FUNCTIONS (sqrt, fabs)



#include <stdio.h>

#include <math.h>


#ifndef MATRIXD_C
#define MATRIXD_C


#include "Memory.h"


static inline double* dMatrix_create(int si, int sj);

static inline double* dMatrix_create2(int si, int sj, double v);

static inline double* dMatrix_createI(int si, int sj);

static inline void dMatrix_destroy(void *ptr);

static inline void dMatrix_print(double *m, int si, int sj);

static inline int dMatrix_save(double *m, int si, int sj, FILE *f);

static inline double* dMatrix_load(FILE *f, int *si, int *sj);

static inline int dMatrix_saveb(double *m, int si, int sj, FILE *f);

static inline double* dMatrix_loadb(FILE *f, int *si, int *sj);

static inline double dMatrix_setValue(double *m, int si, int sj, int i, int j, double v);

static inline double dMatrix_getValue(double *m, int si, int sj, int i, int j);

static inline double dMatrix_length(double *m, int si, int sj);

static inline int dMatrix_set(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj);

static inline int dMatrix_setI(double *m, int m_si, int m_sj);

static inline int dMatrix_set0(double *m, int m_si, int m_sj);

static inline int dMatrix_randomize(double *m, int m_si, int m_sj);

static inline int dMatrix_getSubdMatrix(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, int ri, int rj);

static inline int dMatrix_transpose(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj);

static inline int dMatrix_sum(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, double *m2, int m2_si, int m2_sj);

static inline int dMatrix_sub(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, double *m2, int m2_si, int m2_sj);

static inline int dMatrix_mult(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, double k);

static inline int dMatrix_prod(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, double *m2, int m2_si, int m2_sj);

static inline void dMatrix_swapLine(double *m, int m_si, int m_sj, int i0, int i1);

static inline void dMatrix_multLine(double *m, int m_si, int m_sj, int i, double k);

static inline void dMatrix_sumLine(double *m, int m_si, int m_sj, int i0, int i1);

static inline void dMatrix_sumMultLine(double *m, int m_si, int m_sj, int i0, int i1, double k);

static inline int dMatrix_scal(double *m, int m_si, int m_sj);

static inline int dMatrix_det(double *d, double *m, int m_si, int m_sj);

static inline int dMatrix_det2(double *d, double *m, int m_si, int m_sj);

static inline int dMatrix_inv(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj);

static inline int dMatrix_inv2(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj);

static inline int dMatrix_solve(double *x, int x_s, double *a, int a_si, int a_sj, double *b, int b_s);


static inline double* dMatrix_create(int si, int sj) {
	double *m = (double*) Memory_alloc(si * sj * sizeof(double));

	return m;
}

static inline double* dMatrix_create2(int si, int sj, double v) {
	double *m = dMatrix_create(si, sj);

	int i, n = si * sj;

	for(i=0; i<n; i++)
		m[i] = v;

	return m;
}

static inline double* dMatrix_createI(int si, int sj) {
	double *m = dMatrix_create(si, sj);

	int i, j;

	for(i=0; i<si; i++)
		for(j=0; j<sj; j++) {
			if(i == j) {
				m[i * sj + j] = 1.0;

			} else
				m[i * sj + j] = 0.0;
		}

	return m;
}

static inline void dMatrix_destroy(void *ptr) {
	Memory_free(ptr);
}

static inline void dMatrix_print(double *m, int si, int sj) {
	int i, j;

	for(i=0; i<si; i++) {
		for(j=0; j<sj; j++)
			printf("%f\t", m[i * sj + j]);

		printf("\n");
	}

	printf("\n");
}

static inline int dMatrix_save(double *m, int si, int sj, FILE *f) {
	int i, j;

	for(i=0; i<si; i++) {
		for(j=0; j<sj; j++)
			if(fprintf(f, "%.6f\t", m[i * sj + j]) < 0) return 0;

		if(fprintf(f, "\n") < 0) return 0;
	}

	return 1;
}

static inline int __dMatrix_isSpace(char c) {
	return c == ' ' || c == '\t';
}

static inline int __dMatrix_isNewLine(char c) {
	return c == '\n';
}

static inline int __dMatrix_isEnd(char c) {
	return c == '\0';
}

static inline int __dMatrix_countLineElements(char *c, char **e) {
	int s = 0;

	while(1) {
		while(__dMatrix_isSpace(*c)) ++c;

		if(__dMatrix_isEnd(*c) || __dMatrix_isNewLine(*c)) break;

		++s;

		while(!__dMatrix_isSpace(*c)) {
			if(__dMatrix_isEnd(*c) || __dMatrix_isNewLine(*c)) break;

			++c;
		}
	}

	*e = c;

	return s;
}

static inline double* dMatrix_load(FILE *f, int *si, int *sj) {
	#define __MATRIXD_FILEMAXSIZE (24 * 1024 * 1024) // 24 MiB

	char *buf = (char*) Memory_alloc(__MATRIXD_FILEMAXSIZE);

	if(buf == NULL) return NULL;

	int i, n = fread(buf, 1, __MATRIXD_FILEMAXSIZE - 1, f);

	if(!feof(f)) {
		Memory_free(buf);

		return NULL; // FILE IS TOO BIG
	}

	buf[n] = '\0';

	char *tmp = buf;

	*si = 0;
	*sj = - 1;

	int err = 0;

	while(__dMatrix_isSpace(*tmp) || __dMatrix_isNewLine(*tmp)) ++tmp;

	char *fst_ln = tmp;

	int sj_tmp = 0;

	while(1) {
		char *ln = tmp;

		sj_tmp = __dMatrix_countLineElements(ln, &tmp);

		if(tmp == NULL) {
			err = 1;

			break;
		}

		if(sj_tmp == 0) break;

		++(*si);

		if(*sj == - 1) {
			*sj = sj_tmp;

		} else if(*sj != sj_tmp) {
			err = 1;

			break;
		}

		++tmp;
	}

	if(err) {
		Memory_free(buf);

		return NULL;
	}

	double *m = dMatrix_create(*si, *sj);

	n = *si * *sj;

	tmp = fst_ln;

	for(i=0; i<n; i++) {
		while(__dMatrix_isSpace(*tmp) || __dMatrix_isNewLine(*tmp)) ++tmp;

		if(__dMatrix_isEnd(*tmp)) {
			m[i] = 0.0;

		} else {
			char *sym = tmp;

			while(!__dMatrix_isSpace(*tmp) && !__dMatrix_isNewLine(*tmp)) {
				if(__dMatrix_isEnd(*tmp)) break;

				++tmp;
			}

			if(!__dMatrix_isEnd(*tmp)) {
				*tmp++ = '\0';
			}

			m[i] = strtod(sym, NULL);
		}
	}

	Memory_free(buf);

	return m;
}

static inline int dMatrix_saveb(double *m, int si, int sj, FILE *f) {
	if(fwrite(&si, sizeof(int), 1, f) != 1) return 0;
	if(fwrite(&sj, sizeof(int), 1, f) != 1) return 0;

	int n = si * sj;

	return fwrite(m, sizeof(double), n, f) == n;
}

static inline double* dMatrix_loadb(FILE *f, int *si, int *sj) {
	if(fread(si, sizeof(int), 1, f) != 1) return NULL;
	if(fread(sj, sizeof(int), 1, f) != 1) return NULL;

	double *m = dMatrix_create(*si, *sj);

	if(m == NULL) return NULL;

	int n = *si * *sj;

	if(fread(m, sizeof(double), n, f) == n) return m;

	dMatrix_destroy(m);

	return NULL;
}

static inline double dMatrix_setValue(double *m, int si, int sj, int i, int j, double v) {
	m[i * sj + j] = v;

	return v;
}

static inline double dMatrix_getValue(double *m, int si, int sj, int i, int j) {
	return m[i * sj + j];
}

static inline double dMatrix_length(double *m, int si, int sj) {
	double s2 = 0.0;

	int n = si * sj;

	while(--n >= 0) {
		double v = *m++;

		s2 += v * v;
	}

	return sqrt(s2);
}

static inline int dMatrix_set(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj) {
	if(m0_si != m1_si) return 0;

	if(m0_sj != m1_sj) return 0;

	int i, j;

	for(i=0; i<m1_si; i++)
		for(j=0; j<m1_sj; j++)
			m0[i * m0_sj + j] = m1[i * m1_sj + j];

	return 1;
}

static inline int dMatrix_setI(double *m, int m_si, int m_sj) {
	int i, j;

	for(i=0; i<m_si; i++)
		for(j=0; j<m_sj; j++) {
			if(i == j) {
				m[i * m_sj + j] = 1.0;

			} else
				m[i * m_sj + j] = 0.0;
		}

	return 1;
}

static inline int dMatrix_set0(double *m, int m_si, int m_sj) {
	memset(m, 0, m_si * m_sj * sizeof(double));

	return 1;
}

static inline int dMatrix_randomize(double *m, int m_si, int m_sj) {
	int i, n = m_si * m_sj;

	for(i=0; i<n; i++)
		m[i] = ((double) rand() / RAND_MAX);

	return 1;
}

static inline int dMatrix_getSubdMatrix(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, int ri, int rj) {
	if(m0_si != m1_si - 1) return 0;

	if(m0_sj != m1_sj - 1) return 0;

	int i, j;

	for(i=0; i<ri; i++) {
		for(j=0; j<rj; j++)
			m0[i * m0_si + j] = m1[i * m1_sj + j];

		for(j=rj+1; j<m1_sj; j++)
			m0[i * m0_si + (j-1)] = m1[i * m1_sj + j];
	}

	for(i=ri+1; i<m1_si; i++) {
		for(j=0; j<rj; j++)
			m0[(i-1) * m0_si + j] = m1[i * m1_sj + j];

		for(j=rj+1; j<m1_sj; j++)
			m0[(i-1) * m0_si + (j-1)] = m1[i * m1_sj + j];
	}

	return 1;
}

static inline int dMatrix_transpose(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj) {
	if(m0_si != m1_sj) return 0;

	if(m0_sj != m1_si) return 0;

	int i, j;

	for(i=0; i<m1_si; i++)
		for(j=0; j<m1_sj; j++)
			m0[j * m0_si + i] = m1[i * m1_si + j];

	return 1;
}

static inline int dMatrix_sum(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, double *m2, int m2_si, int m2_sj) {
	if(m1_si != m2_si) return 0;

	if(m1_sj != m2_sj) return 0;

	if(m0_si != m1_si) return 0;

	if(m0_sj != m1_sj) return 0;

	int i, n = m1_si * m1_sj;

	for(i=0; i<n; i++)
		m0[i] = m1[i] + m2[i];

	return 1;
}

static inline int dMatrix_sub(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, double *m2, int m2_si, int m2_sj) {
	if(m1_si != m2_si) return 0;

	if(m1_sj != m2_sj) return 0;

	if(m0_si != m1_si) return 0;

	if(m0_sj != m1_sj) return 0;

	int i, n = m1_si * m1_sj;

	for(i=0; i<n; i++)
		m0[i] = m1[i] - m2[i];

	return 1;
}

static inline int dMatrix_mult(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, double k) {
	if(m0_si != m1_si) return 0;

	if(m0_sj != m1_sj) return 0;

	int i, n = m0_si * m0_sj;

	for(i=0; i<n; i++)
		m0[i] = m1[i] * k;

	return 1;
}

static inline int dMatrix_prod(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj, double *m2, int m2_si, int m2_sj) {
	if(m1_sj != m2_si) return 0;

	if(m0_si != m1_si) return 0;

	if(m0_sj != m2_sj) return 0;

	int i, j, k;

	for(i=0; i<m1_si; i++)
		for(j=0; j<m2_sj; j++) {
			m0[i * m0_sj + j] = 0.0;

			for(k=0; k<m1_sj; k++)
				m0[i * m0_sj + j] += m1[i * m1_sj + k] * m2[k * m2_sj + j];
		}

	return 1;
}

static inline void dMatrix_swapLine(double *m, int m_si, int m_sj, int i0, int i1) {
	double tmp;

	int j;

	for(j=0; j<m_sj; j++) {
		tmp = m[i0 * m_sj + j];

		m[i0 * m_sj + j] = m[i1 * m_sj + j];

		m[i1 * m_sj + j] = tmp;
	}
}

static inline void dMatrix_multLine(double *m, int m_si, int m_sj, int i, double k) {
	int j;

	for(j=0; j<m_sj; j++)
		m[i * m_sj + j] *= k;
}

static inline void dMatrix_sumLine(double *m, int m_si, int m_sj, int i0, int i1) {
	int j;

	for(j=0; j<m_sj; j++)
		m[i0 * m_sj + j] += m[i1 * m_sj + j];
}

static inline void dMatrix_sumMultLine(double *m, int m_si, int m_sj, int i0, int i1, double k) {
	int j;

	for(j=0; j<m_sj; j++)
		m[i0 * m_sj + j] += m[i1 * m_sj + j] * k;
}

static inline int dMatrix_scal(double *m, int m_si, int m_sj) {
	int i, j;

	for(j=0; j<m_sj; j++) {
		if(m[j * m_sj + j] == 0.0)
			for(i=j+1; i<m_si; i++)
				if(m[i * m_sj + j] != 0.0) {
					dMatrix_swapLine(m, m_si, m_sj, j, i);

					break;
				}

		if(m[j *m_sj + j] != 0.0)
			for(i=j+1; i<m_si; i++)
				if(m[i * m_sj + j] != 0.0)
					dMatrix_sumMultLine(m, m_si, m_sj, i, j, - m[i * m_sj + j] / m[j * m_sj + j]);
	}

	return 1;
}

static inline int dMatrix_det(double *d, double *m, int m_si, int m_sj) {
	if(m_si != m_sj) {
		return 0;
	}

	double tmp[m_si * m_sj];

	int i, j;

	for(i=0; i<m_si; i++)
		for(j=0; j<m_sj; j++)
			tmp[i * m_sj + j] = m[i * m_sj + j];

	m = tmp;

	*d = 1.0;

	for(j=0; j<m_sj; j++) {
		if(m[j * m_sj + j] == 0.0) {
			for(i=j+1; i<m_si; i++)
				if(m[i * m_sj + j] != 0.0) {
					dMatrix_swapLine(m, m_si, m_sj, j, i);

					*d *= - 1.0;

					break;
				}

			if(m[j *m_sj + j] == 0.0) {
				*d = 0.0;

				return 1;
			}
		}

		for(i=j+1; i<m_si; i++)
			if(m[i * m_sj + j] != 0.0)
				dMatrix_sumMultLine(m, m_si, m_sj, i, j, - m[i * m_sj + j] / m[j * m_sj + j]);
	}

	for(i=0; i<m_si; i++)
		*d *= m[i * m_sj + i];

	return 1;
}

static inline int dMatrix_det2(double *d, double *m, int m_si, int m_sj) {
	if(m_si != m_sj) return 0;

	if(m_si == 0) {
		*d = 0.0;

	} else if(m_si == 1) {
		*d = m[0];

	} else if(m_si == 2) {
		*d = m[0] * m[3] - m[1] * m[2];

	} else if(m_si == 3) {
		*d = m[0] * m[4] * m[8] + m[1] * m[5] * m[6] + m[2] * m[3] * m[7] - m[2] * m[4] * m[6] - m[1] * m[3] * m[8] - m[0] * m[5] * m[7];

	} else {
		int i;

		double td = 0.0;
		double sd = 0.0;

		double tmp[(m_si-1)*(m_sj-1)];

		for(i=0; i<m_si; i++) {
			dMatrix_getSubdMatrix(tmp, m_si-1, m_sj-1, m, m_si, m_sj, 0, i);

			dMatrix_det2(&sd, tmp, m_si-1, m_sj-1);

			if(i % 2 == 0) {
				td += m[i] * sd;

			} else {
				td -= m[i] * sd;
			}
		}

		*d = td;
	}

	return 1;
}

static inline int dMatrix_inv(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj) {
	if(m0_si != m1_si) return 0;

	if(m1_si != m1_sj) return 0;

	if(m0_si != m0_sj) return 0;

	double k, tmp[m0_si * m0_sj];

	dMatrix_setI(m0, m0_si, m0_sj);

	dMatrix_set(tmp, m0_si, m0_sj, m1, m1_si, m1_sj);

	int i, j;

	for(j=0; j<m0_sj; j++) {
		double max_v = fabs(tmp[j * m0_sj + j]);

		int max_i = j;

		for(i=j+1; i<m0_si; i++) {
			double a = fabs(tmp[i * m0_sj + j]);

			if(a > max_v) {
				max_v = a;
				max_i = i;
			}
		}

		if(max_v == 0.0) return 0;

		if(max_i != j) {
			dMatrix_swapLine(tmp, m0_si, m0_sj, j, max_i);

			dMatrix_swapLine(m0, m0_si, m0_sj, j, max_i);
		}

		if(tmp[j * m0_sj + j] != 1.0) {
			k = 1.0 / tmp[j * m0_sj + j];
			
			dMatrix_multLine(tmp, m0_si, m0_sj, j, k);

			dMatrix_multLine(m0, m0_si, m0_sj, j, k);
		}

		for(i=j+1; i<m0_si; i++)
			if(tmp[i * m0_sj + j] != 0.0) {
				k = - tmp[i * m0_sj + j];

				dMatrix_sumMultLine(tmp, m0_si, m0_sj, i, j, k);

				dMatrix_sumMultLine(m0, m0_si, m0_sj, i, j, k);
			}

		for(i=0; i<j; i++)
			if(tmp[i * m0_sj + j] != 0.0) {
				k = - tmp[i * m0_sj + j];

				dMatrix_sumMultLine(tmp, m0_si, m0_sj, i, j, k);

				dMatrix_sumMultLine(m0, m0_si, m0_sj, i, j, k);
			}
	}

	return 1;
}

static inline int dMatrix_inv2(double *m0, int m0_si, int m0_sj, double *m1, int m1_si, int m1_sj) {
	if(m0_si != m1_si) return 0;

	if(m1_si != m1_sj) return 0;

	if(m0_si != m0_sj) return 0;

	double tmp[(m1_si-1) * (m1_sj-1)];

	double det, sd = 0.0;

	dMatrix_det(&det, m1, m1_si, m1_sj);

	int i, j;

	for(i=0; i<m1_si; i++) {
		for(j=0; j<m1_sj; j++) {
			dMatrix_getSubdMatrix(tmp, m1_si-1, m1_sj-1, m1, m1_si, m1_sj, i, j);

			dMatrix_det(&sd, tmp, m1_si-1, m1_sj-1);

			if((i + j) % 2 == 0) {
				m0[j * m0_sj + i] = sd / det;

			} else
				m0[j * m0_sj + i] = - sd / det;
		}
	}

	return 1;
}

static inline int dMatrix_solve(double *x, int x_s, double *a, int a_si, int a_sj, double *b, int b_s) {
	if(a_si != a_sj) return 0;

	if(a_si != b_s) return 0;

	if(a_sj != x_s) return 0;

	double tmp[a_si * a_sj];

	if(dMatrix_inv(tmp, a_si, a_sj, a, a_si, a_sj) == 0) return 0;

	return dMatrix_prod(x, x_s, 1, tmp, a_si, a_sj, b, b_s, 1);
}


#endif

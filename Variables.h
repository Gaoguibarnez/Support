
#ifndef VARIABLES_H
#define VARIABLES_H


#include "Queue.h"

#include "RedBlackTree.h"


#include "Keys.h"

#include "Mutex.h"


#include "Types.c"



typedef struct Variables {
	Mutex *mtx;

	Queue *key;

	RedBlackTree *var_val;
	RedBlackTree *var_type;

	RedBlackTree *str_val;

} Variables;


Variables* Variables_create();

Variables* Variables_createMT();

void Variables_destroy(void *ptr);


void Variables_setVar(Variables *var, void *key, Type_t type, void *val);

void* Variables_getVar(Variables *var, void *key, Type_t type, void *def_val);

void** Variables_getAddr(Variables *var, void *key, Type_t type, void *def_val);


void* Variables_getStringAddressFromKey(Variables *var, void *key, char *def_val);

void Variables_setStringFromKey(Variables *var, void *key, char *val);

char* Variables_getStringFromKey(Variables *var, void *key, char *def_val);

void* Variables_getStringAddress(Variables *var, char *nm, char *def_val);

void Variables_setString(Variables *var, char *nm, char *val);

char* Variables_getString(Variables *var, char *nm, char *def_val);


void Variables_setStringFromAddress(void *addr, char *str);

static inline char* Variables_getStringFromAddress(void *addr) {
	return *(char**) addr;
}

static inline char* Variables_getStringFromAddress(void *addr);


int Variables_hasVar(Variables *var, void *key);

int Variables_hasStr(Variables *var, void *key);


static inline void** Variables_getPtrAddrFromKey(Variables *var, void *key, void *def_val);

static inline void Variables_setPtrFromKey(Variables *var, void *key, void *val);

static inline void* Variables_getPtrFromKey(Variables *var, void *key, void *def_val);

static inline void** Variables_getPtrAddr(Variables *var, char *nm, void *def_val);

static inline void Variables_setPtr(Variables *var, char *nm, void *val);

static inline void* Variables_getPtr(Variables *var, char *nm, void *def_val);


static inline double* Variables_getDblAddrFromKey(Variables *var, void *key, double def_val);

static inline void Variables_setDblFromKey(Variables *var, void *key, double val);

static inline double Variables_getDblFromKey(Variables *var, void *key, double def_val);

static inline double* Variables_getDblAddr(Variables *var, char *nm, double def_val);

static inline void Variables_setDbl(Variables *var, char *nm, double val);

static inline double Variables_getDbl(Variables *var, char *nm, double def_val);


static inline float* Variables_getFltAddrFromKey(Variables *var, void *key, float def_val);

static inline void Variables_setFltFromKey(Variables *var, void *key, float val);

static inline float Variables_getFltFromKey(Variables *var, void *key, float def_val);

static inline float* Variables_getFltAddr(Variables *var, char *nm, float def_val);

static inline void Variables_setFlt(Variables *var, char *nm, float val);

static inline float Variables_getFlt(Variables *var, char *nm, float def_val);


static inline int* Variables_getIntAddrFromKey(Variables *var, void *key, int def_val);

static inline void Variables_setIntFromKey(Variables *var, void *key, int val);

static inline int Variables_getIntFromKey(Variables *var, void *key, int def_val);

static inline int* Variables_getIntAddr(Variables *var, char *nm, int def_val);

static inline void Variables_setInt(Variables *var, char *nm, int val);

static inline int Variables_getInt(Variables *var, char *nm, int def_val);


static inline char* Variables_getCharAddrFromKey(Variables *var, void *key, char def_val);

static inline void Variables_setCharFromKey(Variables *var, void *key, char val);

static inline char Variables_getCharFromKey(Variables *var, void *key, char def_val);

static inline char* Variables_getCharAddr(Variables *var, char *nm, char def_val);

static inline void Variables_setChar(Variables *var, char *nm, char val);

static inline char Variables_getChar(Variables *var, char *nm, char def_val);


#define VARIABLES_DEFINETYPEFUNCTIONS(Owner, TypeName, TypeDecl, TypeID, GetVariables)	\
	\
static inline TypeDecl* Owner ## _get ## TypeName ## AddrFromKey(Owner *own, void *k, TypeDecl v) {	\
	void *v_ptr;	\
	memcpy(&v_ptr, &v, sizeof(TypeDecl));	\
	return (TypeDecl*) Variables_getAddr(GetVariables, k, TypeID, v_ptr);	\
}	\
	\
static inline void Owner ## _set ## TypeName ## FromKey(Owner *own, void *k, TypeDecl v) {	\
	void *v_ptr;	\
	memcpy(&v_ptr, &v, sizeof(TypeDecl));	\
	Variables_setVar(GetVariables, k, TypeID, v_ptr);	\
}	\
	\
static inline TypeDecl Owner ## _get ## TypeName ## FromKey(Owner *own, void *k, TypeDecl v) {	\
	void *v_ptr;	\
	memcpy(&v_ptr, &v, sizeof(TypeDecl));	\
	return ptrTo ## TypeName (Variables_getVar(GetVariables, k, TypeID, v_ptr));	\
}	\
	\
static inline TypeDecl* Owner ## _get ## TypeName ## Addr(Owner *own, char *n, TypeDecl v) {	\
	return Owner ## _get ## TypeName ## AddrFromKey(GetVariables, Keys_get(n), v);	\
}	\
	\
static inline void Owner ## _set ## TypeName(Owner *own, char *n, TypeDecl v) {	\
	Owner ## _set ## TypeName ## FromKey (GetVariables, Keys_get(n), v);	\
}	\
	\
static inline TypeDecl Owner ## _get ## TypeName(Owner *own, char *n, TypeDecl v) {	\
	return Owner ## _get ## TypeName ## FromKey(GetVariables, Keys_get(n), v);	\
}	\


#define VARIABLES_DEFINEFUNCTIONS(Owner, GetVariables)	\
	\
VARIABLES_DEFINETYPEFUNCTIONS(Owner, Ptr, void*, TYPE_ID_PTR, GetVariables)	\
	\
VARIABLES_DEFINETYPEFUNCTIONS(Owner, Dbl, double, TYPE_ID_DBL, GetVariables)	\
	\
VARIABLES_DEFINETYPEFUNCTIONS(Owner, Flt, float, TYPE_ID_FLT, GetVariables)	\
	\
VARIABLES_DEFINETYPEFUNCTIONS(Owner, Int, int, TYPE_ID_INT, GetVariables)	\
	\
VARIABLES_DEFINETYPEFUNCTIONS(Owner, Char, char, TYPE_ID_CHR, GetVariables)	\
	\

VARIABLES_DEFINEFUNCTIONS(Variables, own)


void Variables_setTypeFromKey(Variables *var, void *key, Type_t type);

void Variables_setType(Variables *var, char *nm, Type_t type);

Type_t Variables_getTypeFromKey(Variables *var, void *key);

Type_t Variables_getType(Variables *var, char *nm);


int Variables_save(Variables *var, FILE *file);

int Variables_load(Variables *var, FILE *file);


#endif

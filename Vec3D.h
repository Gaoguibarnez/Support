
#ifndef VEC3D_H
#define VEC3D_H


typedef struct Vec3D {
	float x;
	float y;
	float z;
	
} Vec3D;

typedef struct Vec2D {
	float x;
	float y;
	
} Vec2D;

typedef struct Vec4D {
	float w;
	float x;
	float y;
	float z;
	
} Vec4D;


/*

static inline Vec3D* Vec3D_create();

static inline Vec3D* Vec3D_create0();

static inline Vec3D* Vec3D_create1(const Vec3D *v);

static inline Vec3D* Vec3D_create2(float hang, float vang);

static inline Vec3D* Vec3D_create3(float x, float y, float z);

static inline Vec3D* Vec3D_create4(float *b);

static inline void Vec3D_destroy(void *v);

static inline void Vec3D_print(const Vec3D *v);

static inline Vec3D* Vec3D_set(Vec3D *v1, const Vec3D *v2);

static inline Vec3D* Vec3D_set0(Vec3D *v);

static inline Vec3D* Vec3D_set2(Vec3D *v, float hang, float vang);

static inline Vec3D* Vec3D_set3(Vec3D *v, float x, float y, float z);

static inline Vec3D* Vec3D_set4(Vec3D *v, float *b);

static inline void Vec3D_get(const Vec3D *v, float *x, float *y, float *z);

static inline float Vec3D_getX(const Vec3D *v);

static inline float Vec3D_getY(const Vec3D *v);

static inline float Vec3D_getZ(const Vec3D *v);

static inline Vec3D* Vec3D_sum(Vec3D *v1, const Vec3D *v2);

static inline Vec3D* Vec3D_sum2(Vec3D *v1, const Vec3D *v2, const Vec3D *v3);

static inline Vec3D* Vec3D_sumMult(Vec3D *v1, const Vec3D *v2, float k);

static inline Vec3D* Vec3D_sumMult2(Vec3D *v1, const Vec3D *v2, const Vec3D *v3, float k);

static inline Vec3D* Vec3D_sub(Vec3D *v1, const Vec3D *v2);

static inline Vec3D* Vec3D_sub2(Vec3D *v1, const Vec3D *v2, const Vec3D *v3);

static inline Vec3D* Vec3D_mult(Vec3D *v, float f);

static inline Vec3D* Vec3D_mult2(Vec3D *v1, const Vec3D *v2, float f);

static inline float Vec3D_dotProduct(const Vec3D *v1, const Vec3D *v2);

static inline Vec3D* Vec3D_crossProduct(Vec3D *v1, const Vec3D *v2);

static inline Vec3D* Vec3D_crossProduct2(Vec3D *v1, const Vec3D *v2, const Vec3D *v3);

static inline float Vec3D_length(const Vec3D *v);

static inline float Vec3D_distance(const Vec3D *v1, const Vec3D *v2);

static inline float Vec3D_distance2(const Vec3D *v1, const Vec3D *v2);

static inline Vec3D* Vec3D_normalize(Vec3D *v);

static inline Vec3D* Vec3D_normalize2(Vec3D *v1, const Vec3D *v2);

static inline Vec3D* Vec3D_normalize3(Vec3D *v, float *l);

static inline Vec3D* Vec3D_normalize4(Vec3D *v1, const Vec3D *v2, float *l);

static inline Vec3D* Vec3D_transform(Vec3D *v, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_transform2(Vec3D *v1, const Vec3D *v2, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_transform3(Vec3D *v, const Vec3D *p, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_transform4(Vec3D *v1, const Vec3D *v2, const Vec3D *p, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_untransform(Vec3D *v, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_untransform2(Vec3D *v1, const Vec3D *v2, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_untransform3(Vec3D *v, const Vec3D *p, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_untransform4(Vec3D *v1, const Vec3D *v2, const Vec3D *p, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_fastUntransform(Vec3D *v, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_fastUntransform2(Vec3D *v1, const Vec3D *v2, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_fasterUntransform(Vec3D *v, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_fasterUntransform2(Vec3D *v1, const Vec3D *v2, const Vec3D *x, const Vec3D *y, const Vec3D *z);

static inline Vec3D* Vec3D_rotate(Vec3D *v, const Vec3D *n, float a);

static inline Vec3D* Vec3D_rotate2(Vec3D *v1, const Vec3D *v2, const Vec3D *n, float a);


static inline Vec2D* Vec2D_create();

static inline Vec2D* Vec2D_create0();

static inline Vec2D* Vec2D_create1(const Vec2D *v);

static inline Vec2D* Vec2D_create2(float x, float y);

static inline void Vec2D_destroy(void *v);

static inline void Vec2D_print(const Vec2D *v);

static inline Vec2D* Vec2D_set(Vec2D *v1, const Vec2D *v2);

static inline Vec2D* Vec2D_set0(Vec2D *v);

static inline Vec2D* Vec2D_set2(Vec2D *v, float x, float y);

static inline void Vec2D_get(const Vec2D *v, float *x, float *y);

static inline float Vec2D_getX(const Vec2D *v);

static inline float Vec2D_getY(const Vec2D *v);

static inline Vec2D* Vec2D_mult(Vec2D *v, float k);

static inline Vec2D* Vec2D_sumMult(Vec2D *v1, const Vec2D *v2, float k);

static inline Vec3D* Vec2D_toVec3D(Vec3D *u, const Vec2D *v);

static inline Vec2D* Vec3D_toVec2D(Vec2D *u, const Vec3D *v);

static inline Vec2D* Vec2D_rotate2(Vec2D *u, const Vec2D *v, float a);

static inline Vec2D* Vec2D_rotate(Vec2D *u, float a);

static inline Vec2D* Vec2D_rotate90(Vec2D *u, const Vec2D *v);

static inline float Vec2D_dotProduct(const Vec2D *u, const Vec2D *v);

static inline float Vec2D_crossProduct(const Vec2D *u, const Vec2D *v);

static inline float Vec2D_length2(const Vec2D *v);

static inline float Vec2D_length(const Vec2D *v);

static inline float Vec2D_direction(const Vec2D *v);

static inline Vec2D* Vec2D_sum(Vec2D *v, const Vec2D *u);

static inline Vec2D* Vec2D_sum2(Vec2D *v, const Vec2D *u, const Vec2D *w);


static inline Vec4D* Vec4D_inverse(Vec4D *v);

static inline Vec4D* Vec4D_inverse2(Vec4D *v1, const Vec4D *v2);

static inline Vec4D* Vec4D_prod(Vec4D *v1, const Vec4D *v2);

static inline Vec4D* Vec4D_prod2(Vec4D *v1, const Vec4D *v2, const Vec4D *v3);

static inline float* Vec4D_getRotationMatrix(const Vec4D *v, float *m);

*/


#endif

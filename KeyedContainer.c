
#include "Memory.h"


#include "Keys.h"

#include "Mutex.h"

#include "Flags.c"


#ifndef KEYEDCONTAINER_C
#define KEYEDCONTAINER_C



#define KEYEDCONTAINER_FLAG_ALLOWREPVAL 0x1

#define KEYEDCONTAINER_FLAG_IGNOREORDER 0x4


typedef struct KeyedContainer {
	Mutex *mtx;

	Flags flg;

	int num;
	int cnt;

	void **key;
	void **val;

} KeyedContainer;


static int KEYEDCONTAINER_INITSIZE = 4;

static inline void KeyedContainer_init(KeyedContainer *ctn) {
	ctn->mtx = NULL;

	ctn->flg = 0;

	ctn->num = KEYEDCONTAINER_INITSIZE;
	ctn->cnt = 0;

	ctn->key = (void**) Memory_alloc(2 * KEYEDCONTAINER_INITSIZE * sizeof(void*));
	ctn->val = ctn->key + KEYEDCONTAINER_INITSIZE;	
}

static inline void KeyedContainer_term(void *ptr) {
	KeyedContainer *ctn = (KeyedContainer*) ptr;

	Memory_free(ctn->key);
}


static inline KeyedContainer* KeyedContainer_create() {
	KeyedContainer *ctn = (KeyedContainer*) Memory_alloc(sizeof(KeyedContainer));

	KeyedContainer_init(ctn);

	return ctn;
}

static inline void KeyedContainer_destroy(void *ptr) {
	KeyedContainer_term(ptr);

	Memory_free(ptr);
}


static inline void KeyedContainer_setMT(KeyedContainer *ctn) {
	if(ctn->mtx == NULL)
		Mutex_init(ctn->mtx = (Mutex*) Memory_alloc(sizeof(Mutex)));
}

static inline void KeyedContainer_lock(KeyedContainer *ctn) {
	if(ctn->mtx != NULL)
		Mutex_lock(ctn->mtx);
}

static inline void KeyedContainer_unlock(KeyedContainer *ctn) {
	if(ctn->mtx != NULL)
		Mutex_unlock(ctn->mtx);
}


static inline int _KeyedContainer_getIdx(KeyedContainer *ctn, void *key) {
	int i, n = ctn->cnt;

	for(i=0; i<n; i++)
		if(ctn->key[i] == key)
			break;

	return (i < n) ? i : - 1;
}

static inline int KeyedContainer_insert(KeyedContainer *ctn, void *key, void *val) {
	KeyedContainer_lock(ctn);

	if(Flags_getf(&ctn->flg, KEYEDCONTAINER_FLAG_ALLOWREPVAL))
		if(_KeyedContainer_getIdx(ctn, key) >= 0) {
			KeyedContainer_unlock(ctn);

			return 0;
		}

	if(ctn->cnt >= ctn->num) {
		int i, num = ctn->num * 2;

		void **key = (void**) Memory_alloc(2 * num * sizeof(void*));;
		void **val = key + num;

		for(i=0; i<ctn->cnt; i++) {
			key[i] = ctn->key[i];
			val[i] = ctn->val[i];
		}

		Memory_free(ctn->key);

		ctn->num = num;

		ctn->key = key;
		ctn->val = val;
	}

	ctn->key[ctn->cnt] = key;
	ctn->val[ctn->cnt] = val;

	++ctn->cnt;

	KeyedContainer_unlock(ctn);

	return 1;
}

static inline int KeyedContainer_remove(KeyedContainer *ctn, void *key) {
	KeyedContainer_lock(ctn);

	int num = ctn->cnt;
	int idx = _KeyedContainer_getIdx(ctn, key);

	if(idx < 0) {
		KeyedContainer_unlock(ctn);

		return 0;
	}

	--ctn->cnt;

	if(Flags_getf(&ctn->flg, KEYEDCONTAINER_FLAG_IGNOREORDER)) {
		ctn->key[idx] = ctn->key[ctn->cnt];
		ctn->val[idx] = ctn->val[ctn->cnt];

	} else
		for(; idx<num; idx++) {
			ctn->key[idx] = ctn->key[idx+1];
			ctn->val[idx] = ctn->val[idx+1];
		}

	KeyedContainer_unlock(ctn);

	return 1;
}

static inline void* KeyedContainer_search(KeyedContainer *ctn, void *key) {
	KeyedContainer_lock(ctn);

	int idx = _KeyedContainer_getIdx(ctn, key);

	if(idx < 0) {
		KeyedContainer_unlock(ctn);

		return NULL;
	}

	void *ret = ctn->val[idx];

	KeyedContainer_unlock(ctn);

	return ret;
}


static inline void** KeyedContainer_getKeys(KeyedContainer *ctn, int *num) {
	if(num != NULL)
		*num = ctn->cnt;

	return ctn->key;
}

static inline void** KeyedContainer_getVals(KeyedContainer *ctn, int *num) {
	if(num != NULL)
		*num = ctn->cnt;

	return ctn->val;
}

static inline int KeyedContainer_get(KeyedContainer *ctn, void ***key, void ***val) {
	if(key != NULL)
		*key = ctn->key;

	if(val != NULL)
		*val = ctn->val;

	return ctn->cnt;
}


static inline int KeyedContainer_fill(KeyedContainer *ctn, void **k, void **v, int n, int o) {
	KeyedContainer_lock(ctn);

	if(n < 0)
		n = 0;

	if(o < 0)
		o = 0;

	n += o;

	if(n > ctn->cnt)
		n = ctn->cnt;

	int m = n - o;

	if(k != NULL && v != NULL) {
		for(; o<n; o++) {
			*k++ = ctn->key[o];
			*v++ = ctn->val[o];
		}

	} else if(k != NULL) {
		for(; o<n; o++)
			*k++ = ctn->key[o];

	} else if(v != NULL) {
		for(; o<n; o++)
			*v++ = ctn->val[o];
	}

	KeyedContainer_unlock(ctn);

	return m;
}



#endif


// EXTRA FUNCTIONS FOR Vec3Dd USING float AND Vec3D 


#ifndef VEC3DDF_C
#define VEC3DDF_C


#include "Vec3D.c"

#include "Vec3Dd.c"


static inline Vec3Dd* Vec3Dd_create2f(float hang, float vang);

static inline Vec3Dd* Vec3Dd_create3f(float x, float y, float z);

static inline Vec3Dd* Vec3Dd_set2f(Vec3Dd *v, float hang, float vang);

static inline Vec3Dd* Vec3Dd_set3f(Vec3Dd *v, float x, float y, float z);

static inline Vec3Dd* Vec3Dd_sumMultf(Vec3Dd *v1, const Vec3Dd *v2, float k);

static inline Vec3Dd* Vec3Dd_sumMult2f(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3, float k);

static inline Vec3Dd* Vec3Dd_multf(Vec3Dd *v, float f);

static inline Vec3Dd* Vec3Dd_mult2f(Vec3Dd *v1, const Vec3Dd *v2, float f);

static inline float Vec3Dd_dotProductf(const Vec3Dd *v1, const Vec3Dd *v2);

static inline float Vec3Dd_lengthf(Vec3Dd *v);

static inline Vec3Dd* Vec3Dd_create2f(float hang, float vang) {
	return Vec3Dd_create2((double) hang, (double) vang);
}

static inline Vec3Dd* Vec3Dd_create3f(float x, float y, float z) {
	return Vec3Dd_create3((double) x, (double) y, (double) z);
}

static inline Vec3Dd* Vec3Dd_set2f(Vec3Dd *v, float hang, float vang) {
	return Vec3Dd_set2(v, (double) hang, (double) vang);
}

static inline Vec3Dd* Vec3Dd_set3f(Vec3Dd *v, float x, float y, float z) {
	return Vec3Dd_set3(v, (double) x, (double) y, (double) z);
}

static inline Vec3Dd* Vec3Dd_sumMultf(Vec3Dd *v1, const Vec3Dd *v2, float k) {
	return Vec3Dd_sumMult(v1, v2, (double) k);
}

static inline Vec3Dd* Vec3Dd_sumMult2f(Vec3Dd *v1, const Vec3Dd *v2, const Vec3Dd *v3, float k) {
	return Vec3Dd_sumMult2(v1, v2, v3, (double) k);
}

static inline Vec3Dd* Vec3Dd_multf(Vec3Dd *v, float f) {
	return Vec3Dd_mult(v, (double) f);
}

static inline Vec3Dd* Vec3Dd_mult2f(Vec3Dd *v1, const Vec3Dd *v2, float f) {
	return Vec3Dd_mult2(v1, v2, (double) f);
}

static inline float Vec3Dd_dotProductf(const Vec3Dd *v1, const Vec3Dd *v2) {
	return (float) Vec3Dd_dotProduct(v1, v2);
}

static inline float Vec3Dd_lengthf(Vec3Dd *v) {
	return (float) Vec3Dd_length(v);
}


static inline Vec3D* Vec3Dd_toVec3D(Vec3D *u, const Vec3Dd *v);

static inline Vec3Dd* Vec3D_toVec3Dd(Vec3Dd *u, const Vec3D *v);

static inline Vec3D* Vec3Dd_toVec3D(Vec3D *u, const Vec3Dd *v) {
	u->x = (float) v->x;
	u->y = (float) v->y;
	u->z = (float) v->z;

	return u;
}

static inline Vec3Dd* Vec3D_toVec3Dd(Vec3Dd *u, const Vec3D *v) {
	u->x = (double) v->x;
	u->y = (double) v->y;
	u->z = (double) v->z;

	return u;
}


#endif

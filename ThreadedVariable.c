
#include "Thread.h"


#include "RedBlackTree.c"

#include "Types.c"


#ifndef THREADEDVARIABLE_C
#define THREADEDVARIABLE_C


#define _THREADEDVARIABLE_BUFSIZE 8


typedef struct ThreadedVariable {
	Mutex mtx;

	RedBlackTree *val;

	int cnt;

	void **buf;

} ThreadedVariable;


static void ThreadedVariable_init(ThreadedVariable *var) {
	Mutex_init(&var->mtx);

	var->val = RedBlackTree_create(RedBlackTree_defaultComparator);

	var->cnt = 0;

	var->buf = (void **) Memory_alloc((_THREADEDVARIABLE_BUFSIZE + 1) * sizeof(void *));

	var->buf[_THREADEDVARIABLE_BUFSIZE] = NULL;
}

static void ThreadedVariable_term(ThreadedVariable *var) {
	Mutex_term(&var->mtx);

	RedBlackTree_destroy(var->val);

	void **buf = var->buf;

	while(buf != NULL) {
		void **tmp = (void**) buf[_THREADEDVARIABLE_BUFSIZE];

		Memory_free(buf);

		buf = tmp;
	}
}


static inline ThreadedVariable* ThreadedVariable_create() {
	ThreadedVariable *var = (ThreadedVariable *) Memory_alloc(sizeof(ThreadedVariable));

	ThreadedVariable_init(var);

	return var;
}

static inline void ThreadedVariable_destroy(void *ptr) {
	ThreadedVariable_term((ThreadedVariable *) ptr);

	Memory_free(ptr);
}


static void** _ThreadedVariable_getVarAddrFromID(ThreadedVariable *var, void *idx, void *val) {
	Mutex_lock(&var->mtx);

	void **addr = RedBlackTree_search(var->val, idx);

	if(addr == NULL) {
		if(var->cnt >= _THREADEDVARIABLE_BUFSIZE) {
			void **buf = (void **) Memory_alloc((_THREADEDVARIABLE_BUFSIZE + 1) * sizeof(void *));

			buf[_THREADEDVARIABLE_BUFSIZE] = var->buf;

			var->cnt = 0;

			var->buf = buf;
		}

		addr = var->buf + var->cnt++;

		*addr = val;

		RedBlackTree_insert(var->val, idx, addr);
	}

	Mutex_unlock(&var->mtx);

	return addr;
}


static inline void** ThreadedVariable_getVarAddrFromID(ThreadedVariable *var, int idx, void *val) {
	return _ThreadedVariable_getVarAddrFromID(var, intToPtr(idx), val);
}

static inline void ThreadedVariable_setFromID(ThreadedVariable *var, int idx, void *val) {
	*_ThreadedVariable_getVarAddrFromID(var, intToPtr(idx), val) = val;
}

static inline void* ThreadedVariable_getFromID(ThreadedVariable *var, int idx, void *def_val) {
	return *_ThreadedVariable_getVarAddrFromID(var, intToPtr(idx), def_val);
}


static inline void** ThreadedVariable_getVarAddr(ThreadedVariable *var, void *val) {
	return ThreadedVariable_getVarAddrFromID(var, Thread_getID(), val);
}

static inline void ThreadedVariable_set(ThreadedVariable *var, void *val) {
	ThreadedVariable_setFromID(var, Thread_getID(), val);
}

static inline void* ThreadedVariable_get(ThreadedVariable *var, void *def_val) {
	return ThreadedVariable_getFromID(var, Thread_getID(), def_val);
}



static inline void** ThreadedVariable_getPointerAddressFromID(ThreadedVariable *var, int idx, void *def_val) {
	return ThreadedVariable_getVarAddrFromID(var, idx, def_val);
}

static inline double* ThreadedVariable_getDblAddrFromID(ThreadedVariable *var, int idx, double def_val) {
	return (double *) ThreadedVariable_getVarAddrFromID(var, idx, dblToPtr(def_val));
}

static inline float* ThreadedVariable_getFltAddrFromID(ThreadedVariable *var, int idx, float def_val) {
	return (float *) ThreadedVariable_getVarAddrFromID(var, idx, fltToPtr(def_val));
}

static inline int* ThreadedVariable_getIntAddrFromID(ThreadedVariable *var, int idx, int def_val) {
	return (int *) ThreadedVariable_getVarAddrFromID(var, idx, intToPtr(def_val));
}

static inline char* ThreadedVariable_getCharAddrFromID(ThreadedVariable *var, int idx, char def_val) {
	return (char *) ThreadedVariable_getVarAddrFromID(var, idx, charToPtr(def_val));
}


static inline void** ThreadedVariable_getPointerAddress(ThreadedVariable *var, void *def_val) {
	return ThreadedVariable_getVarAddr(var, def_val);
}

static inline double* ThreadedVariable_getDblAddr(ThreadedVariable *var, double def_val) {
	return (double *) ThreadedVariable_getVarAddr(var, dblToPtr(def_val));
}

static inline float* ThreadedVariable_getFltAddr(ThreadedVariable *var, float def_val) {
	return (float *) ThreadedVariable_getVarAddr(var, fltToPtr(def_val));
}

static inline int* ThreadedVariable_getIntAddr(ThreadedVariable *var, int def_val) {
	return (int *) ThreadedVariable_getVarAddr(var, intToPtr(def_val));
}

static inline char* ThreadedVariable_getCharAddr(ThreadedVariable *var, char def_val) {
	return (char *) ThreadedVariable_getVarAddr(var, charToPtr(def_val));
}


static inline void ThreadedVariable_setPointerFromID(ThreadedVariable *var, int idx, void *val) {
	ThreadedVariable_setFromID(var, idx, val);
}

static inline void ThreadedVariable_setDblFromID(ThreadedVariable *var, int idx, double val) {
	ThreadedVariable_setFromID(var, idx, dblToPtr(val));
}

static inline void ThreadedVariable_setFltFromID(ThreadedVariable *var, int idx, float val) {
	ThreadedVariable_setFromID(var, idx, fltToPtr(val));
}

static inline void ThreadedVariable_setIntFromID(ThreadedVariable *var, int idx, int val) {
	ThreadedVariable_setFromID(var, idx, intToPtr(val));
}

static inline void ThreadedVariable_setCharFromID(ThreadedVariable *var, int idx, char val) {
	ThreadedVariable_setFromID(var, idx, charToPtr(val));
}


static inline void* ThreadedVariable_getPointerFromID(ThreadedVariable *var, int idx, void *def_val) {
	return ThreadedVariable_getFromID(var, idx, def_val);
}

static inline double ThreadedVariable_getDblFromID(ThreadedVariable *var, int idx, double def_val) {
	return ptrToDbl(ThreadedVariable_getFromID(var, idx, dblToPtr(def_val)));
}

static inline float ThreadedVariable_getFltFromID(ThreadedVariable *var, int idx, float def_val) {
	return ptrToFlt(ThreadedVariable_getFromID(var, idx, fltToPtr(def_val)));
}

static inline int ThreadedVariable_getIntFromID(ThreadedVariable *var, int idx, int def_val) {
	return ptrToInt(ThreadedVariable_getFromID(var, idx, intToPtr(def_val)));
}

static inline char ThreadedVariable_getCharFromID(ThreadedVariable *var, int idx, char def_val) {
	return ptrToChar(ThreadedVariable_getFromID(var, idx, charToPtr(def_val)));
}



static inline void ThreadedVariable_setPointer(ThreadedVariable *var, void *val) {
	ThreadedVariable_set(var, val);
}

static inline void ThreadedVariable_setDbl(ThreadedVariable *var, double val) {
	ThreadedVariable_set(var, dblToPtr(val));
}

static inline void ThreadedVariable_setFlt(ThreadedVariable *var, float val) {
	ThreadedVariable_set(var, fltToPtr(val));
}

static inline void ThreadedVariable_setInt(ThreadedVariable *var, int val) {
	ThreadedVariable_set(var, intToPtr(val));
}

static inline void ThreadedVariable_setChar(ThreadedVariable *var, char val) {
	ThreadedVariable_set(var, charToPtr(val));
}


static inline void* ThreadedVariable_getPointer(ThreadedVariable *var, void *def_val) {
	return ThreadedVariable_get(var, def_val);
}

static inline double ThreadedVariable_getDbl(ThreadedVariable *var, double def_val) {
	return ptrToDbl(ThreadedVariable_get(var, dblToPtr(def_val)));
}

static inline float ThreadedVariable_getFlt(ThreadedVariable *var, float def_val) {
	return ptrToFlt(ThreadedVariable_get(var, fltToPtr(def_val)));
}

static inline int ThreadedVariable_getInt(ThreadedVariable *var, int def_val) {
	return ptrToInt(ThreadedVariable_get(var, intToPtr(def_val)));
}

static inline char ThreadedVariable_getChar(ThreadedVariable *var, char def_val) {
	return ptrToChar(ThreadedVariable_get(var, charToPtr(def_val)));
}


#endif

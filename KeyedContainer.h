
#ifndef KEYEDCONTAINER_H
#define KEYEDCONTAINER_H

typedef struct KeyedContainer KeyedContainer;

#endif


/*

static inline void KeyedContainer_init(KeyedContainer *ctn);

static inline void KeyedContainer_term(void *ptr);


static inline KeyedContainer* KeyedContainer_create();

static inline void KeyedContainer_destroy(void *ptr);


static inline void KeyedContainer_setMT(KeyedContainer *ctn);

static inline void KeyedContainer_lock(KeyedContainer *ctn);

static inline void KeyedContainer_unlock(KeyedContainer *ctn);


static inline int KeyedContainer_insert(KeyedContainer *ctn, void *key, void *val);

static inline int KeyedContainer_remove(KeyedContainer *ctn, void *key);

static inline void* KeyedContainer_search(KeyedContainer *ctn, void *key);


static inline void** KeyedContainer_getKeys(KeyedContainer *ctn, int *num);

static inline void** KeyedContainer_getVals(KeyedContainer *ctn, int *num);

static inline int KeyedContainer_get(KeyedContainer *ctn, void ***key, void ***val);


static inline int KeyedContainer_fill(KeyedContainer *ctn, void **k, void **v, int n, int o);

*/

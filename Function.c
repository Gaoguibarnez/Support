
#include "Memory.h"


#ifndef FUNCTION_C
#define FUNCTION_C



typedef struct Function {
	void *ptr;

	void* (*call)(void *, int, void *[]);

} Function;


static void Function_init(Function *fnc, void *ptr, void* (*call)(void *, int , void *[])) {
	fnc->ptr = ptr;

	fnc->call = call;
}

static void Function_term(Function *fnc) {
}


static Function* Function_create(void *ptr, void* (*call)(void *, int, void *[])) {
	Function *fnc = (Function*) Memory_alloc(sizeof(Function));

	Function_init(fnc, ptr, call);

	return fnc;
}

static void Function_destroy(void *ptr) {
	Memory_free(ptr);
}


static inline void* Function_call(Function *fnc, int argc, void *argv[]) {
	return fnc->call(fnc->ptr, argc, argv);
}


static inline int Function_checkCallFunction(Function *fnc, void* (*call)(void *, int, void *[])) {
	return fnc->call == call;
}

static inline void* Function_getRefPointer(Function *fnc) {
	return fnc->ptr;
}


static inline void __Function__use() {
	Function_init(NULL, NULL, NULL);
	Function_term(NULL);
	Function_create(NULL, NULL);
	Function_destroy(NULL);
	Function_call(NULL, 0, NULL);
}



#endif

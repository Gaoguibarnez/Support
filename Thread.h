
#ifndef THREAD_H
#define THREAD_H



#if defined _WIN32

#define WIN32_LEAN_AND_MEAN

#include <windows.h>

typedef CRITICAL_SECTION Mutex;

#elif defined __linux__

#include <pthread.h>

typedef pthread_mutex_t Mutex;

#else

#error THREAD: UNSUPPORTED PLATFORM

#endif


#include "Memory.h"



int Thread_getID();


typedef struct Thread {
	void* (*run)(void *arg);

	int str;
	int end;

	void *arg;
	void *ret;

} Thread;


void Thread_init(Thread *thr, void* (*run)(void*));

void Thread_term(Thread *thr);


Thread* Thread_create(void* (*run)(void *arg));

void Thread_destroy(void *ptr);


int Thread_start(Thread *thr, void *arg);

void Thread_wait(Thread *thr);


int Thread_isRunning(Thread *thr);

void* Thread_getReturn(Thread *thr);


void Thread_sleep(double t);

void Thread_sleepf(float t);


int QuickThread_start(void* (*run)(void*), void *arg);


#endif

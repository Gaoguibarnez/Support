
#include "Memory.h"

#include "Mutex.h"


#ifndef QUEUEMT_C
#define QUEUEMT_C


typedef struct Queue Queue;

static void Queue_term(void *q);

static void Queue_initMT(Queue *q);

static void Queue_init(Queue *q);

static void Queue_destroy(void *v);

static void Queue_destroy2(void *v, void (*destroyElement)(void *v));

static Queue* Queue_createMT();

static Queue* Queue_create();

static void Queue_insert(Queue *q, void *v);

static void* Queue_remove(Queue *q);

static void* Queue_peek(Queue *q, int i);

static void Queue_push(Queue *q, void *v);

static void* Queue_pop(Queue *q);

static void* Queue_next(Queue *q);

static void* Queue_getNext(Queue *q);

static inline int Queue_hasNext(Queue *q);

static void Queue_append(Queue *q, Queue *n);

static void Queue_iterator(Queue *q, int (*iterator)(void *ptr, void *val), void *ptr);


static inline void __Queue__use() {
    Queue_createMT();
    Queue_create();
    Queue_insert(NULL, NULL);
    Queue_peek(NULL, 0);
    Queue_push(NULL, NULL);
    Queue_pop(NULL);
    Queue_remove(NULL);
    Queue_getNext(NULL);
    Queue_hasNext(NULL);
    Queue_append(NULL, NULL);
    Queue_iterator(NULL, NULL, NULL);
    Queue_destroy(NULL);
    Queue_destroy2(NULL, NULL);
}



typedef struct __QueueNode__ {
    void *v;

    struct __QueueNode__ *next;

} __QueueNode__;

static inline __QueueNode__* _QueueNode_create() {
    return (__QueueNode__*) Memory_alloc(sizeof(__QueueNode__));
}

static inline void _QueueNode_destroy(void *ptr) {
    Memory_free(ptr);
}


typedef struct Queue {
    __QueueNode__ *in;
    __QueueNode__ *out;

    Mutex *mtx;

} Queue;

static void Queue_term(void *v) {
    Queue *q = (Queue*) v;

    while(Queue_hasNext(q)) Queue_remove(q);
        

    if(q->mtx != NULL) {
        Mutex_term(q->mtx);

        Memory_free(q->mtx);
    }
}

static void Queue_init(Queue *q) {
    q->in = NULL;
    q->out = NULL;

    q->mtx = NULL;
}

static void Queue_initMT(Queue *q) {
    Queue_init(q);

    Mutex_init(q->mtx = Memory_alloc(sizeof(Mutex)));
}

static void Queue_destroy(void *v) {
    Queue_term(v);

    Memory_free(v);
}

static void Queue_destroy2(void *v, void (*destroyElement)(void *)) {
    Queue *q = (Queue*) v;

    while(Queue_getNext(q) != NULL)
        destroyElement(Queue_remove(q));

    if(q->mtx != NULL) {
        Mutex_term(q->mtx);

        Memory_free(q->mtx);
    }

    Memory_free(q);
}

static inline Queue* Queue_create() {
    Queue *q = (Queue*) Memory_alloc(sizeof(Queue));

    Queue_init(q);

    return q;
}

static Queue* Queue_createMT() {
    Queue *q = (Queue*) Memory_alloc(sizeof(Queue));

    Queue_initMT(q);

    return q;
}

static inline void __QUEUE_LOCK(Queue *q) {
    if(q->mtx != NULL) Mutex_lock(q->mtx);
}

static inline void __QUEUE_UNLOCK(Queue *q) {
    if(q->mtx != NULL) Mutex_unlock(q->mtx);
}

static void Queue_insert(Queue *q, void *v) {
    __QueueNode__ *n = _QueueNode_create();

    n->v = v;

    n->next = NULL;

    __QUEUE_LOCK(q);

    if(q->in == NULL) {
        if(q->out == NULL) {
            q->out = n;

        } else {
            q->out->next = n;

            q->in = n;
        }

    } else {
        q->in->next = n;

        q->in = n;
    }

    __QUEUE_UNLOCK(q);
}

static void* Queue_remove(Queue *q) {
    void *v = NULL;

    __QUEUE_LOCK(q);

    if(q->out != NULL) {
        v = q->out->v;

        __QueueNode__ *n = q->out->next;

        _QueueNode_destroy(q->out);

        q->out = n;

        if(n == q->in) {
            q->in = NULL;
        }
    }

    __QUEUE_UNLOCK(q);

    return v;
}

static void* Queue_peek(Queue *q, int i) {
    void *v = NULL;

    __QUEUE_LOCK(q);

    __QueueNode__ *n = q->out;

    while(n != NULL) {
        if(--i < 0) {
            v = n->v;

            break;
        }

        n = n->next;
    }

    __QUEUE_UNLOCK(q);

    return v;
}

static void Queue_push(Queue *q, void *v) {
    __QueueNode__ *n = _QueueNode_create();

    n->v = v;

    __QUEUE_LOCK(q);

    n->next = q->out;

    if(q->in != NULL) {
        q->out = n;

    } else {
        if(q->out == NULL) {
            q->out = n;

        } else {
            q->in = q->out;

            q->out = n;
        }
    }

    __QUEUE_UNLOCK(q);
}

static void* Queue_pop(Queue *q) {
    return Queue_remove(q);
}

static void* Queue_next(Queue *q) {
    void *v = NULL;

    __QUEUE_LOCK(q);

    if(q->out != NULL) {
        v = q->out->v;
    }

    __QUEUE_UNLOCK(q);

    return v;
}

static inline void* Queue_getNext(Queue *q) {
    return Queue_next(q);
}

static inline int Queue_hasNext(Queue *q) {
	return q->out != NULL;
}

static void Queue_append(Queue *q, Queue *n) {
    __QUEUE_LOCK(n);

    __QueueNode__ *in = n->in;
    __QueueNode__ *out = n->out;

    n->in = NULL;
    n->out = NULL;

    __QUEUE_UNLOCK(n);

    if(out == NULL) return;

    __QUEUE_LOCK(q);

    if(q->in == NULL) {
        if(q->out == NULL) {
            q->out = out;

            q->in = in;

        } else {
            q->out->next = out;

            if(in == NULL) {
                q->in = out;

            } else {
                q->in = in;
            }
        }

    } else {
        q->in->next = out;

        if(in == NULL) {
            q->in = out;

        } else {
            q->in = in;
        }
    }

    __QUEUE_UNLOCK(q);
}

static void Queue_iterator(Queue *q, int (*iterator)(void *ptr, void *val), void *ptr) {
    __QueueNode__ *o = q->out;

    __QUEUE_LOCK(q);

    while(o != NULL) {
        if(iterator(ptr, o->v) == 0) {
            break;
        }

        o = o->next;
    }

    __QUEUE_UNLOCK(q);
}


#endif


#include <stdint.h>

#include <stdint.h>

#include <math.h>


#ifndef RAND64_C
#define RAND64_C



static uint32_t __rnd64_m1 = 0;
static uint32_t __rnd64_m2 = 0;
static uint64_t __rnd64_tm = 0;

typedef struct Rand64 {
	uint64_t st[2];
    uint32_t m1;
    uint32_t m2;
    uint64_t tm;

} Rand64;

static inline void __Rand64_next(Rand64 *r) {
    r->st[0] &= UINT64_C(0x7FFFFFFFFFFFFFFF);

    uint64_t x = r->st[0] ^ r->st[1];

    x ^= x << 12;
    x ^= x >> 32;
    x ^= x << 32;
    x ^= x << 11;

    r->st[0] = r->st[1];
    r->st[1] = x;
    r->st[0] ^= - ((int64_t) (x & 1)) & r->m1;
    r->st[1] ^= - ((int64_t) (x & 1)) & (((uint64_t) r->m2) << 32);
}

static inline uint64_t __Rand64_ext(Rand64 *r) {
    uint64_t x = r->st[0] + r->st[1];

    x ^= r->st[0] >> 8;
    x ^= - ((int64_t) (x & 1)) & r->tm;

    return x;
}

static inline double __Rand64_ext_conv(Rand64 *r) {
    union {
		uint64_t u;
		double d;

    } conv;

    uint64_t x = r->st[0] + r->st[1];

    x ^= r->st[0] >> 8;

    conv.u = ((x ^ (- ((int64_t) (x & 1)) & r->tm)) >> 12) | UINT64_C(0x3FF0000000000000);

    return conv.d;
}

static inline void Rand64_init(Rand64 *r, uint64_t s) {
    r->m1 = __rnd64_m1;
    r->m2 = __rnd64_m2;
    r->tm = __rnd64_tm;

    r->st[0] = s ^ ((uint64_t) r->m1 << 32);
    r->st[1] = r->m2 ^ r->tm;

    int i;

    for(i=1; i<8; i++) {
		r->st[i & 1] ^= i + UINT64_C(6364136223846793005) * (r->st[(i - 1) & 1] ^ (r->st[(i - 1) & 1] >> 62));
    }

    if((r->st[0] & UINT64_C(0x7FFFFFFFFFFFFFFF)) == 0 && r->st[1] == 0) {
		r->st[0] = 'T';
		r->st[1] = 'M';
    }
}

static inline void Rand64_term(Rand64 *r) {
}

static inline uint64_t Rand64_uint64(Rand64 *r) {
    __Rand64_next(r);

    return __Rand64_ext(r);
}

static inline unsigned int Rand64_int(Rand64 *r, unsigned int rng) {
    if(rng == 0) return 0;

    return (unsigned int) Rand64_uint64(r) % rng;
}

static inline double Rand64_double(Rand64 *r) {
    return (double) Rand64_uint64(r) / 18446744073709551615.0;
}

static inline double Rand64_gaussianDouble(Rand64 *r, double m, double s) {
    double u1, u2, rt;

    while(1) {
        u1 = Rand64_double(r);

        u2 = (2.0f * Rand64_double(r) - 1.0f) * 0.857763884960706796480190f;

        rt = u2 / u1;

        if(- 4.0f * log(u1) >= rt * rt) {
            if(Rand64_int(r, 2) == 0) {
                return - rt * s + m;
            }

            return rt * s + m;
        }
    }

    return 0.0;
}

static inline double Rand64_gammaDouble(Rand64 *r, double a, double b) {
    #define e 2.71828182845904523536029

    double c1 = pow((a - 1.0f) / e, (a - 1.0f) / 2.0f);
    double c2 = pow((a + 1.0f) / e, (a + 1.0f) / 2.0f);

    double u1, u2, rt;

    #undef e

    while(1) {
        u1 = c1 * Rand64_double(r);
        u2 = c2 * Rand64_double(r);

        rt = u2 / u1;

        if(u1 <= sqrt(pow(rt, a - 1.0f) * exp(- rt))) {
            return rt * b;
        }
    }

    return 0.0;
}

static inline float Rand64_float(Rand64 *r) {
    return (float) Rand64_double(r);
}

static inline float Rand64_gaussianFloat(Rand64 *r, float m, float s) {
    return (float) Rand64_gaussianDouble(r, (double) m, (double) s);
}

static inline float Rand64_gammaFloat(Rand64 *r, float a, float b) {
    return (float) Rand64_gammaDouble(r, (double) a, (double) b);
}

static inline double __Rand64_pow2(double d) {
    return d * d;
}

static inline unsigned int Rand64_poissonInt(Rand64 *r, float l) {
    if(l < 30.0f) {
        unsigned int k = 0;

        double e_l = exp((double) - l);
        double p = Rand64_double(r);

        while(p > e_l) {
            p *= Rand64_double(r);
            ++k;
        }

        return k;
    }

    double l_d = (double) l;
    double c = 0.767 - 3.36 / l_d;
    double beta = M_PI / sqrt(3.0 * l_d);
    double alpha = beta * l_d;
    double k = log(c / beta) - l_d;
    double log_l = log(l_d);

    while(1) {
        double u = Rand64_double(r);
        double x = (alpha - log((1.0 - u) / u)) / beta;
        double n = floor(x + 0.5);

        if(n >= 0.0) {
            double v = Rand64_double(r);
            double y = alpha - beta * x;
            double lhs = y + log(v / __Rand64_pow2(1.0 + exp(y)));
            double rhs = k + n * log_l - lgamma(n + 1.0);

            if(lhs < rhs) return (unsigned int) n;
        }
    }

    return 0;
}

#define __RND64STR_NUM_CHR 72

const char __rnd64str_chr[] = {
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 
    'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 
    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 
    'y', 'z', 
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
    'Y', 'Z', 
    '0', '1', '2', '3', '4', '5', '6', '7', 
    '8', '9', 
    '-', '*', '#', '~', '&', '@', '!', '?', 
    '+', '=', 
};

static inline char* Rand64_string(Rand64 *r, char *b, int n) {
    char *t = b;

    while(--n > 0) {
        *t++ = __rnd64str_chr[Rand64_int(r, __RND64STR_NUM_CHR)];
    }

    *t = '\0';

    return b;
}



#endif

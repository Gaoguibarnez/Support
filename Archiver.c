
#include <stdio.h>

#include <sys/stat.h>



#ifndef ARCHIVER_C
#define ARCHIVER_C



static inline int _Archiver_getFileSize(char *file) {
	struct stat st; 

	if(stat(file, &st) == 0) {
    	return st.st_size;
	}

	return 0; 
}

static int Archiver_pack(char *out, int n, char *in[]) {
	FILE *f = fopen(out, "wb");

	if(f == NULL) {
		printf("could not open output file '%s'.\n", out);

		return 0;
	}

	int s[n], i, j, off = 0;

	fwrite(&n, sizeof(int), 1, f);

	for(i=0; i<n; i++) {
		int l = strlen(in[i]);

		fwrite(&l, sizeof(int), 1, f);

		fwrite(in[i], sizeof(char), l, f);

		s[i] = _Archiver_getFileSize(in[i]);

		fwrite(s + i, sizeof(int), 1, f);

		off += s[i];

		fwrite(&off, sizeof(int), 1, f);
	}

	for(i=0; i<n; i++) {
		if(s[i] > 0) {
			FILE *t = fopen(in[i], "rb");

			if(t == NULL) {
				printf("could not open input file '%s'.\n", in[i]);

				fclose(f);

				return 0;
			}

			for(j=s[i]; j>0; j--) {
				char c = fgetc(t);

				fwrite(&c, sizeof(char), 1, f);
			}
		}
	}

	fclose(f);

	return 1;
}

static int Archiver_print(char *arch) {
	FILE *f = fopen(arch, "rb");

	if(f == NULL) {
		printf("could not open archive '%s'.\n", arch);

		return 0;
	}

	printf("%s:\n", arch);

	int i, o, s, n = 0;

	fread(&n, sizeof(int), 1, f);

	for(i=0; i<n; i++) {
		int l;

		fread(&l, sizeof(int), 1, f);

		char name[l + 1];

		fread(name, sizeof(char), l, f);

		name[l] = '\0';

		fread(&s, sizeof(int), 1, f);

		fread(&o, sizeof(int), 1, f);

		printf("\t%s [%d] [%d]\n", name, s, o);
	}

	printf("\n");

	fclose(f);

	return 1;
}

static int Archiver_unpackFile(char *arch, char *file, char *out) {
	FILE *f = fopen(arch, "rb");

	if(f == NULL) {
		printf("could not open archive '%s'.\n", arch);

		return 0;
	}

	int fnd = 0;

	int i, n = 0;

	int o = 0;
	int s = 0;

	fread(&n, sizeof(int), 1, f);

	for(i=0; i<n; i++) {
		int l;

		fread(&l, sizeof(int), 1, f);

		char name[l + 1];

		fread(name, sizeof(char), l, f);

		name[l] = '\0';

		if(fnd) {
			int t[2];

			fread(t, sizeof(int), 2, f);

		} else {
			fread(&s, sizeof(int), 1, f);

			fread(&o, sizeof(int), 1, f);

			if(strcasecmp(name, file) == 0) {
				fnd = 1;
			}
		}
	}

	if(!fnd) {
		printf("could not find file '%s' in archive '%s'.\n", file, arch);

		fclose(f);

		return 0;
	}

	fseek(f, o, SEEK_CUR);

	FILE *t = fopen(out, "wb");

	if(t == NULL) {
		printf("could not open output file '%s'.\n", out);

		fclose(f);

		return 0;
	}

	for(i=0; i<s; i++) {
		char c = fgetc(f);

		fwrite(&c, sizeof(char), 1, t);
	}

	fclose(t);

	fclose(f);

	return 1;
}

static FILE* Archiver_openFile(char *arch, char *file) {
	FILE *f = fopen(arch, "rb");

	if(f == NULL) {
		printf("could not open archive '%s'.\n", arch);

		return NULL;
	}

	int fnd = 0;

	int i, n = 0;

	int o = 0;
	int s = 0;

	fread(&n, sizeof(int), 1, f);

	for(i=0; i<n; i++) {
		int l;

		fread(&l, sizeof(int), 1, f);

		char name[l + 1];

		fread(name, sizeof(char), l, f);

		name[l] = '\0';

		if(fnd) {
			int t[2];

			fread(t, sizeof(int), 2, f);

		} else {
			fread(&s, sizeof(int), 1, f);

			fread(&o, sizeof(int), 1, f);

			if(strcasecmp(name, file) == 0) {
				fnd = 1;
			}
		}
	}

	if(!fnd) {
		printf("could not find file '%s' in archive '%s'.\n", file, arch);

		fclose(f);

		return NULL;
	}

	fseek(f, o, SEEK_CUR);

	FILE *t = tmpfile();

	if(t == NULL) {
		printf("could not create temporary file.\n");

		fclose(f);

		return NULL;
	}

	for(i=0; i<s; i++) {
		char c = fgetc(f);

		fwrite(&c, sizeof(char), 1, t);
	}

	rewind(t);

	fclose(f);

	return t;
}


#endif


#include <stdio.h>

#include <stdlib.h>

#include <math.h>


#ifndef MATRIX_C
#define MATRIX_C


#include "Memory.h"


static inline float* Matrix_create(int si, int sj);

static inline float* Matrix_create2(int si, int sj, float v);

static inline float* Matrix_createI(int si, int sj);

static inline void Matrix_destroy(void *ptr);

static inline void Matrix_print(float *m, int si, int sj);

static inline int Matrix_save(float *m, int si, int sj, FILE *f);

static inline float* Matrix_load(FILE *f, int *si, int *sj);

static inline int Matrix_saveb(float *m, int si, int sj, FILE *f);

static inline float* Matrix_loadb(FILE *f, int *si, int *sj);

static inline float Matrix_setValue(float *m, int si, int sj, int i, int j, float v);

static inline float Matrix_getValue(float *m, int si, int sj, int i, int j);

static inline float Matrix_length(float *m, int si, int sj);

static inline int Matrix_set(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj);

static inline int Matrix_setI(float *m, int m_si, int m_sj);

static inline int Matrix_set0(float *m, int m_si, int m_sj);

static inline int Matrix_randomize(float *m, int m_si, int m_sj);

static inline int Matrix_getSubMatrix(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, int oi, int oj);

static inline int Matrix_rmvLineAndCol(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, int ri, int rj);

static inline int Matrix_transpose(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj);

static inline int Matrix_sum(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, float *m2, int m2_si, int m2_sj);

static inline int Matrix_sub(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, float *m2, int m2_si, int m2_sj);

static inline int Matrix_mult(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, float k);

static inline int Matrix_prod(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, float *m2, int m2_si, int m2_sj);

static inline void Matrix_swapLine(float *m, int m_si, int m_sj, int i0, int i1);

static inline void Matrix_multLine(float *m, int m_si, int m_sj, int i, float k);

static inline void Matrix_sumLine(float *m, int m_si, int m_sj, int i0, int i1);

static inline void Matrix_sumMultLine(float *m, int m_si, int m_sj, int i0, int i1, float k);

static inline int Matrix_scal(float *m, int m_si, int m_sj);

static inline int Matrix_det(float *d, float *m, int m_si, int m_sj);

static inline int Matrix_det2(float *d, float *m, int m_si, int m_sj);

static inline int Matrix_inv(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj);

static inline int Matrix_inv2(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj);

static inline int Matrix_solve(float *x, int x_s, float *a, int a_si, int a_sj, float *b, int b_s);


static inline float* Matrix_create(int si, int sj) {
	float *m = (float*) Memory_alloc(si * sj * sizeof(float));

	return m;
}

static inline float* Matrix_create2(int si, int sj, float v) {
	float *m = Matrix_create(si, sj);

	int i, n = si * sj;

	for(i=0; i<n; i++)
		m[i] = v;

	return m;
}

static inline float* Matrix_createI(int si, int sj) {
	float *m = Matrix_create(si, sj);

	int i, j;

	for(i=0; i<si; i++)
		for(j=0; j<sj; j++) {
			if(i == j) {
				m[i * sj + j] = 1.0f;

			} else
				m[i * sj + j] = 0.0f;
		}

	return m;
}

static inline void Matrix_destroy(void *ptr) {
	Memory_free(ptr);
}

static inline void Matrix_print(float *m, int si, int sj) {
	int i, j;

	for(i=0; i<si; i++) {
		for(j=0; j<sj; j++)
			printf("%f\t", m[i * sj + j]);

		printf("\n");
	}

	printf("\n");
}

static inline int Matrix_save(float *m, int si, int sj, FILE *f) {
	int i, j;

	for(i=0; i<si; i++) {
		for(j=0; j<sj; j++)
			if(fprintf(f, "%.8f\t", m[i * sj + j]) < 0) return 0;

		if(fprintf(f, "\n") < 0) return 0;
	}

	return 1;
}

static inline int __Matrix_isSpace(char c) {
	return c == ' ' || c == '\t';
}

static inline int __Matrix_isNewLine(char c) {
	return c == '\n';
}

static inline int __Matrix_isEnd(char c) {
	return c == '\0';
}

static inline int __Matrix_countLineElements(char *c, char **e) {
	int s = 0;

	while(1) {
		while(__Matrix_isSpace(*c)) ++c;

		if(__Matrix_isEnd(*c) || __Matrix_isNewLine(*c)) break;

		++s;

		while(!__Matrix_isSpace(*c)) {
			if(__Matrix_isEnd(*c) || __Matrix_isNewLine(*c)) break;

			++c;
		}
	}

	*e = c;

	return s;
}

static inline float* Matrix_load(FILE *f, int *si, int *sj) {
	#define __MATRIX_FILEMAXSIZE (24 * 1024 * 1024) // 24 MiB

	char *buf = (char*) Memory_alloc(__MATRIX_FILEMAXSIZE);

	if(buf == NULL) return NULL;

	int i, n = fread(buf, 1, __MATRIX_FILEMAXSIZE - 1, f);

	if(!feof(f)) {
		Memory_free(buf);

		return NULL; // FILE IS TOO BIG
	}

	buf[n] = '\0';

	char *tmp = buf;

	*si = 0;
	*sj = - 1;

	int err = 0;

	while(__Matrix_isSpace(*tmp) || __Matrix_isNewLine(*tmp)) ++tmp;

	char *fst_ln = tmp;

	int sj_tmp = 0;

	while(1) {
		char *ln = tmp;

		sj_tmp = __Matrix_countLineElements(ln, &tmp);

		if(tmp == NULL) {
			err = 1;

			break;
		}

		if(sj_tmp == 0) break;

		++(*si);

		if(*sj == - 1) {
			*sj = sj_tmp;

		} else if(*sj != sj_tmp) {
			err = 1;

			break;
		}

		++tmp;
	}

	if(err) {
		Memory_free(buf);

		return NULL;
	}

	float *m = Matrix_create(*si, *sj);

	n = *si * *sj;

	tmp = fst_ln;

	for(i=0; i<n; i++) {
		while(__Matrix_isSpace(*tmp) || __Matrix_isNewLine(*tmp)) ++tmp;

		if(__Matrix_isEnd(*tmp)) {
			m[i] = 0.0f;

		} else {
			char *sym = tmp;

			while(!__Matrix_isSpace(*tmp) && !__Matrix_isNewLine(*tmp)) {
				if(__Matrix_isEnd(*tmp)) break;

				++tmp;
			}

			if(!__Matrix_isEnd(*tmp)) {
				*tmp++ = '\0';
			}

			m[i] = strtof(sym, NULL);
		}
	}

	Memory_free(buf);

	return m;
}

static inline int Matrix_saveb(float *m, int si, int sj, FILE *f) {
	if(fwrite(&si, sizeof(int), 1, f) != 1) return 0;
	if(fwrite(&sj, sizeof(int), 1, f) != 1) return 0;

	int n = si * sj;

	return fwrite(m, sizeof(float), n, f) == n;
}

static inline float* Matrix_loadb(FILE *f, int *si, int *sj) {
	if(fread(si, sizeof(int), 1, f) != 1) return NULL;
	if(fread(sj, sizeof(int), 1, f) != 1) return NULL;

	float *m = Matrix_create(*si, *sj);

	if(m == NULL) return NULL;

	int n = *si * *sj;

	if(fread(m, sizeof(float), n, f) == n) return m;

	Matrix_destroy(m);

	return NULL;
}

static inline float Matrix_setValue(float *m, int si, int sj, int i, int j, float v) {
	m[i * sj + j] = v;

	return v;
}

static inline float Matrix_getValue(float *m, int si, int sj, int i, int j) {
	return m[i * sj + j];
}

static inline float Matrix_length(float *m, int si, int sj) {
	float s2 = 0.0f;

	int n = si * sj;

	while(--n >= 0) {
		float v = *m++;

		s2 += v * v;
	}

	return sqrtf(s2);
}

static inline int Matrix_set(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj) {
	if(m0_si != m1_si) return 0;

	if(m0_sj != m1_sj) return 0;

	int i, j;

	for(i=0; i<m1_si; i++)
		for(j=0; j<m1_sj; j++)
			m0[i * m0_sj + j] = m1[i * m1_sj + j];

	return 1;
}

static inline int Matrix_setI(float *m, int m_si, int m_sj) {
	int i, j;

	for(i=0; i<m_si; i++)
		for(j=0; j<m_sj; j++) {
			if(i == j) {
				m[i * m_sj + j] = 1.0f;

			} else
				m[i * m_sj + j] = 0.0f;
		}

	return 1;
}

static inline int Matrix_set0(float *m, int m_si, int m_sj) {
	memset(m, 0, m_si * m_sj * sizeof(float));

	return 1;
}

static inline int Matrix_randomize(float *m, int m_si, int m_sj) {
	int i, n = m_si * m_sj;

	for(i=0; i<n; i++)
		m[i] = ((float) rand() / RAND_MAX);

	return 1;
}

static inline int Matrix_getSubMatrix(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, int oi, int oj) {
	int i, j;

	for(i=0; i<m0_si; i++) {
		for(j=0; j<m0_sj; j++) {
			float v = 0.0f;

			int i1 = i + oi;
			int j1 = j + oj;

			if(i1 >= 0 && i1 < m1_si) {
				if(j1 >= 0 && j1 < m1_sj) {
					v = m1[i1 * m1_sj + j1];
				}
			}

			m0[i * m0_sj + j] = v;
		}
	}

	return 1;
}

static inline int Matrix_rmvLineAndCol(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, int ri, int rj) {
	if(m0_si != m1_si - 1) return 0;

	if(m0_sj != m1_sj - 1) return 0;

	int i, j;

	for(i=0; i<ri; i++) {
		for(j=0; j<rj; j++)
			m0[i * m0_sj + j] = m1[i * m1_sj + j];

		for(j=rj+1; j<m1_sj; j++)
			m0[i * m0_sj + (j-1)] = m1[i * m1_sj + j];
	}

	for(i=ri+1; i<m1_si; i++) {
		for(j=0; j<rj; j++)
			m0[(i-1) * m0_sj + j] = m1[i * m1_sj + j];

		for(j=rj+1; j<m1_sj; j++)
			m0[(i-1) * m0_sj + (j-1)] = m1[i * m1_sj + j];
	}

	return 1;
}

static inline int Matrix_transpose(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj) {
	if(m0_si != m1_sj) return 0;

	if(m0_sj != m1_si) return 0;

	int i, j;

	for(i=0; i<m1_si; i++)
		for(j=0; j<m1_sj; j++)
			m0[j * m0_si + i] = m1[i * m1_si + j];

	return 1;
}

static inline int Matrix_sum(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, float *m2, int m2_si, int m2_sj) {
	if(m1_si != m2_si) return 0;

	if(m1_sj != m2_sj) return 0;

	if(m0_si != m1_si) return 0;

	if(m0_sj != m1_sj) return 0;

	int i, n = m1_si * m1_sj;

	for(i=0; i<n; i++)
		m0[i] = m1[i] + m2[i];

	return 1;
}

static inline int Matrix_sub(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, float *m2, int m2_si, int m2_sj) {
	if(m1_si != m2_si) return 0;

	if(m1_sj != m2_sj) return 0;

	if(m0_si != m1_si) return 0;

	if(m0_sj != m1_sj) return 0;

	int i, n = m1_si * m1_sj;

	for(i=0; i<n; i++)
		m0[i] = m1[i] - m2[i];

	return 1;
}

static inline int Matrix_mult(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, float k) {
	if(m0_si != m1_si) return 0;

	if(m0_sj != m1_sj) return 0;

	int i, n = m0_si * m0_sj;

	for(i=0; i<n; i++)
		m0[i] = m1[i] * k;

	return 1;
}

static inline int Matrix_prod(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj, float *m2, int m2_si, int m2_sj) {
	if(m1_sj != m2_si) return 0;

	if(m0_si != m1_si) return 0;

	if(m0_sj != m2_sj) return 0;

	int i, j, k;

	for(i=0; i<m1_si; i++)
		for(j=0; j<m2_sj; j++) {
			m0[i * m0_sj + j] = 0.0f;

			for(k=0; k<m1_sj; k++)
				m0[i * m0_sj + j] += m1[i * m1_sj + k] * m2[k * m2_sj + j];
		}

	return 1;
}

static inline void Matrix_swapLine(float *m, int m_si, int m_sj, int i0, int i1) {
	float tmp;

	int j;

	for(j=0; j<m_sj; j++) {
		tmp = m[i0 * m_sj + j];

		m[i0 * m_sj + j] = m[i1 * m_sj + j];

		m[i1 * m_sj + j] = tmp;
	}
}

static inline void Matrix_multLine(float *m, int m_si, int m_sj, int i, float k) {
	int j;

	for(j=0; j<m_sj; j++)
		m[i * m_sj + j] *= k;
}

static inline void Matrix_sumLine(float *m, int m_si, int m_sj, int i0, int i1) {
	int j;

	for(j=0; j<m_sj; j++)
		m[i0 * m_sj + j] += m[i1 * m_sj + j];
}

static inline void Matrix_sumMultLine(float *m, int m_si, int m_sj, int i0, int i1, float k) {
	int j;

	for(j=0; j<m_sj; j++)
		m[i0 * m_sj + j] += m[i1 * m_sj + j] * k;
}

static inline int Matrix_scal(float *m, int m_si, int m_sj) {
	int i, j;

	for(j=0; j<m_sj; j++) {
		if(m[j * m_sj + j] == 0.0f)
			for(i=j+1; i<m_si; i++)
				if(m[i * m_sj + j] != 0.0f) {
					Matrix_swapLine(m, m_si, m_sj, j, i);

					break;
				}

		if(m[j *m_sj + j] != 0.0f)
			for(i=j+1; i<m_si; i++)
				if(m[i * m_sj + j] != 0.0f)
					Matrix_sumMultLine(m, m_si, m_sj, i, j, - m[i * m_sj + j] / m[j * m_sj + j]);
	}

	return 1;
}

static inline int Matrix_det(float *d, float *m, int m_si, int m_sj) {
	if(m_si != m_sj) {
		return 0;
	}

	float tmp[m_si * m_sj];

	int i, j;

	for(i=0; i<m_si; i++)
		for(j=0; j<m_sj; j++)
			tmp[i * m_sj + j] = m[i * m_sj + j];

	m = tmp;

	*d = 1.0f;

	for(j=0; j<m_sj; j++) {
		if(m[j * m_sj + j] == 0.0f) {
			for(i=j+1; i<m_si; i++)
				if(m[i * m_sj + j] != 0.0f) {
					Matrix_swapLine(m, m_si, m_sj, j, i);

					*d *= - 1.0f;

					break;
				}

			if(m[j *m_sj + j] == 0.0f) {
				*d = 0.0f;

				return 1;
			}
		}

		for(i=j+1; i<m_si; i++)
			if(m[i * m_sj + j] != 0.0f)
				Matrix_sumMultLine(m, m_si, m_sj, i, j, - m[i * m_sj + j] / m[j * m_sj + j]);
	}

	for(i=0; i<m_si; i++)
		*d *= m[i * m_sj + i];

	return 1;
}

static inline int Matrix_det2(float *d, float *m, int m_si, int m_sj) {
	if(m_si != m_sj) return 0;

	if(m_si == 0) {
		*d = 0.0f;

	} else if(m_si == 1) {
		*d = m[0];

	} else if(m_si == 2) {
		*d = m[0] * m[3] - m[1] * m[2];

	} else if(m_si == 3) {
		*d = m[0] * m[4] * m[8] + m[1] * m[5] * m[6] + m[2] * m[3] * m[7] - m[2] * m[4] * m[6] - m[1] * m[3] * m[8] - m[0] * m[5] * m[7];

	} else {
		int i;

		float td = 0.0f;
		float sd = 0.0f;

		float tmp[(m_si-1)*(m_sj-1)];

		for(i=0; i<m_si; i++) {
			Matrix_rmvLineAndCol(tmp, m_si-1, m_sj-1, m, m_si, m_sj, 0, i);

			Matrix_det2(&sd, tmp, m_si-1, m_sj-1);

			if(i % 2 == 0) {
				td += m[i] * sd;

			} else {
				td -= m[i] * sd;
			}
		}

		*d = td;
	}

	return 1;
}

static inline int Matrix_inv(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj) {
	if(m0_si != m1_si) return 0;

	if(m1_si != m1_sj) return 0;

	if(m0_si != m0_sj) return 0;

	float k, tmp[m0_si * m0_sj];

	Matrix_setI(m0, m0_si, m0_sj);

	Matrix_set(tmp, m0_si, m0_sj, m1, m1_si, m1_sj);

	int i, j;

	for(j=0; j<m0_sj; j++) {
		float max_v = fabsf(tmp[j * m0_sj + j]);

		int max_i = j;

		for(i=j+1; i<m0_si; i++) {
			float a = fabsf(tmp[i * m0_sj + j]);

			if(a > max_v) {
				max_v = a;
				max_i = i;
			}
		}

		if(max_v == 0.0f) return 0;

		if(max_i != j) {
			Matrix_swapLine(tmp, m0_si, m0_sj, j, max_i);

			Matrix_swapLine(m0, m0_si, m0_sj, j, max_i);
		}

		if(tmp[j * m0_sj + j] != 1.0f) {
			k = 1.0f / tmp[j * m0_sj + j];
			
			Matrix_multLine(tmp, m0_si, m0_sj, j, k);

			Matrix_multLine(m0, m0_si, m0_sj, j, k);
		}

		for(i=j+1; i<m0_si; i++)
			if(tmp[i * m0_sj + j] != 0.0f) {
				k = - tmp[i * m0_sj + j];

				Matrix_sumMultLine(tmp, m0_si, m0_sj, i, j, k);

				Matrix_sumMultLine(m0, m0_si, m0_sj, i, j, k);
			}

		for(i=0; i<j; i++)
			if(tmp[i * m0_sj + j] != 0.0f) {
				k = - tmp[i * m0_sj + j];

				Matrix_sumMultLine(tmp, m0_si, m0_sj, i, j, k);

				Matrix_sumMultLine(m0, m0_si, m0_sj, i, j, k);
			}
	}

	return 1;
}

static inline int Matrix_inv2(float *m0, int m0_si, int m0_sj, float *m1, int m1_si, int m1_sj) {
	if(m0_si != m1_si) return 0;

	if(m1_si != m1_sj) return 0;

	if(m0_si != m0_sj) return 0;

	float tmp[(m1_si-1) * (m1_sj-1)];

	float det, sd = 0.0f;

	Matrix_det(&det, m1, m1_si, m1_sj);

	int i, j;

	for(i=0; i<m1_si; i++) {
		for(j=0; j<m1_sj; j++) {
			Matrix_rmvLineAndCol(tmp, m1_si-1, m1_sj-1, m1, m1_si, m1_sj, i, j);

			Matrix_det(&sd, tmp, m1_si-1, m1_sj-1);

			if((i + j) % 2 == 0) {
				m0[j * m0_sj + i] = sd / det;

			} else
				m0[j * m0_sj + i] = - sd / det;
		}
	}

	return 1;
}

static inline int Matrix_solve(float *x, int x_s, float *a, int a_si, int a_sj, float *b, int b_s) {
	if(a_si != a_sj) return 0;

	if(a_si != b_s) return 0;

	if(a_sj != x_s) return 0;

	float tmp[a_si * a_sj];

	if(Matrix_inv(tmp, a_si, a_sj, a, a_si, a_sj) == 0) return 0;

	return Matrix_prod(x, x_s, 1, tmp, a_si, a_sj, b, b_s, 1);
}


#endif

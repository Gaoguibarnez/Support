
#include <stdint.h>

#include <math.h>


#define FLT16_INF (0x1F << 10)

#define FLT16_NAN ((0x1F << 10) | 1)


typedef uint16_t flt16_t;

float flt16ToFlt(flt16_t v) {
	int sig = (v >> 15) & 1;

	if((v & 0x7FFF) == 0) return sig ? - 0.0f : 0.0f;

	int exp = (v >> 10) & 0x1F;
	int man = v & 0x3FF;

	if(exp == 0x1F) {
		if(man != 0) return NAN;

		return sig ? - INFINITY : INFINITY;
	}

	float v_f = 1.0 + man / 1024.0f;
	v_f = v_f * powf(2.0f, exp - 15);

	return sig ? - v_f : v_f;
}

flt16_t fltToFlt16(float v) {
	if(v != v) return FLT16_NAN;

	if(v < - 65536.0f) return FLT16_INF | (1 << 15);

	if(v > 65536.0f) return FLT16_INF;

	int exp, sig = (signbit(v) != 0);

	if(sig) v = - v;

	if(v == 0.0f) return sig ? (1 << 15) : 0x0;

	float ln_v = log2f(v);

	if(ln_v < 0.0f) exp = (int) (floorf(ln_v) - 0.5f);

	else exp = (int) (floorf(ln_v) + 0.5f);

	v = v * powf(2.0f, - exp) - 1.0f;

	int man = (int) (roundf(v * powf(2.0f, 10)) + 0.5f);

	flt16_t f16 = (((exp + 15) & 0x1F) << 10) | (man & 0x3FF);

	return (sig ? 1 << 15 : 0) | f16;
}


typedef uint32_t Complex32;

Complex32 Complex32_build(float re, float im) {
	flt16_t re16 = fltToFlt16(re);
	flt16_t im16 = fltToFlt16(im);

	return re16 << 16 | im16;
}


#define COMPLEX32_I Complex32_build(0.0f, 1.0f)


float Complex32_real(Complex32 x) {
	return flt16ToFlt((x >> 16) & 0xFFFF);
}

float Complex32_imag(Complex32 x) {
	return flt16ToFlt(x & 0xFFFF);
}

float Complex32_abs(Complex32 z) {
	float im = Complex32_imag(z);
	float re = Complex32_real(z);

	return sqrtf(im * im + re * re);
}

float Complex32_arg(Complex32 z) {
	return atan2f(Complex32_imag(z), Complex32_real(z));
}

Complex32 Complex32_conj(Complex32 z) {
	return z ^ (0x1 << 15);
}


Complex32 Complex32_sum(Complex32 a, Complex32 b) {
	float a_re = Complex32_real(a);
	float a_im = Complex32_imag(a);

	float b_re = Complex32_real(b);
	float b_im = Complex32_imag(b);

	float re = a_re + b_re;
	float im = a_im + b_im;

	return Complex32_build(re, im);
}

Complex32 Complex32_sub(Complex32 a, Complex32 b) {
	float a_re = Complex32_real(a);
	float a_im = Complex32_imag(a);

	float b_re = Complex32_real(b);
	float b_im = Complex32_imag(b);

	float re = a_re - b_re;
	float im = a_im - b_im;

	return Complex32_build(re, im);
}

Complex32 Complex32_mul(Complex32 a, Complex32 b) {
	float a_re = Complex32_real(a);
	float a_im = Complex32_imag(a);

	float b_re = Complex32_real(b);
	float b_im = Complex32_imag(b);

	float re = a_re * b_re - a_im * b_im;
	float im = a_re * b_im + a_im * b_re;

	return Complex32_build(re, im);
}

Complex32 Complex32_div(Complex32 a, Complex32 b) {
	float a_re = Complex32_real(a);
	float a_im = Complex32_imag(a);

	float b_re = Complex32_real(b);
	float b_im = Complex32_imag(b);

	float l2 = b_re * b_re + b_im * b_im;

	float re = a_re * b_re + a_im * b_im;
	float im = a_im * b_re - a_re * b_im;

	return Complex32_build(re / l2, im / l2);
}

Complex32 Complex32_pow(Complex32 a, Complex32 b) {
	float a_re = Complex32_real(a);
	float a_im = Complex32_imag(a);

	float b_re = Complex32_real(b);
	float b_im = Complex32_imag(b);

	float a_rad = sqrtf(a_re * a_re + a_im * a_im);
	float a_arg = atan2f(a_im, a_re);

	// (r e^io)^(b_re+ib_im)
	// r^b_re e^iob_re r^ib_im e^io ib_im
	// r^b_re e^iob_re e^ln(r)ib_im e^-ob_im
	// r^b_re e^-ob_im e^i(ob_re+ln(r)b_im)

	float c_rad = powf(a_rad, b_re) * expf(- a_arg * b_im);
	float c_arg = a_arg * b_re + b_im * logf(a_rad);

	float c_re = c_rad * cosf(c_arg);
	float c_im = c_rad * sinf(c_arg);

	return Complex32_build(c_re, c_im);
}

Complex32 Complex32_exp(Complex32 z) {
	float r = expf(Complex32_real(z));
	float a = Complex32_imag(z);
	return Complex32_build(r * cos(a), r * sin(a));
}

Complex32 Complex32_ln(Complex32 z) {
	return Complex32_build(logf(Complex32_abs(z)), Complex32_arg(z));
}

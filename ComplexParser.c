
#ifndef COMPLEXPARSER_C
#define COMPLEXPARSER_C

#include <stdlib.h>

#include <string.h>


static inline int __is_i(char c) {
	return c == 'i';
}

static inline int __is_pm(char c) {
	return c == '+' || c == '-';
}

static inline int __is_space(char c) {
	return c == '\n' || c == '\r' || c == '\t' || c == ' ';
}

static inline int __is_flt_pow(char c) {
	return c == 'p' || c == 'P' || c == 'e' || c == 'E';
}

static inline int __is_imag(const char *str) {
	return strchr(str, 'i') != NULL || strchr(str, 'I') != NULL;
}

static inline void __parse_complex(const char *str, double *re_ptr, double *im_ptr) {
	char buf[260];

	int idx = 0;

	while(1) {
		while(__is_space(*str)) ++str;

		if((buf[idx++] = *str++) == '\0') break;

		if(idx >= 259) {
			buf[259] = '\0';
			break;
		}
	}

	for(idx=1; idx<259; idx++) {
		if(buf[idx] == '\0') break;
		if(__is_pm(buf[idx]) && !__is_flt_pow(buf[idx - 1])) break;
	}

	char *str0 = buf;
	char *str1 = buf + idx;

	int sig_re = 0;
	int sig_im = 0;
	if(str1[0] == '-') sig_im = 1;
	if(idx < 259) *str1++ = '\0';

	int is_im_0 = __is_imag(str0);
	int is_im_1 = __is_imag(str1);

	if(is_im_0 && is_im_1) return;

	if(!is_im_0 && !is_im_1) if(strlen(str1) > 0) return;

	if(is_im_0) {
		char *tmp = str0;
		str0 = str1;
		str1 = tmp;

		int sig_tmp = sig_re;
		sig_re = sig_im;
		sig_im = sig_tmp;
	}

	*re_ptr = strtod(str0, NULL);
	if(sig_re) *re_ptr = - *re_ptr;

	char *i_ptr = strchr(str1, 'i');
	char *I_ptr = strchr(str1, 'I');

	if(i_ptr == NULL) i_ptr = I_ptr;

	if(I_ptr != NULL && I_ptr < i_ptr) i_ptr = I_ptr;

	if(i_ptr != NULL) *i_ptr = '\0';

	*im_ptr = (i_ptr != str1) ? strtod(str1, NULL) : 1.0;
	if(sig_im) *im_ptr = - *im_ptr;
}

void parse_complex(const char *str, double *re_ptr, double *im_ptr) {
	double re = 0.0;
	double im = 0.0;

	if(str) __parse_complex(str, &re, &im);

	if(re_ptr) *re_ptr = re;
	if(im_ptr) *im_ptr = im;
}


#endif

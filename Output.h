
#include <stdarg.h>


#ifndef OUTPUT_H
#define OUTPUT_H


#define OUTPUT_MAXSIZE 4096


void Output_overridePrint(void (*prt)(const char *));


void Output_print(const char *msg);

void Output_printf(const char *fmt, ...);


void Output_printnoln(const char *msg);


void Output_error(const char *msg);

void Output_errorf(const char *fmt, ...);


void Output_warning(const char *msg);

void Output_warningf(const char *fmt, ...);


void Output_log(const char *msg);

void Output_logf(const char *fmt, ...);


void Output_setLogFile(const char *log);

void Output_initLogFile(const char *log);

void Output_enablePrintLog(int v);

void Output_enableLogPrint(int v);


#define oprint Output_print
#define oprintf Output_printf
#define oerror Output_error
#define oerrorf Output_errorf
#define owarning Output_warning
#define owarningf Output_warningf

#define oprt oprint
#define oprtf oprint
#define oerr oerror
#define oerrf oerrorf
#define owar owarning
#define owarf owarningf

#define print Output_print


#define OUTPUT_DEBUG_MAX 16

void Output_setDebugLevel(int v);

void _pvt_Output_debug(unsigned int lvl, char *msg);

void _pvt_Output_debugf(unsigned int lvl, char *fmt, va_list args);

// #define OUTPUT_DISABLEDEBUG

static inline void Output_debugl(unsigned int lvl, char *msg) {
	#ifndef OUTPUT_DISABLEDEBUG

	_pvt_Output_debug(lvl, msg);

	#endif
}

static inline void Output_debuglf(unsigned int lvl, char *fmt, ...) {
	#ifndef OUTPUT_DISABLEDEBUG

	va_list args;

	va_start(args, fmt);

	_pvt_Output_debugf(lvl, fmt, args);

	va_end(args);

	#endif
}

static inline void Output_debug(char *msg) {
	Output_debugl(OUTPUT_DEBUG_MAX, msg);
}

static inline void Output_debugf(char *fmt, ...) {
	#ifndef OUTPUT_DISABLEDEBUG

	va_list args;

	va_start(args, fmt);

	_pvt_Output_debugf(OUTPUT_DEBUG_MAX, fmt, args);

	va_end(args);

	#endif
}

void Output_debugl(unsigned int lvl, char *msg);

void Output_debuglf(unsigned int lvl, char *fmt, ...);

void Output_debug(char *msg);

void Output_debugf(char *fmt, ...);


#endif

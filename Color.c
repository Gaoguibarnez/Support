
#include <stdio.h>

#include "Memory.h"


#include "Color.h"


#ifndef COLOR_C
#define COLOR_C


// typedef struct Color {
// 	float r;
// 	float g;
// 	float b;

// 	float a;

// } Color;

static inline Color* Color_create() {
	Color *c = (Color*) Memory_alloc(sizeof(Color));

	return c;
}

static inline Color* Color_create0() {
	Color *c = Color_create();

	c->r = 0.0f;
	c->g = 0.0f;
	c->b = 0.0f;
	c->a = 0.0f;

	return c;
}

static inline Color* Color_create1(const Color *c) {
	Color *c1 = Color_create();

	c1->r = c->r;
	c1->g = c->g;
	c1->b = c->b;
	c1->a = c->a;

	return c1;
}

static inline Color* Color_create3(float r, float g, float b) {
	Color *c = Color_create();

	c->r = r;
	c->g = g;
	c->b = b;
	c->a = 1.0f;

	return c;
}

static inline Color* Color_create4(float r, float g, float b, float a) {
	Color *c = Color_create();

	c->r = r;
	c->g = g;
	c->b = b;
	c->a = a;

	return c;
}

static inline void Color_destroy(void *ptr) {
	Memory_free(ptr);
}

static inline Color* Color_set(Color *c1, const Color *c2) {
	*c1 = *c2;

	return c1;
}

static inline Color* Color_set0(Color *c) {
	c->r = 0.0f;
	c->g = 0.0f;
	c->b = 0.0f;
	c->a = 0.0f;

	return c;
}

static inline Color* Color_set3(Color *c, float r, float g, float b) {
	c->r = r;
	c->g = g;
	c->b = b;
	c->a = 1.0f;

	return c;
}

static inline Color* Color_set4(Color *c, float r, float g, float b, float a) {
	c->r = r;
	c->g = g;
	c->b = b;
	c->a = a;

	return c;
}

static inline Color* Color_set3i(Color *c, int r, int g, int b) {
	return Color_set3(c, r / 255.0f, g / 255.0f, b / 255.0f);
}

static inline Color* Color_set4i(Color *c, int r, int g, int b, int a) {
	return Color_set4(c, r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
}

static inline void Color_get(const Color *c, float *r, float *g, float *b, float *a) {
	if(r != NULL)
		*r = c->r;

	if(g != NULL)
		*g = c->g;

	if(b != NULL)
		*b = c->b;

	if(a != NULL)
		*a = c->a;
}

static inline Color* Color_mult(Color *c1, const Color *c2) {
	c1->r *= c2->r;
	c1->g *= c2->g;
	c1->b *= c2->b;
	c1->a *= c2->a;

	return c1;
}

static inline Color* Color_mult2(Color *c1, const Color *c2, const Color *c3) {
	c1->r = c2->r * c3->r;
	c1->g = c2->g * c3->g;
	c1->b = c2->b * c3->b;
	c1->a = c2->a * c3->a;

	return c1;
}

static inline float* Color_asFloatArray(Color *c) {
	return (float *) c;
}

// Color* Color_randomize(Color *c, float r) {
// 	c->r = c->r + r * (randomFloat() - 0.5f);
// 	c->g = c->g + r * (randomFloat() - 0.5f);
// 	c->b = c->b + r * (randomFloat() - 0.5f);

// 	return c;
// }

// Color* Color_randomize2(Color *c1, const Color *c2, float r) {
// 	c1->r = c2->r + r * (randomFloat() - 0.5f);
// 	c1->g = c2->g + r * (randomFloat() - 0.5f);
// 	c1->b = c2->b + r * (randomFloat() - 0.5f);

// 	return c1;
// }

static inline void Color_print(const Color *c) {
	printf("%f %f %f %f\n", c->r, c->g, c->b, c->a);
}

static const Color _color_black = {0.0f, 0.0f, 0.0f, 1.0f};
static const Color _color_gray = {0.5f, 0.5f, 0.5f, 1.0f};
static const Color _color_white = {1.0f, 1.0f, 1.0f, 1.0f};

static const Color _color_light_gray = {0.75f, 0.75f, 0.75f, 1.0f};
static const Color _color_dark_gray = {0.25f, 0.25f, 0.25f, 1.0f};

static const Color _color_red = {0.5f, 0.0f, 0.0f, 1.0f};
static const Color _color_green = {0.0f, 0.5f, 0.0f, 1.0f};
static const Color _color_blue = {0.0f, 0.0f, 0.5f, 1.0f};

static const Color _color_dark_red = {0.25f, 0.0f, 0.0f, 1.0f};
static const Color _color_dark_green = {0.0f, 0.25f, 0.0f, 1.0f};
static const Color _color_dark_blue = {0.0f, 0.0f, 0.25f, 1.0f};

static const Color _color_transparent_black = {0.0f, 0.0f, 0.0f, 0.0f};

#define BLACK (&_color_black)
#define GRAY (&_color_gray)
#define WHITE (&_color_white)

#define LIGHT_GRAY (&_color_light_gray)
#define DARK_GRAY (&_color_dark_gray)

#define RED (&_color_red)
#define GREEN (&_color_green)
#define BLUE (&_color_blue)

#define DARK_RED (&_color_dark_red)
#define DARK_GREEN (&_color_dark_green)
#define DARK_BLUE (&_color_dark_blue)

#define TRANSPARENT_BLACK (&_color_transparent_black)


#endif


#ifndef CONTAINER_C
#define CONTAINER_C

#include "Memory.h"


#include "Mutex.h"

#include "Flags.c"


#include "Container.h"



#define CONTAINER_FLAG_ALLOWREPVAL 0x1
#define CONTAINER_FLAG_SEARCHFROMBACK 0x2
#define CONTAINER_FLAG_IGNOREORDER 0x4

#define CONTAINER_FLAG_BLOCKSET 0x8
#define CONTAINER_FLAG_BLOCKGET 0x10


// typedef struct Container {
// 	Mutex *mtx;

// 	Flags flg;

// 	int num;
// 	int cnt;

// 	void **val;

// 	void *ptr;
// 	void (*insCbk)(void *, void *);
// 	void (*rmvCbk)(void *, void *);

// } Container;


static int CONTAINER_INITSIZE = 4;

static inline void Container_init(Container *ctn) {
	ctn->mtx = NULL;

	ctn->flg = 0;
	
	ctn->num = CONTAINER_INITSIZE;
	ctn->cnt = 0;

	ctn->val = (void**) Memory_alloc(CONTAINER_INITSIZE * sizeof(void*));

	ctn->ptr = NULL;
	ctn->insCbk = NULL;
	ctn->rmvCbk = NULL;
}

static inline void Container_term(void *ptr) {
	Container *ctn = (Container*) ptr;

	if(ctn->mtx != NULL) {
		Mutex_term(ctn->mtx);

		Memory_free(ctn->mtx);
	}

	Memory_free(ctn->val);
}


static inline Container* Container_create() {
	Container *ctn = (Container*) Memory_alloc(sizeof(Container));

	Container_init(ctn);

	return ctn;
}

static inline void Container_destroy(void *ptr) {
	Container_term(ptr);

	Memory_free(ptr);
}


static inline void Container_setMT(Container *ctn) {
	if(ctn->mtx == NULL)
		Mutex_init(ctn->mtx = (Mutex*) Memory_alloc(sizeof(Mutex)));
}

static inline void Container_lock(Container *ctn) {
	if(ctn->mtx != NULL)
		Mutex_lock(ctn->mtx);
}

static inline void Container_unlock(Container *ctn) {
	if(ctn->mtx != NULL)
		Mutex_unlock(ctn->mtx);
}


static inline void Container_setFlags(Container *ctn, Flags flg) {
	ctn->flg = flg;
}


static inline int __Container_searchi(Container *ctn, void *val) {
	int i, n = ctn->cnt;

	if(Flags_getf(&ctn->flg, CONTAINER_FLAG_SEARCHFROMBACK)) {
		for(i=n-1; i>=0; i--)
			if(ctn->val[i] == val) break;

		return i;
	}

	for(i=0; i<n; i++)
		if(ctn->val[i] == val) break;

	return (i < n) ? i : - 1;
}

static inline int Container_insert(Container *ctn, void *val) {
	Container_lock(ctn);

	if(!Flags_getf(&ctn->flg, CONTAINER_FLAG_ALLOWREPVAL))
		if(__Container_searchi(ctn, val) >= 0) {
			Container_unlock(ctn);

			return 0;
		}

	if(ctn->cnt >= ctn->num) {
		int num = 2 * ctn->num;

		void **val = (void**) Memory_alloc(num * sizeof(void*));

		memcpy(val, ctn->val, ctn->num * sizeof(void*));

		Memory_free(ctn->val);

		ctn->num = num;

		ctn->val = val;
	}

	ctn->val[ctn->cnt++] = val;

	Container_unlock(ctn);

	if(ctn->insCbk != NULL)
		ctn->insCbk(ctn->ptr, val);

	return 1;
}

static inline int Container_remove(Container *ctn, void *val) {
	Container_lock(ctn);

	int n = ctn->cnt;
	int i = __Container_searchi(ctn, val);

	if(i < 0) {
		Container_unlock(ctn);

		return 0;
	}

	if(Flags_getf(&ctn->flg, CONTAINER_FLAG_IGNOREORDER)) {
		ctn->val[i] = ctn->val[--ctn->cnt];

	} else {
		--ctn->cnt;
		
		for(; i<n; i++)
			ctn->val[i] = ctn->val[i+1];
	}

	Container_unlock(ctn);

	if(ctn->rmvCbk != NULL)
		ctn->rmvCbk(ctn->ptr, val);

	return 1;
}

static inline int Container_search(Container *ctn, void *val) {
	Container_lock(ctn);

	int r = __Container_searchi(ctn, val);

	Container_unlock(ctn);

	return r >= 0;
}


static inline int Container_size(Container *ctn) {
	return ctn->cnt;
}


static inline void* Container_geti(Container *ctn, int i) {
	if(i < 0 || i >= ctn->cnt)
		return NULL;

	return ctn->val[i];
}

static inline void** Container_get(Container *ctn, int *n) {
	if(n != NULL)
		*n = ctn->cnt;

	if(Flags_getf(&ctn->flg, CONTAINER_FLAG_BLOCKGET))
		return NULL;

	return ctn->val;
}

static inline int Container_set(Container *ctn, int n, void **v) {
	if(Flags_getf(&ctn->flg, CONTAINER_FLAG_BLOCKSET))
		return 0;

	Container_lock(ctn);

	if(n > ctn->num) {
		void **val = (void**) Memory_alloc(n * sizeof(void*));

		Memory_free(ctn->val);

		ctn->num = n;

		ctn->val = val;
	}

	if(v == NULL) {
		memset(ctn->val, 0, n * sizeof(void*));

	} else
		memcpy(ctn->val, v, n * sizeof(void*));

	ctn->cnt = n;

	Container_unlock(ctn);

	return 1;
}


static inline int Container_fill(Container *ctn, void **v, int n, int o) {
	Container_lock(ctn);

	int m = ctn->cnt;

	if(n < 0) n = 0;
	if(o < 0) o = 0;

	n += o;

	if(n > m) n = m;

	m = n - o;

	for(; o<n; o++) *v++ = ctn->val[o];

	Container_unlock(ctn);

	return m;
}

static inline int Container_clear(Container *ctn) {
	while(Container_size(ctn) > 0) {
		void *val = Container_geti(ctn, 0);

		if(!Container_remove(ctn, val)) return 0;
	}

	return 1;
}


static inline void Container_clearAndDestroy(void *ptr) {
	Container_clear(ptr);
	Container_destroy(ptr);
}


static inline void Container_setCallbacks(Container *ctn, void *ptr, void (*ins)(void *, void *), void (*rmv)(void *, void *)) {
	ctn->ptr = ptr;

	ctn->insCbk = ins;
	ctn->rmvCbk = rmv;
}



static inline Container* Container_createMT() {
	Container *ctn = Container_create();

	Container_setMT(ctn);

	return ctn;
}



#endif

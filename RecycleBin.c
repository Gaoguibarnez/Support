
#ifndef RECYCLEBYN
#define RECYCLEBYN


typedef struct RecycleBin {
	int size;
	int cnt;
	void *val[];

} RecycleBin;

static inline void RecycleBin_init(RecycleBin *bin, int size) {
	bin->size = size;
	bin->cnt = 0;
}

static inline void RecycleBin_term(RecycleBin *bin, void (*free_f)(void *)) {
	while(bin->cnt > 0) free_f(bin->val[--bin->cnt]);
}

static inline int RecycleBin_add(RecycleBin *bin, void *ptr) {
	if(bin->cnt >= bin->size) return 0;

	bin->val[bin->cnt++] = ptr;

	return 1;
}

static inline void* RecycleBin_get(RecycleBin *bin) {
	if(bin->cnt > 0) return bin->val[--bin->cnt];

	return NULL;
}

#define LRECYCLEBIN(SIZE) char local_recycle_bin_buffer[(SIZE) * sizeof(void*) + sizeof(RecycleBin)]; RecycleBin *local_recycle_bin;

#define LRECYCLEBIN_INIT(PAR, SIZE) RecycleBin_init((PAR)->local_recycle_bin = (RecycleBin*) (PAR)->local_recycle_bin_buffer, (SIZE));

#define LRECYCLEBIN_TERM(PAR, FREE) RecycleBin_term((PAR)->local_recycle_bin, (FREE));

#define LRECYCLEBIN_ADD(PAR, PTR) RecycleBin_add((PAR)->local_recycle_bin, (PTR));

#define LRECYCLEBIN_GET(PAR) RecycleBin_get((PAR)->local_recycle_bin);


#define RECYCLEBIN_INIT(PREFIX, SIZE) \
static char _ ## PREFIX ## _bin_buffer[(SIZE) * sizeof(void*) + sizeof(RecycleBin)] = {0}; \
static RecycleBin *_ ## PREFIX ## _bin = NULL; \
static inline int PREFIX ## _addToRecycleBin(void *ptr) { \
	if(_ ## PREFIX ## _bin == NULL) RecycleBin_init(_ ## PREFIX ## _bin = (RecycleBin*) _ ## PREFIX ## _bin_buffer, (SIZE)); \
	return RecycleBin_add(_ ## PREFIX ## _bin, ptr); \
} \
static inline void* PREFIX ## _getFromRecycleBin() { \
	if(_ ## PREFIX ## _bin == NULL) return NULL; \
	return RecycleBin_get(_ ## PREFIX ## _bin); \
}

#define RECYCLEBIN_TERM(PREFIX, FREE) RecycleBin_term(_ ## PREFIX ## _bin, (FREE))


#endif

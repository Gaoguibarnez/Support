
#ifndef KEYS_H
#define KEYS_H


void* Keys_has(const char *name);

void* Keys_get(const char *name);

void* Keys_hasi(const char *nm, unsigned int idx);

void* Keys_geti(const char *nm, unsigned int idx);


const char* Keys_toString(void *key);


void Keys_init();

void Keys_term();


static inline void* getkey(char *name) {
	return Keys_get(name);
}

static inline void* getkey(char *name);


static inline void* getikey(char *name, unsigned int idx) {
	return Keys_geti(name, idx);
}

static inline void* getikey(char *name, unsigned int idx);


static inline const char* Key_toString(void *key) {
	return Keys_toString(key);
}

static inline const char* Key_toString(void *key);


#endif


#ifndef COLOR_H
#define COLOR_H

typedef struct Color {
	float r;
	float g;
	float b;

	float a;

} Color;


/*

static inline Color* Color_create();

static inline Color* Color_create0();

static inline Color* Color_create1(const Color *c);

static inline Color* Color_create3(float r, float g, float b);

static inline Color* Color_create4(float r, float g, float b, float a);

static inline void Color_destroy(void *ptr);

static inline Color* Color_set(Color *c1, const Color *c2);

static inline Color* Color_set0(Color *c);

static inline Color* Color_set3(Color *c, float r, float g, float b);

static inline Color* Color_set4(Color *c, float r, float g, float b, float a);

static inline Color* Color_set3i(Color *c, int r, int g, int b);

static inline Color* Color_set4i(Color *c, int r, int g, int b, int a);

static inline void Color_get(const Color *c, float *r, float *g, float *b, float *a);

static inline Color* Color_mult(Color *c1, const Color *c2);

static inline Color* Color_mult2(Color *c1, const Color *c2, const Color *c3);

static inline void Color_print(const Color *c);

*/

#endif

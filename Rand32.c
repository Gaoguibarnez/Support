
#ifndef RAND32_C
#define RAND32_C



static uint32_t __rnd32_m1 = 0;
static uint32_t __rnd32_m2 = 0;
static uint32_t __rnd32_tm = 0;

typedef struct Rand32 {
    uint32_t st[4];
    uint32_t m1;
    uint32_t m2;
    uint32_t tm;

} Rand32;

#define __RANDOM_MAX 4294967295

static inline void __Rand32_next(Rand32 * r) {
    uint32_t y = r->st[3];
    uint32_t x = (r->st[0] & UINT32_C(0x7FFFFFFF)) ^ r->st[1] ^ r->st[2];

    x ^= (x << 1);
    y ^= (y >> 1) ^ x;

    r->st[0] = r->st[1];
    r->st[1] = r->st[2];
    r->st[2] = x ^ (y << 10);
    r->st[3] = y;

    r->st[1] ^= - ((int32_t) (y & 1)) & r->m1;
    r->st[2] ^= - ((int32_t) (y & 1)) & r->m2;
}

static inline uint32_t __Rand32_ext(Rand32 *r) {
    uint32_t t0 = r->st[3];
    uint32_t t1 = r->st[0] + (r->st[2] >> 8);

    t0 ^= t1;
    t0 ^= - ((int32_t) (t1 & 1)) & r->tm;

    return t0;
}

static inline float __Rand32_ext_conv(Rand32 *r) {
    union {
        uint32_t u;
        float f;

    } conv;

    uint32_t t0 = r->st[3];
    uint32_t t1 = r->st[0] + (r->st[2] >> 8);

    t0 ^= t1;

    conv.u = ((t0 ^ (- ((int32_t) (t1 & 1)) & r->tm)) >> 9) | UINT32_C(0x3F800000);

    return conv.f;
}

static inline void Rand32_init(Rand32 *r, uint32_t s) {
    r->m1 = __rnd32_m1;
    r->m2 = __rnd32_m2;
    r->tm = __rnd32_tm;

    r->st[0] = s;
    r->st[1] = r->m1;
    r->st[2] = r->m2;
    r->st[3] = r->tm;

    int i;

    for(i=1; i<8; i++) {
        r->st[i & 3] ^= i + UINT32_C(1812433253) * (r->st[(i - 1) & 3] ^ (r->st[(i - 1) & 3] >> 30));
    }

    if((r->st[0] & UINT32_C(0x7FFFFFFF)) == 0 && r->st[1] == 0 && r->st[2] == 0 && r->st[3] == 0) {
        r->st[0] = 'T';
        r->st[1] = 'I';
        r->st[2] = 'N';
        r->st[3] = 'Y';
    }

    for(i=0; i<8; i++) {
        __Rand32_next(r);
    }
}

static inline void Rand32_term(Rand32 *r) {
}

static inline Rand32* Rand32_create(uint32_t s) {
	Rand32 *r = (Rand32*) Memory_alloc(sizeof(Rand32));

	Rand32_init(r, s);

	return r;
}

static inline void Rand32_destroy(void *ptr) {
	Memory_free(ptr);
}

static inline uint32_t Rand32_uint32(Rand32 *r) {
    __Rand32_next(r);

    return __Rand32_ext(r);
}

static inline unsigned int Rand32_int(Rand32 *r, unsigned int rng) {
	if(rng == 0) return 0;

	return (unsigned int) Rand32_uint32(r) % rng;
}

static inline double Rand32_double(Rand32 *r) {
    return (double) Rand32_uint32(r) / 4294967295.0;
}

static inline float Rand32_float(Rand32 *r) {
	return (float) Rand32_double(r);
}

static inline float Rand32_gaussianFloat(Rand32 *r, float m, float s) {
	float u1, u2, rt;

	while(1) {
		u1 = Rand32_float(r);

		u2 = (2.0f * Rand32_float(r) - 1.0f) * 0.857763884960706796480190f;

		rt = u2 / u1;

		if(- 4.0f * log(u1) >= rt * rt) {
            if(Rand32_int(r, 2) == 0) {
                return - rt * s + m;
            }

			return rt * s + m;
		}
	}

	return 0.0f;
}

static inline float Rand32_gammaFloat(Rand32 *r, float a, float b) {
	#define e 2.71828182845904523536029f

	float c1 = pow((a - 1.0f) / e, (a - 1.0f) / 2.0f);
	float c2 = pow((a + 1.0f) / e, (a + 1.0f) / 2.0f);

	float u1, u2, rt;

	#undef e

	while(1) {
		u1 = c1 * Rand32_float(r);
		u2 = c2 * Rand32_float(r);

		rt = u2 / u1;

		if(u1 <= sqrt(pow(rt, a - 1.0f) * exp(- rt))) {
			return rt * b;
		}
	}

	return 0.0f;
}

#define __RND32STR_NUM_CHR 72

static const char __rnd32str_chr[] = {
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 
    'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 
    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 
    'y', 'z', 
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
    'Y', 'Z', 
    '0', '1', '2', '3', '4', '5', '6', '7', 
    '8', '9', 
    '-', '*', '#', '~', '&', '@', '!', '?', 
    '+', '=', 
};

static inline char* Rand32_string(Rand32 *r, char *b, int n) {
	char *t = b;

	while(--n >= 0) {
		*t++ = __rnd32str_chr[Rand32_int(r, __RND32STR_NUM_CHR)];
	}

	*t = '\0';

	return b;
}



#endif

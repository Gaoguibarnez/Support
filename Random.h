
#include <stdint.h>


#ifndef RANDOM_H
#define RANDOM_H



void randomSeed(int s);

void randomSeed64(uint64_t s);

unsigned int randomInt(unsigned int rng);

double randomDouble();

double randomGaussianDouble(double m, double s);

double randomGammaDouble(double a, double b);

float randomFloat();

float randomGaussianFloat(float m, float s);

float randomGammaFloat(float a, float b);

char* randomString(char *b, int n);

unsigned int randomPoissonInt(float l);


float symRandomFloat();



#endif

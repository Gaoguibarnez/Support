
#ifndef FUNCTIONERROR_C
#define FUNCTIONERROR_C


#include "ThreadLocal.h"

static ThreadLocal int function_internal_error = 0;

static void Function_setInternalErrorFlag(int e) {
	function_internal_error = e;
}

static int Function_checkInternalErrorFlag() {
	return function_internal_error;
}


static inline void __FunctionError__use() {
	Function_setInternalErrorFlag(0);
	Function_checkInternalErrorFlag();
}


#endif

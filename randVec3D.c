
#ifndef RANDVEC3D_C
#define RANDVEC3D_C


#include <math.h>


#include "Random.h"

#include "Vec3D.c"


static inline Vec3D* randVec3D(Vec3D *v) {
	return Vec3D_set2(v, 2.0f * (float) M_PI * randomFloat(), (float) acos(2.0f * randomFloat() - 1.0f));
}


static inline Vec3D* randVec3D(Vec3D *v);


#endif

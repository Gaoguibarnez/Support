
all: tools build

.PHONY: tools build clear test

tools:
	make -C "_build_" tools

build:
	make -C "_build_"

clear:
	make -C "_build_" clear

test:
	make -C "_build_" test

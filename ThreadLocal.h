
#ifdef __GNUC__

#define ThreadLocal __thread

#elif __STDC_VERSION__ >= 201112L

#define ThreadLocal _Thread_local

#elif defined(_MSC_VER)

#define ThreadLocal __declspec(thread)

#else

#error THREADLOCAL: UNSUPPORTED PLATFORM

#endif
